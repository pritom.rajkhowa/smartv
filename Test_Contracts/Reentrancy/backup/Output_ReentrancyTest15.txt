0
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Contract : EtherStore
Type : contract
FUNCTION NAME : EtherStore_deposit
FUNCTION DEFINITION : CONRT_EtherStore : _c1 --> void
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Output for 

~~~~~~~~~~~~~~Details Start~~~~~~~~~~~~~~~~~
EtherStore_balances : CONRT_EtherStore --> map
msg_sender : address
d1FaddressTuint256mapping : map X address --> uint256
msg_value : uint256
_1reentrancy : uint256
IsEtherStore : CONRT_EtherStore --> boolean
~~~~~~~~~~~~~~Details End~~~~~~~~~~~~~~~~~

~~~~~~~~~~~~~~Translation Start~~~~~~~~~~~~~~~~~


Output in normal notation:

1. Frame axioms:
IsEtherStore(_x1) -> EtherStore_balances1(_x1) = EtherStore_balances(_x1)
msg_sender1 = msg_sender
msg_value1 = msg_value


2. Output equations:
IsEtherStore(_c1) -> d1FaddressTuint256mapping1(_x1,_x2) = ite(((_x1=EtherStore_balances(_c1)) and (_x2=msg_sender)),(d1FaddressTuint256mapping(EtherStore_balances(_c1),msg_sender)+msg_value),d1FaddressTuint256mapping(_x1,_x2))
IsEtherStore(_c1) -> _1reentrancy1 = (d1FaddressTuint256mapping(EtherStore_balances(_c1),msg_sender)-ite(((EtherStore_balances(_c1)=EtherStore_balances(_c1)) and (msg_sender=msg_sender)),(d1FaddressTuint256mapping(EtherStore_balances(_c1),msg_sender)+msg_value),d1FaddressTuint256mapping(EtherStore_balances(_c1),msg_sender)))


3. Other axioms:

4. Assumption :


5. Assertion :


5. User Defined Assertion :


~~~~~~~~~~~~~~Translation End~~~~~~~~~~~~~~~~~

=======================================================================================================
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Contract : EtherStore
Type : contract
FUNCTION NAME : EtherStore_sendEther
FUNCTION DEFINITION : CONRT_EtherStore : _c1 X uint256 : bal-->  bool
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Output for 

~~~~~~~~~~~~~~Details Start~~~~~~~~~~~~~~~~~
EtherStore_balances : CONRT_EtherStore --> map
TUPLE_0 : TUPLE
msg_sender : address
bal : uint256
LOW_LEVEL_CALL_call : address X string --> TUPLE
sent : bool
UNPACK2DTUPLETbool : TUPLE X uint256 --> bool
ret : bool
_2IsReentrancy : uint256
IsEtherStore : CONRT_EtherStore --> boolean
~~~~~~~~~~~~~~Details End~~~~~~~~~~~~~~~~~

~~~~~~~~~~~~~~Translation Start~~~~~~~~~~~~~~~~~


Output in normal notation:

1. Frame axioms:
IsEtherStore(_x1) -> EtherStore_balances1(_x1) = EtherStore_balances(_x1)
msg_sender1 = msg_sender
bal1 = bal
LOW_LEVEL_CALL_call1(_x1,_x2) = LOW_LEVEL_CALL_call(_x1,_x2)
UNPACK2DTUPLETbool1(_x1,_x2) = UNPACK2DTUPLETbool(_x1,_x2)


2. Output equations:
TUPLE_01 = LOW_LEVEL_CALL_call(msg_sender,bal,"")
sent1 = UNPACK2DTUPLETbool(LOW_LEVEL_CALL_call(msg_sender,bal,""),0)
ret1 = UNPACK2DTUPLETbool(LOW_LEVEL_CALL_call(msg_sender,bal,""),0)
_2IsReentrancy1 = ite((_2reentrancy==bal),0,-1)


3. Other axioms:

4. Assumption :


5. Assertion :


5. User Defined Assertion :
(ite((_2reentrancy==bal),0,-1)==0)


~~~~~~~~~~~~~~Translation End~~~~~~~~~~~~~~~~~

=======================================================================================================
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Contract : EtherStore
Type : contract
FUNCTION NAME : EtherStore_withdraw
FUNCTION DEFINITION : CONRT_EtherStore : _c1 X uint256 : total --> void
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Output for 

~~~~~~~~~~~~~~Details Start~~~~~~~~~~~~~~~~~
EtherStore_balances : CONRT_EtherStore --> map
total : uint256
msg_sender : address
d1FaddressTuint256mapping : map X address --> uint256
bal : uint256
_1solidity : void
sent : bool
_2solidity : void
TUPLE_0 : TUPLE
LOW_LEVEL_CALL_call : address X string --> TUPLE
UNPACK2DTUPLETbool : TUPLE X uint256 --> bool
lv_1var : bool
_3reentrancy : uint256
_3IsReentrancy : uint256
IsEtherStore : CONRT_EtherStore --> boolean
~~~~~~~~~~~~~~Details End~~~~~~~~~~~~~~~~~

~~~~~~~~~~~~~~Translation Start~~~~~~~~~~~~~~~~~


Output in normal notation:

1. Frame axioms:
IsEtherStore(_x1) -> EtherStore_balances1(_x1) = EtherStore_balances(_x1)
msg_sender1 = msg_sender
LOW_LEVEL_CALL_call1(_x1,_x2) = LOW_LEVEL_CALL_call(_x1,_x2)
UNPACK2DTUPLETbool1(_x1,_x2) = UNPACK2DTUPLETbool(_x1,_x2)


2. Output equations:
total1 = ite(((total<=100)==True),(total+2),(total+1))
IsEtherStore(_c1) -> d1FaddressTuint256mapping1(_x1,_x2) = ite(((total<=100)==True),ite(((_x1=EtherStore_balances(_c1)) and (_x2=msg_sender)),0,d1FaddressTuint256mapping(_x1,_x2)),d1FaddressTuint256mapping(_x1,_x2))
IsEtherStore(_c1) -> bal1 = ite(((total<=100)==True),d1FaddressTuint256mapping(EtherStore_balances(_c1),msg_sender),bal)
IsEtherStore(_c1) -> sent1 = ite(((total<=100)==True),UNPACK2DTUPLETbool(LOW_LEVEL_CALL_call(msg_sender,d1FaddressTuint256mapping(EtherStore_balances(_c1),msg_sender),""),0),sent)
IsEtherStore(_c1) -> TUPLE_01 = ite(((total<=100)==True),LOW_LEVEL_CALL_call(msg_sender,d1FaddressTuint256mapping(EtherStore_balances(_c1),msg_sender),""),TUPLE_0)
IsEtherStore(_c1) -> lv_1var1 = ite(((total<=100)==True),UNPACK2DTUPLETbool(LOW_LEVEL_CALL_call(msg_sender,d1FaddressTuint256mapping(EtherStore_balances(_c1),msg_sender),""),0),lv_1var)
IsEtherStore(_c1) -> _3reentrancy1 = ite(((total<=100)==True),(d1FaddressTuint256mapping(EtherStore_balances(_c1),msg_sender)-ite(((EtherStore_balances(_c1)=EtherStore_balances(_c1)) and (msg_sender=msg_sender)),0,d1FaddressTuint256mapping(EtherStore_balances(_c1),msg_sender))),_3reentrancy)
IsEtherStore(_c1) -> _3IsReentrancy1 = ite(((total<=100)==True),ite(((d1FaddressTuint256mapping(EtherStore_balances(_c1),msg_sender)-ite(((EtherStore_balances(_c1)=EtherStore_balances(_c1)) and (msg_sender=msg_sender)),0,d1FaddressTuint256mapping(EtherStore_balances(_c1),msg_sender)))==d1FaddressTuint256mapping(EtherStore_balances(_c1),msg_sender)),0,-1),_3IsReentrancy)


3. Other axioms:

4. Assumption :
IsEtherStore(_c1) -> _1solidity1 = ite(((total<=100)==True),SOLIDITY_CALL_require((d1FaddressTuint256mapping(EtherStore_balances(_c1),msg_sender)>0)),_1solidity)
IsEtherStore(_c1) -> _2solidity1 = ite(((total<=100)==True),SOLIDITY_CALL_require(UNPACK2DTUPLETbool(LOW_LEVEL_CALL_call(msg_sender,d1FaddressTuint256mapping(EtherStore_balances(_c1),msg_sender),""),0),"Failed to send Ether"),_2solidity)


5. Assertion :


5. User Defined Assertion :
IsEtherStore(_c1) -> ((total<=100)==True) -> (ite(((d1FaddressTuint256mapping(EtherStore_balances(_c1),msg_sender)-ite(((EtherStore_balances(_c1)=EtherStore_balances(_c1)) and (msg_sender=msg_sender)),0,d1FaddressTuint256mapping(EtherStore_balances(_c1),msg_sender)))==d1FaddressTuint256mapping(EtherStore_balances(_c1),msg_sender)),0,-1)==0)


~~~~~~~~~~~~~~Translation End~~~~~~~~~~~~~~~~~

=======================================================================================================
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Contract : EtherStore
Type : contract
FUNCTION NAME : EtherStore_getBalance
FUNCTION DEFINITION : CONRT_EtherStore : _c1-->  uint256
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Output for 

~~~~~~~~~~~~~~Details Start~~~~~~~~~~~~~~~~~
EtherStore_balances : CONRT_EtherStore --> map
this : CONRT_EtherStore
convertToaddress : string --> address
BALANCE : address --> uint256
ret : uint256
IsEtherStore : CONRT_EtherStore --> boolean
~~~~~~~~~~~~~~Details End~~~~~~~~~~~~~~~~~

~~~~~~~~~~~~~~Translation Start~~~~~~~~~~~~~~~~~


Output in normal notation:

1. Frame axioms:
IsEtherStore(_x1) -> EtherStore_balances1(_x1) = EtherStore_balances(_x1)
this1 = this
convertToaddress1(_x1) = convertToaddress(_x1)
BALANCE1(_x1) = BALANCE(_x1)


2. Output equations:
ret1 = BALANCE(convertToaddress(this))


3. Other axioms:

4. Assumption :


5. Assertion :


5. User Defined Assertion :


~~~~~~~~~~~~~~Translation End~~~~~~~~~~~~~~~~~

=======================================================================================================
