class SmartVException(Exception):
    pass


class SmartVError(SmartVException):
    pass
