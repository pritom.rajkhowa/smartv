from z3 import *
set_param(proof=True)
try:
	CONRT_EtherStore= DeclareSort('CONRT_EtherStore')
	map= DeclareSort('map')
	EtherStore_balances= Function('EtherStore_balances',CONRT_EtherStore,map)
	total= Int('total')
	bal1= Int('bal1')
	void= DeclareSort('void')
	_1solidity= Const('_1solidity',void)
	TUPLE= DeclareSort('TUPLE')
	TUPLE_0= Const('TUPLE_0',TUPLE)
	address= DeclareSort('address')
	msg_sender= Const('msg_sender',address)
	LOW_LEVEL_CALL_call= Function('LOW_LEVEL_CALL_call',address,IntSort(),TUPLE)
	sent= Const('sent',BoolSort())
	UNPACK2DTUPLETbool= Function('UNPACK2DTUPLETbool',TUPLE,IntSort(),BoolSort())
	_2solidity= Const('_2solidity',void)
	d1FaddressTuint256mapping= Function('d1FaddressTuint256mapping',map,address,IntSort())
	bal2= Int('bal2')
	_3solidity= Const('_3solidity',void)
	TUPLE_1= Const('TUPLE_1',TUPLE)
	sent_scope_0= Const('sent_scope_0',BoolSort())
	_4solidity= Const('_4solidity',void)
	_1reentrancy_verifier= Int('_1reentrancy_verifier')
	_2reentrancy_verifier= Int('_2reentrancy_verifier')
	_3reentrancy_verifier= Int('_3reentrancy_verifier')
	_4reentrancy_verifier= Int('_4reentrancy_verifier')
	IsEtherStore= Function('IsEtherStore',CONRT_EtherStore,BoolSort())
	EtherStore_balances1= Function('EtherStore_balances1',CONRT_EtherStore,map)
	total1= Int('total1')
	bal11= Int('bal11')
	_1solidity1= Const('_1solidity1',void)
	TUPLE_01= Const('TUPLE_01',TUPLE)
	msg_sender1= Const('msg_sender1',address)
	LOW_LEVEL_CALL_call1= Function('LOW_LEVEL_CALL_call1',address,IntSort(),TUPLE)
	sent1= Const('sent1',BoolSort())
	UNPACK2DTUPLETbool1= Function('UNPACK2DTUPLETbool1',TUPLE,IntSort(),BoolSort())
	_2solidity1= Const('_2solidity1',void)
	d1FaddressTuint256mapping1= Function('d1FaddressTuint256mapping1',map,address,IntSort())
	bal21= Int('bal21')
	_3solidity1= Const('_3solidity1',void)
	TUPLE_11= Const('TUPLE_11',TUPLE)
	sent_scope_01= Const('sent_scope_01',BoolSort())
	_4solidity1= Const('_4solidity1',void)
	_1reentrancy_verifier1= Int('_1reentrancy_verifier1')
	_2reentrancy_verifier1= Int('_2reentrancy_verifier1')
	_3reentrancy_verifier1= Int('_3reentrancy_verifier1')
	_4reentrancy_verifier1= Int('_4reentrancy_verifier1')
	_y25= Const('_y25',CONRT_EtherStore)
	_y26= Const('_y26',address)
	_y27= Int('_y27')
	_y28= Const('_y28',TUPLE)
	_y29= Int('_y29')
	_y30= Const('_y30',map)
	_y31= Const('_y31',address)
	_y32= Const('_y32',CONRT_EtherStore)
	_s=Solver()
	_s.set("timeout",50000)
	_s.add(ForAll([_y25],EtherStore_balances1(_y25)==EtherStore_balances(_y25)))
	_s.add(total1==total)
	_s.add(bal11==bal1)
	_s.add(_1solidity1==_1solidity)
	_s.add(ForAll([_y26,_y27],LOW_LEVEL_CALL_call1(_y26,_y27)==LOW_LEVEL_CALL_call(_y26,_y27)))
	_s.add(ForAll([_y28,_y29],UNPACK2DTUPLETbool1(_y28,_y29)==UNPACK2DTUPLETbool(_y28,_y29)))
	_s.add(_2solidity1==_2solidity)
	_s.add(bal21==bal2)
	_s.add(_3solidity1==_3solidity)
	_s.add(sent_scope_01==sent_scope_0)
	_s.add(_4solidity1==_4solidity)
	_s.add(ForAll([msg_sender],TUPLE_01==If(((total > 0)==(True)),If(bal1 > 0,LOW_LEVEL_CALL_call(msg_sender,bal1),TUPLE_0),If(bal2 > 0,If(((total > 0)==(True)),If(bal1 > 0,LOW_LEVEL_CALL_call(msg_sender,bal1),TUPLE_0),TUPLE_0),TUPLE_0))))
	_s.add(ForAll([msg_sender],sent1==If(((total > 0)==(True)),If(bal1 > 0,UNPACK2DTUPLETbool(LOW_LEVEL_CALL_call(msg_sender,bal1),0),sent),If(bal2 > 0,UNPACK2DTUPLETbool(LOW_LEVEL_CALL_call(msg_sender,bal2),0),sent))))
	_s.add(ForAll([_y30,_y31,msg_sender,_y32],d1FaddressTuint256mapping1(_y30,_y31)==If(((total > 0)==(True)),If(bal1 > 0,If(UNPACK2DTUPLETbool(LOW_LEVEL_CALL_call(msg_sender,bal1),0),If(And(((_y30)==(EtherStore_balances(_y32))),((_y31)==(msg_sender))),((If(((total > 0)==(True)),If(bal1 > 0,If(UNPACK2DTUPLETbool(LOW_LEVEL_CALL_call(msg_sender,bal1),0),If(And(((EtherStore_balances(_y32))==(EtherStore_balances(_y32))),((msg_sender)==(msg_sender))),((d1FaddressTuint256mapping(EtherStore_balances(_y32),msg_sender))-(bal1)),d1FaddressTuint256mapping(EtherStore_balances(_y32),msg_sender)),d1FaddressTuint256mapping(EtherStore_balances(_y32),msg_sender)),d1FaddressTuint256mapping(EtherStore_balances(_y32),msg_sender)),If(bal2 > 0,If(sent_scope_0,If(And(((EtherStore_balances(_y32))==(EtherStore_balances(_y32))),((msg_sender)==(msg_sender))),((d1FaddressTuint256mapping(EtherStore_balances(_y32),msg_sender))-(bal2)),d1FaddressTuint256mapping(EtherStore_balances(_y32),msg_sender)),d1FaddressTuint256mapping(EtherStore_balances(_y32),msg_sender)),d1FaddressTuint256mapping(EtherStore_balances(_y32),msg_sender))))-(bal1)),If(((total > 0)==(True)),If(bal1 > 0,If(UNPACK2DTUPLETbool(LOW_LEVEL_CALL_call(msg_sender,bal1),0),If(And(((_y30)==(EtherStore_balances(_y32))),((_y31)==(msg_sender))),((d1FaddressTuint256mapping(EtherStore_balances(_y32),msg_sender))-(bal1)),d1FaddressTuint256mapping(_y30,_y31)),d1FaddressTuint256mapping(_y30,_y31)),d1FaddressTuint256mapping(_y30,_y31)),If(bal2 > 0,If(sent_scope_0,If(And(((_y30)==(EtherStore_balances(_y32))),((_y31)==(msg_sender))),((d1FaddressTuint256mapping(EtherStore_balances(_y32),msg_sender))-(bal2)),d1FaddressTuint256mapping(_y30,_y31)),d1FaddressTuint256mapping(_y30,_y31)),d1FaddressTuint256mapping(_y30,_y31)))),If(((total > 0)==(True)),If(bal1 > 0,If(UNPACK2DTUPLETbool(LOW_LEVEL_CALL_call(msg_sender,bal1),0),If(And(((_y30)==(EtherStore_balances(_y32))),((_y31)==(msg_sender))),((d1FaddressTuint256mapping(EtherStore_balances(_y32),msg_sender))-(bal1)),d1FaddressTuint256mapping(_y30,_y31)),d1FaddressTuint256mapping(_y30,_y31)),d1FaddressTuint256mapping(_y30,_y31)),If(bal2 > 0,If(sent_scope_0,If(And(((_y30)==(EtherStore_balances(_y32))),((_y31)==(msg_sender))),((d1FaddressTuint256mapping(EtherStore_balances(_y32),msg_sender))-(bal2)),d1FaddressTuint256mapping(_y30,_y31)),d1FaddressTuint256mapping(_y30,_y31)),d1FaddressTuint256mapping(_y30,_y31)))),d1FaddressTuint256mapping(_y30,_y31)),If(bal2 > 0,If(sent_scope_0,If(And(((_y30)==(EtherStore_balances(_y32))),((_y31)==(msg_sender))),((If(((total > 0)==(True)),If(bal1 > 0,If(UNPACK2DTUPLETbool(LOW_LEVEL_CALL_call(msg_sender,bal1),0),If(And(((EtherStore_balances(_y32))==(EtherStore_balances(_y32))),((msg_sender)==(msg_sender))),((If(((total > 0)==(True)),If(bal1 > 0,If(UNPACK2DTUPLETbool(LOW_LEVEL_CALL_call(msg_sender,bal1),0),If(And(((EtherStore_balances(_y32))==(EtherStore_balances(_y32))),((msg_sender)==(msg_sender))),((d1FaddressTuint256mapping(EtherStore_balances(_y32),msg_sender))-(bal1)),d1FaddressTuint256mapping(EtherStore_balances(_y32),msg_sender)),d1FaddressTuint256mapping(EtherStore_balances(_y32),msg_sender)),d1FaddressTuint256mapping(EtherStore_balances(_y32),msg_sender)),If(bal2 > 0,If(sent_scope_0,If(And(((EtherStore_balances(_y32))==(EtherStore_balances(_y32))),((msg_sender)==(msg_sender))),((d1FaddressTuint256mapping(EtherStore_balances(_y32),msg_sender))-(bal2)),d1FaddressTuint256mapping(EtherStore_balances(_y32),msg_sender)),d1FaddressTuint256mapping(EtherStore_balances(_y32),msg_sender)),d1FaddressTuint256mapping(EtherStore_balances(_y32),msg_sender))))-(bal1)),If(((total > 0)==(True)),If(bal1 > 0,If(UNPACK2DTUPLETbool(LOW_LEVEL_CALL_call(msg_sender,bal1),0),If(And(((EtherStore_balances(_y32))==(EtherStore_balances(_y32))),((msg_sender)==(msg_sender))),((d1FaddressTuint256mapping(EtherStore_balances(_y32),msg_sender))-(bal1)),d1FaddressTuint256mapping(EtherStore_balances(_y32),msg_sender)),d1FaddressTuint256mapping(EtherStore_balances(_y32),msg_sender)),d1FaddressTuint256mapping(EtherStore_balances(_y32),msg_sender)),If(bal2 > 0,If(sent_scope_0,If(And(((EtherStore_balances(_y32))==(EtherStore_balances(_y32))),((msg_sender)==(msg_sender))),((d1FaddressTuint256mapping(EtherStore_balances(_y32),msg_sender))-(bal2)),d1FaddressTuint256mapping(EtherStore_balances(_y32),msg_sender)),d1FaddressTuint256mapping(EtherStore_balances(_y32),msg_sender)),d1FaddressTuint256mapping(EtherStore_balances(_y32),msg_sender)))),If(((total > 0)==(True)),If(bal1 > 0,If(UNPACK2DTUPLETbool(LOW_LEVEL_CALL_call(msg_sender,bal1),0),If(And(((EtherStore_balances(_y32))==(EtherStore_balances(_y32))),((msg_sender)==(msg_sender))),((d1FaddressTuint256mapping(EtherStore_balances(_y32),msg_sender))-(bal1)),d1FaddressTuint256mapping(EtherStore_balances(_y32),msg_sender)),d1FaddressTuint256mapping(EtherStore_balances(_y32),msg_sender)),d1FaddressTuint256mapping(EtherStore_balances(_y32),msg_sender)),If(bal2 > 0,If(sent_scope_0,If(And(((EtherStore_balances(_y32))==(EtherStore_balances(_y32))),((msg_sender)==(msg_sender))),((d1FaddressTuint256mapping(EtherStore_balances(_y32),msg_sender))-(bal2)),d1FaddressTuint256mapping(EtherStore_balances(_y32),msg_sender)),d1FaddressTuint256mapping(EtherStore_balances(_y32),msg_sender)),d1FaddressTuint256mapping(EtherStore_balances(_y32),msg_sender)))),d1FaddressTuint256mapping(EtherStore_balances(_y32),msg_sender)),If(bal2 > 0,If(sent_scope_0,If(And(((EtherStore_balances(_y32))==(EtherStore_balances(_y32))),((msg_sender)==(msg_sender))),((d1FaddressTuint256mapping(EtherStore_balances(_y32),msg_sender))-(bal2)),d1FaddressTuint256mapping(EtherStore_balances(_y32),msg_sender)),d1FaddressTuint256mapping(EtherStore_balances(_y32),msg_sender)),d1FaddressTuint256mapping(EtherStore_balances(_y32),msg_sender))))-(bal2)),If(((total > 0)==(True)),If(bal1 > 0,If(UNPACK2DTUPLETbool(LOW_LEVEL_CALL_call(msg_sender,bal1),0),If(And(((_y30)==(EtherStore_balances(_y32))),((_y31)==(msg_sender))),((If(((total > 0)==(True)),If(bal1 > 0,If(UNPACK2DTUPLETbool(LOW_LEVEL_CALL_call(msg_sender,bal1),0),If(And(((EtherStore_balances(_y32))==(EtherStore_balances(_y32))),((msg_sender)==(msg_sender))),((d1FaddressTuint256mapping(EtherStore_balances(_y32),msg_sender))-(bal1)),d1FaddressTuint256mapping(EtherStore_balances(_y32),msg_sender)),d1FaddressTuint256mapping(EtherStore_balances(_y32),msg_sender)),d1FaddressTuint256mapping(EtherStore_balances(_y32),msg_sender)),If(bal2 > 0,If(sent_scope_0,If(And(((EtherStore_balances(_y32))==(EtherStore_balances(_y32))),((msg_sender)==(msg_sender))),((d1FaddressTuint256mapping(EtherStore_balances(_y32),msg_sender))-(bal2)),d1FaddressTuint256mapping(EtherStore_balances(_y32),msg_sender)),d1FaddressTuint256mapping(EtherStore_balances(_y32),msg_sender)),d1FaddressTuint256mapping(EtherStore_balances(_y32),msg_sender))))-(bal1)),If(((total > 0)==(True)),If(bal1 > 0,If(UNPACK2DTUPLETbool(LOW_LEVEL_CALL_call(msg_sender,bal1),0),If(And(((_y30)==(EtherStore_balances(_y32))),((_y31)==(msg_sender))),((d1FaddressTuint256mapping(EtherStore_balances(_y32),msg_sender))-(bal1)),d1FaddressTuint256mapping(_y30,_y31)),d1FaddressTuint256mapping(_y30,_y31)),d1FaddressTuint256mapping(_y30,_y31)),If(bal2 > 0,If(sent_scope_0,If(And(((_y30)==(EtherStore_balances(_y32))),((_y31)==(msg_sender))),((d1FaddressTuint256mapping(EtherStore_balances(_y32),msg_sender))-(bal2)),d1FaddressTuint256mapping(_y30,_y31)),d1FaddressTuint256mapping(_y30,_y31)),d1FaddressTuint256mapping(_y30,_y31)))),If(((total > 0)==(True)),If(bal1 > 0,If(UNPACK2DTUPLETbool(LOW_LEVEL_CALL_call(msg_sender,bal1),0),If(And(((_y30)==(EtherStore_balances(_y32))),((_y31)==(msg_sender))),((d1FaddressTuint256mapping(EtherStore_balances(_y32),msg_sender))-(bal1)),d1FaddressTuint256mapping(_y30,_y31)),d1FaddressTuint256mapping(_y30,_y31)),d1FaddressTuint256mapping(_y30,_y31)),If(bal2 > 0,If(sent_scope_0,If(And(((_y30)==(EtherStore_balances(_y32))),((_y31)==(msg_sender))),((d1FaddressTuint256mapping(EtherStore_balances(_y32),msg_sender))-(bal2)),d1FaddressTuint256mapping(_y30,_y31)),d1FaddressTuint256mapping(_y30,_y31)),d1FaddressTuint256mapping(_y30,_y31)))),d1FaddressTuint256mapping(_y30,_y31)),If(bal2 > 0,If(sent_scope_0,If(And(((_y30)==(EtherStore_balances(_y32))),((_y31)==(msg_sender))),((d1FaddressTuint256mapping(EtherStore_balances(_y32),msg_sender))-(bal2)),d1FaddressTuint256mapping(_y30,_y31)),d1FaddressTuint256mapping(_y30,_y31)),d1FaddressTuint256mapping(_y30,_y31)))),If(((total > 0)==(True)),If(bal1 > 0,If(UNPACK2DTUPLETbool(LOW_LEVEL_CALL_call(msg_sender,bal1),0),If(And(((_y30)==(EtherStore_balances(_y32))),((_y31)==(msg_sender))),((If(((total > 0)==(True)),If(bal1 > 0,If(UNPACK2DTUPLETbool(LOW_LEVEL_CALL_call(msg_sender,bal1),0),If(And(((EtherStore_balances(_y32))==(EtherStore_balances(_y32))),((msg_sender)==(msg_sender))),((d1FaddressTuint256mapping(EtherStore_balances(_y32),msg_sender))-(bal1)),d1FaddressTuint256mapping(EtherStore_balances(_y32),msg_sender)),d1FaddressTuint256mapping(EtherStore_balances(_y32),msg_sender)),d1FaddressTuint256mapping(EtherStore_balances(_y32),msg_sender)),If(bal2 > 0,If(sent_scope_0,If(And(((EtherStore_balances(_y32))==(EtherStore_balances(_y32))),((msg_sender)==(msg_sender))),((d1FaddressTuint256mapping(EtherStore_balances(_y32),msg_sender))-(bal2)),d1FaddressTuint256mapping(EtherStore_balances(_y32),msg_sender)),d1FaddressTuint256mapping(EtherStore_balances(_y32),msg_sender)),d1FaddressTuint256mapping(EtherStore_balances(_y32),msg_sender))))-(bal1)),If(((total > 0)==(True)),If(bal1 > 0,If(UNPACK2DTUPLETbool(LOW_LEVEL_CALL_call(msg_sender,bal1),0),If(And(((_y30)==(EtherStore_balances(_y32))),((_y31)==(msg_sender))),((d1FaddressTuint256mapping(EtherStore_balances(_y32),msg_sender))-(bal1)),d1FaddressTuint256mapping(_y30,_y31)),d1FaddressTuint256mapping(_y30,_y31)),d1FaddressTuint256mapping(_y30,_y31)),If(bal2 > 0,If(sent_scope_0,If(And(((_y30)==(EtherStore_balances(_y32))),((_y31)==(msg_sender))),((d1FaddressTuint256mapping(EtherStore_balances(_y32),msg_sender))-(bal2)),d1FaddressTuint256mapping(_y30,_y31)),d1FaddressTuint256mapping(_y30,_y31)),d1FaddressTuint256mapping(_y30,_y31)))),If(((total > 0)==(True)),If(bal1 > 0,If(UNPACK2DTUPLETbool(LOW_LEVEL_CALL_call(msg_sender,bal1),0),If(And(((_y30)==(EtherStore_balances(_y32))),((_y31)==(msg_sender))),((d1FaddressTuint256mapping(EtherStore_balances(_y32),msg_sender))-(bal1)),d1FaddressTuint256mapping(_y30,_y31)),d1FaddressTuint256mapping(_y30,_y31)),d1FaddressTuint256mapping(_y30,_y31)),If(bal2 > 0,If(sent_scope_0,If(And(((_y30)==(EtherStore_balances(_y32))),((_y31)==(msg_sender))),((d1FaddressTuint256mapping(EtherStore_balances(_y32),msg_sender))-(bal2)),d1FaddressTuint256mapping(_y30,_y31)),d1FaddressTuint256mapping(_y30,_y31)),d1FaddressTuint256mapping(_y30,_y31)))),d1FaddressTuint256mapping(_y30,_y31)),If(bal2 > 0,If(sent_scope_0,If(And(((_y30)==(EtherStore_balances(_y32))),((_y31)==(msg_sender))),((d1FaddressTuint256mapping(EtherStore_balances(_y32),msg_sender))-(bal2)),d1FaddressTuint256mapping(_y30,_y31)),d1FaddressTuint256mapping(_y30,_y31)),d1FaddressTuint256mapping(_y30,_y31)))),d1FaddressTuint256mapping(_y30,_y31)))))
	_s.add(ForAll([msg_sender],TUPLE_11==If(((total > 0)==(True)),If(bal1 > 0,If(((total > 0)==(True)),TUPLE_1,If(bal2 > 0,LOW_LEVEL_CALL_call(msg_sender,bal2),TUPLE_1)),TUPLE_1),If(bal2 > 0,LOW_LEVEL_CALL_call(msg_sender,bal2),TUPLE_1))))
	_s.add(_1reentrancy_verifier1==If(((total > 0)==(True)),If(bal1 > 0,1,0),If(bal2 > 0,If(((total > 0)==(True)),If(bal1 > 0,1,0),0),0)))
	_s.add(_2reentrancy_verifier1==If(((total > 0)==(True)),If(bal1 > 0,If(((total > 0)==(True)),If(bal1 > 0,1,0),0),0),If(bal2 > 0,If(((total > 0)==(True)),If(bal1 > 0,If(((total > 0)==(True)),If(bal1 > 0,1,0),0),0),0),0)))
	_s.add(_3reentrancy_verifier1==If(((total > 0)==(True)),0,If(bal2 > 0,1,0)))
	_s.add(_4reentrancy_verifier1==If(((total > 0)==(True)),0,If(bal2 > 0,If(((total > 0)==(True)),0,If(bal2 > 0,1,0)),0)))
	_s.add(And(Not(((total > 0)==(True))),bal1 > 0))
	_s.add(Not(And((_3reentrancy_verifier1==1),(_4reentrancy_verifier1==1))))
except Exception as e:
	print("Error1(Z3Query)"+str(e))
	sys.exit(1)
try:
	result=_s.check()
	if sat==result:
		print("Counter Example")
		print (_s.model())
	elif unsat==result:
		print ("Successfully Proved")
	else:
		print("Failed To Prove")
except Exception as e:
	print("Error2(Z3Query)"+str(e))
	sys.exit(1)