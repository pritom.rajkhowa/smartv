"""
    Module printing summary of the contract
"""

from smartV.translation.abstract_translation import AbstractTranslator
from slither.utils.myprettytable import MyPrettyTable


class FunctionSummary(AbstractTranslator):

    ARGUMENT = "function-summary"
    HELP = "Print a summary of the functions"

    WIKI = "https://github.com/trailofbits/slither/wiki/Printer-documentation#function-summary"

    @staticmethod
    def _convert(l):
        if l:
            n = 2
            l = [l[i : i + n] for i in range(0, len(l), n)]
            l = [str(x) for x in l]
            return "\n".join(l)
        return str(l)

    def output(self, _filename):  # pylint: disable=too-many-locals
        """
        _filename is not used
        Args:
            _filename(string)
        """
        
        all_contract_var_tables = {}

        for contract in self.slither.contracts_derived:
            #txt += "\n{}:\n".format(contract.name)
            table = MyPrettyTable(["Name", "Type", "Slot", "Offset"])
            print(table.__dict__)
            print('XXXXXXXXXXXXXXXXXX---------------')
            for variable in contract.state_variables_ordered:
                if not variable.is_constant:
                    slot, offset = contract.compilation_unit.storage_layout_of(contract, variable)
                    table.add_row([variable.canonical_name, str(variable.type), slot, offset])
            
            if len(table._rows)>0:
            
               all_contract_var_tables[contract.name] = table

        
               
        all_tables = []
        all_txt = ""

        for c in self.contracts:
            if c.is_top_level:
                continue
                
                
            if c.kind=='library':
            
               (name, inheritance, var, func_summaries, modif_summaries) = c.get_summary()
               txt = "\nContract %s" % name
               txt += "\nContract vars: " + str(var)
               txt += "\nInheritance:: " + str(inheritance)
               if name in all_contract_var_tables:
                  txt += "\n\n" + str(all_contract_var_tables[name])
                  txt += "\n"
            
            elif c.kind=='interface':
            
               (name, inheritance, var, func_summaries, modif_summaries) = c.get_summary()
               txt = "\nContract %s" % name
               txt += "\nContract vars: " + str(var)
               txt += "\nInheritance:: " + str(inheritance)
            
            else:
                
               (name, inheritance, var, func_summaries, modif_summaries) = c.get_summary()
               txt = "\nContract %s" % name
               txt += "\nContract vars: " + str(var)
               txt += "\nInheritance:: " + str(inheritance)
               if name in all_contract_var_tables:
                  txt += "\n\n" + str(all_contract_var_tables[name])
                  txt += "\n"
            
            table = MyPrettyTable(
                [
                    "Function",
                    "Visibility",
                    "Modifiers",
                    "Read",
                    "Write",
                    "Internal Calls",
                    "External Calls",
                ]
            )
            for (
                _c_name,
                f_name,
                visi,
                modifiers,
                read,
                write,
                internal_calls,
                external_calls,
            ) in func_summaries:
                read = self._convert(read)
                write = self._convert(write)
                internal_calls = self._convert(internal_calls)
                external_calls = self._convert(external_calls)
                table.add_row(
                    [
                        f_name,
                        visi,
                        modifiers,
                        read,
                        write,
                        internal_calls,
                        external_calls,
                    ]
                )
            txt += "\n\n" + str(table)
            txt += "\n"
            #self.info(txt)
            print(txt)
            #all_tables.append((name, table))
            all_txt += txt
         
        res = self.generate_output(all_txt)
        for name, table in all_tables:
            res.add_pretty_table(table, name)

        return res
