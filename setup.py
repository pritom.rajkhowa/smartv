from setuptools import setup, find_packages
import os

def copy_dir():
    dir_path = 'packages'
    base_dir = os.path.join('./smartV', dir_path)
    for (dirpath, dirnames, files) in os.walk(base_dir):
        for f in files:
            yield os.path.join(dirpath.split('/', 1)[1], f)

setup(
    name="smartV-verifer",
    description="SmartV is a Solidity static analysis framework written in Python 3.",
    url="https://gitlab.com/pritom.rajkhowa/test-smartv",
    author="Pritom Rajkhowa and Fangzhen Lin",
    version="0.1.1",
    packages=find_packages(),  
    python_requires=">=3.6",
    install_requires=[
        "setuptools>=58.0.4",
        "plyj>=0.1",
        "ply>=3.11",
        "sympy>=1.8",
        "configparser>=5.0.2",
        "pyparsing>=2.4.7",
        "pycparser<=2.18",
        "pycparserext>=2020.1",
        "prettytable>=0.7.2",
        "pysha3>=1.0.2",
        "crytic-compile>=0.2.1",
        "regex>=2021.8.28",
        "slither-analyzer>=0.8.1",
        "evm-cfg-builder>=0.3.1",
        # "crytic-compile",
    ],
    license="AGPL-3.0",
    long_description=open("README.md").read(),
    entry_points={
        "console_scripts": [
            "smartV = smartV.__main__:main",
        ]
    },
)
