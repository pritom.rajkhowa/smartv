from z3 import *
set_param(proof=True)
try:
	CONRT_EtherStore= DeclareSort('CONRT_EtherStore')
	map= DeclareSort('map')
	EtherStore_balances= Function('EtherStore_balances',CONRT_EtherStore,map)
	i= Int('i')
	array= DeclareSort('array')
	senders= Const('senders',array)
	array_length= Function('array_length',array,IntSort())
	address= DeclareSort('address')
	d1Taddressarray= Function('d1Taddressarray',array,IntSort(),address)
	d1FaddressTuint256mapping= Function('d1FaddressTuint256mapping',map,address,IntSort())
	bal= Int('bal')
	void= DeclareSort('void')
	_1solidity= Const('_1solidity',void)
	TUPLE= DeclareSort('TUPLE')
	TUPLE_0= Const('TUPLE_0',TUPLE)
	LOW_LEVEL_CALL_call= Function('LOW_LEVEL_CALL_call',address,IntSort(),TUPLE)
	sent= Const('sent',BoolSort())
	UNPACK2DTUPLETbool= Function('UNPACK2DTUPLETbool',TUPLE,IntSort(),BoolSort())
	_2solidity= Const('_2solidity',void)
	_1reentrancy_verifier= Int('_1reentrancy_verifier')
	_2reentrancy_verifier= Int('_2reentrancy_verifier')
	IsEtherStore= Function('IsEtherStore',CONRT_EtherStore,BoolSort())
	EtherStore_balances1= Function('EtherStore_balances1',CONRT_EtherStore,map)
	i1= Int('i1')
	senders1= Const('senders1',array)
	array_length1= Function('array_length1',array,IntSort())
	d1Taddressarray1= Function('d1Taddressarray1',array,IntSort(),address)
	d1FaddressTuint256mapping1= Function('d1FaddressTuint256mapping1',map,address,IntSort())
	bal1= Int('bal1')
	_1solidity1= Const('_1solidity1',void)
	TUPLE_01= Const('TUPLE_01',TUPLE)
	LOW_LEVEL_CALL_call1= Function('LOW_LEVEL_CALL_call1',address,IntSort(),TUPLE)
	sent1= Const('sent1',BoolSort())
	UNPACK2DTUPLETbool1= Function('UNPACK2DTUPLETbool1',TUPLE,IntSort(),BoolSort())
	_2solidity1= Const('_2solidity1',void)
	_1reentrancy_verifier1= Int('_1reentrancy_verifier1')
	_2reentrancy_verifier1= Int('_2reentrancy_verifier1')
	i8= Function('i8',IntSort(),IntSort(),IntSort())
	d1FaddressTuint256mapping8= Function('d1FaddressTuint256mapping8',map,address,IntSort(),IntSort(),IntSort())
	bal8= Function('bal8',IntSort(),IntSort(),IntSort())
	TUPLE_08= Function('TUPLE_08',IntSort(),IntSort(),TUPLE)
	sent8= Function('sent8',IntSort(),IntSort(),BoolSort())
	_2reentrancy_verifier8= Function('_2reentrancy_verifier8',IntSort(),IntSort(),IntSort())
	d1FaddressTuint256mapping18= Function('d1FaddressTuint256mapping18',map,address,IntSort(),IntSort())
	i18= Function('i18',IntSort(),IntSort())
	TUPLE_018= Function('TUPLE_018',IntSort(),TUPLE)
	sent18= Function('sent18',IntSort(),BoolSort())
	_2reentrancy_verifier18= Function('_2reentrancy_verifier18',IntSort(),IntSort())
	bal18= Function('bal18',IntSort(),IntSort())
	_1reentrancy_verifier18= Function('_1reentrancy_verifier18',IntSort(),IntSort())
	_y34= Const('_y34',CONRT_EtherStore)
	_y35= Const('_y35',array)
	_y36= Const('_y36',array)
	_y37= Int('_y37')
	_y38= Const('_y38',address)
	_y39= Int('_y39')
	_y40= Const('_y40',TUPLE)
	_y41= Int('_y41')
	_y42= Const('_y42',map)
	_y43= Const('_y43',address)
	_y44= Const('_y44',CONRT_EtherStore)
	_n1= Int('_n1')
	_n2= Int('_n2')
	_y45= Const('_y45',map)
	_y46= Const('_y46',address)
	_y47= Const('_y47',CONRT_EtherStore)
	_y48= Const('_y48',CONRT_EtherStore)
	_y49= Const('_y49',CONRT_EtherStore)
	_y50= Const('_y50',CONRT_EtherStore)
	_y51= Const('_y51',CONRT_EtherStore)
	_y52= Const('_y52',map)
	_y53= Const('_y53',address)
	_y54= Const('_y54',CONRT_EtherStore)
	_N1= Function('_N1',IntSort(),IntSort())
	_y55= Const('_y55',CONRT_EtherStore)
	_y56= Const('_y56',map)
	_y57= Const('_y57',address)
	_y58= Const('_y58',CONRT_EtherStore)
	_y59= Const('_y59',CONRT_EtherStore)
	_y60= Const('_y60',CONRT_EtherStore)
	_y61= Const('_y61',CONRT_EtherStore)
	_y62= Const('_y62',CONRT_EtherStore)
	_y63= Const('_y63',CONRT_EtherStore)
	_y64= Const('_y64',map)
	_y65= Const('_y65',address)
	_N2= Int('_N2')
	_y66= Const('_y66',CONRT_EtherStore)
	_s=Solver()
	_s.set("timeout",50000)
	_s.add(ForAll([_y34],EtherStore_balances1(_y34)==EtherStore_balances(_y34)))
	_s.add(senders1==senders)
	_s.add(ForAll([_y35],array_length1(_y35)==array_length(_y35)))
	_s.add(ForAll([_y36,_y37],d1Taddressarray1(_y36,_y37)==d1Taddressarray(_y36,_y37)))
	_s.add(_1solidity1==_1solidity)
	_s.add(ForAll([_y38,_y39],LOW_LEVEL_CALL_call1(_y38,_y39)==LOW_LEVEL_CALL_call(_y38,_y39)))
	_s.add(ForAll([_y40,_y41],UNPACK2DTUPLETbool1(_y40,_y41)==UNPACK2DTUPLETbool(_y40,_y41)))
	_s.add(_2solidity1==_2solidity)
	_s.add(i1==i18(_N2))
	_s.add(ForAll([_y42,_y43],d1FaddressTuint256mapping1(_y42,_y43)==d1FaddressTuint256mapping18(_y42,_y43,_N2)))
	_s.add(bal1==bal18(_N2))
	_s.add(TUPLE_01==TUPLE_018(_N2))
	_s.add(sent1==sent18(_N2))
	_s.add(_1reentrancy_verifier1==_1reentrancy_verifier18(_N2))
	_s.add(_2reentrancy_verifier1==_2reentrancy_verifier18(_N2))
	_s.add(ForAll([_n2,_n1,_y44,_n1,_n2],Implies(And(_n2>=0,_n1>=0),i8(_n1 + 1,_n2)==If(((d1FaddressTuint256mapping8(EtherStore_balances(_y44),d1Taddressarray(senders,i8(_n1,_n2)),_n1,_n2))>(0)),If(((UNPACK2DTUPLETbool(LOW_LEVEL_CALL_call(d1Taddressarray(senders,i8(_n1,_n2)),d1FaddressTuint256mapping8(EtherStore_balances(_y44),d1Taddressarray(senders,i8(_n1,_n2)),_n1,_n2)),0))==(True)),i8(_n1, _n2) + 1,i8(_n1, _n2)),i8(_n1, _n2)))))
	_s.add(ForAll([_n2,_n1,_y45,_y46,_y47,_n1,_n2],Implies(And(_n2>=0,_n1>=0),d1FaddressTuint256mapping8(_y45,_y46,_n1 + 1,_n2)==If(((d1FaddressTuint256mapping8(EtherStore_balances(_y47),d1Taddressarray(senders,i8(_n1,_n2)),_n1,_n2))>(0)),If(((UNPACK2DTUPLETbool(LOW_LEVEL_CALL_call(d1Taddressarray(senders,i8(_n1,_n2)),d1FaddressTuint256mapping8(EtherStore_balances(_y47),d1Taddressarray(senders,i8(_n1,_n2)),_n1,_n2)),0))==(True)),If(And(((_y45)==(EtherStore_balances(_y47))),((_y46)==(d1Taddressarray(senders,i8(_n1,_n2))))),0,d1FaddressTuint256mapping8(_y45,_y46,_n1,_n2)),d1FaddressTuint256mapping8(_y45,_y46,_n1,_n2)),d1FaddressTuint256mapping8(_y45,_y46,_n1,_n2)))))
	_s.add(ForAll([_n2,_n1,_y48,_n1,_n2],Implies(And(_n2>=0,_n1>=0),bal8(_n1 + 1,_n2)==d1FaddressTuint256mapping8(EtherStore_balances(_y48),d1Taddressarray(senders,i8(_n1,_n2)),_n1,_n2))))
	_s.add(ForAll([_n2,_n1,_y49,_n1,_n2],Implies(And(_n2>=0,_n1>=0),TUPLE_08(_n1 + 1,_n2)==If(((d1FaddressTuint256mapping8(EtherStore_balances(_y49),d1Taddressarray(senders,i8(_n1,_n2)),_n1,_n2))>(0)),LOW_LEVEL_CALL_call(d1Taddressarray(senders,i8(_n1,_n2)),d1FaddressTuint256mapping8(EtherStore_balances(_y49),d1Taddressarray(senders,i8(_n1,_n2)),_n1,_n2)),TUPLE_08(_n1,_n2)))))
	_s.add(ForAll([_n2,_n1,_y50,_n1,_n2],Implies(And(_n2>=0,_n1>=0),sent8(_n1 + 1,_n2)==If(((d1FaddressTuint256mapping8(EtherStore_balances(_y50),d1Taddressarray(senders,i8(_n1,_n2)),_n1,_n2))>(0)),UNPACK2DTUPLETbool(LOW_LEVEL_CALL_call(d1Taddressarray(senders,i8(_n1,_n2)),d1FaddressTuint256mapping8(EtherStore_balances(_y50),d1Taddressarray(senders,i8(_n1,_n2)),_n1,_n2)),0),sent8(_n1,_n2)))))
	_s.add(ForAll([_n2,_n1,_y51,_n1,_n2],Implies(And(_n2>=0,_n1>=0),_2reentrancy_verifier8(_n1 + 1,_n2)==If(((d1FaddressTuint256mapping8(EtherStore_balances(_y51),d1Taddressarray(senders,i8(_n1,_n2)),_n1,_n2))>(0)),1,_2reentrancy_verifier8(_n1,_n2)))))
	_s.add(ForAll([_n2,_n2],Implies(_n2>=0,i8(0,_n2)==0)))
	_s.add(ForAll([_n2,_y52,_y53,_n2],Implies(_n2>=0,d1FaddressTuint256mapping8(_y52,_y53,0,_n2)==d1FaddressTuint256mapping18(_y52,_y53,_n2))))
	_s.add(ForAll([_n2,_y54,_n2],Implies(_n2>=0,bal8(0,_n2)==d1FaddressTuint256mapping18(EtherStore_balances(_y54),d1Taddressarray(senders,i18(_n2)),_n2))))
	_s.add(ForAll([_n2,_n2],Implies(_n2>=0,TUPLE_08(0,_n2)==TUPLE_018(_n2))))
	_s.add(ForAll([_n2,_n2],Implies(_n2>=0,sent8(0,_n2)==sent18(_n2))))
	_s.add(ForAll([_n2,_n2],Implies(_n2>=0,_2reentrancy_verifier8(0,_n2)==_2reentrancy_verifier18(_n2))))
	_s.add(ForAll([_n2],Implies(_n2>=0,((((i8(_N1(_n2), _n2))>=(array_length(senders))))!=(True)))))
	_s.add(ForAll([_n2,_n1,_n1],Implies(And(_n2>=0,_n1>=0),Implies(_n1 < _N1(_n2),((((i8(_n1, _n2))<(array_length(senders))))==(True))))))
	_s.add(ForAll([_n2],Implies(_n2>=0,_N1(_n2) >= 0)))
	_s.add(ForAll([_n2,_y55,_n2],Implies(_n2>=0,i18(_n2 + 1)==If(((d1FaddressTuint256mapping18(EtherStore_balances(_y55),d1Taddressarray(senders,i18(_n2)),_n2))>(0)),If(((UNPACK2DTUPLETbool(LOW_LEVEL_CALL_call(d1Taddressarray(senders,i8(_N1(_n2),_n2)),bal8(_N1(_n2),_n2)),0))==(True)),i8(_N1(_n2), _n2) + 1,i8(_N1(_n2), _n2)),i18(_n2)))))
	_s.add(ForAll([_n2,_y56,_y57,_y58,_n2],Implies(_n2>=0,d1FaddressTuint256mapping18(_y56,_y57,_n2 + 1)==If(((d1FaddressTuint256mapping18(EtherStore_balances(_y58),d1Taddressarray(senders,i18(_n2)),_n2))>(0)),If(((UNPACK2DTUPLETbool(LOW_LEVEL_CALL_call(d1Taddressarray(senders,i8(_N1(_n2),_n2)),bal8(_N1(_n2),_n2)),0))==(True)),If(And(((_y56)==(EtherStore_balances(_y58))),((_y57)==(d1Taddressarray(senders,i8(_N1(_n2),_n2))))),0,d1FaddressTuint256mapping8(_y56,_y57,_N1(_n2),_n2)),d1FaddressTuint256mapping8(_y56,_y57,_N1(_n2),_n2)),d1FaddressTuint256mapping18(_y56,_y57,_n2)))))
	_s.add(ForAll([_n2,_y59,_n2],Implies(_n2>=0,bal18(_n2 + 1)==If(((d1FaddressTuint256mapping18(EtherStore_balances(_y59),d1Taddressarray(senders,i18(_n2)),_n2))>(0)),bal8(_N1(_n2), _n2),d1FaddressTuint256mapping18(EtherStore_balances(_y59),d1Taddressarray(senders,i18(_n2)),_n2)))))
	_s.add(ForAll([_n2,_y60,_n2],Implies(_n2>=0,TUPLE_018(_n2 + 1)==If(((d1FaddressTuint256mapping18(EtherStore_balances(_y60),d1Taddressarray(senders,i18(_n2)),_n2))>(0)),LOW_LEVEL_CALL_call(d1Taddressarray(senders,i8(_N1(_n2),_n2)),bal8(_N1(_n2),_n2)),TUPLE_018(_n2)))))
	_s.add(ForAll([_n2,_y61,_n2],Implies(_n2>=0,sent18(_n2 + 1)==If(((d1FaddressTuint256mapping18(EtherStore_balances(_y61),d1Taddressarray(senders,i18(_n2)),_n2))>(0)),UNPACK2DTUPLETbool(LOW_LEVEL_CALL_call(d1Taddressarray(senders,i8(_N1(_n2),_n2)),bal8(_N1(_n2),_n2)),0),sent18(_n2)))))
	_s.add(ForAll([_n2,_y62,_n2],Implies(_n2>=0,_1reentrancy_verifier18(_n2 + 1)==If(((d1FaddressTuint256mapping18(EtherStore_balances(_y62),d1Taddressarray(senders,i18(_n2)),_n2))>(0)),1,_1reentrancy_verifier18(_n2)))))
	_s.add(ForAll([_n2,_y63,_n2],Implies(_n2>=0,_2reentrancy_verifier18(_n2 + 1)==If(((d1FaddressTuint256mapping18(EtherStore_balances(_y63),d1Taddressarray(senders,i18(_n2)),_n2))>(0)),_2reentrancy_verifier8(_N1(_n2),_n2),_2reentrancy_verifier18(_n2)))))
	_s.add(i18(0)==0)
	_s.add(ForAll([_y64,_y65],d1FaddressTuint256mapping18(_y64,_y65,0)==d1FaddressTuint256mapping(_y64,_y65)))
	_s.add(bal18(0)==bal)
	_s.add(TUPLE_018(0)==TUPLE_0)
	_s.add(sent18(0)==sent)
	_s.add(_1reentrancy_verifier18(0)==0)
	_s.add(_2reentrancy_verifier18(0)==0)
	_s.add(((((i18(_N2))>=(array_length(senders))))!=(True)))
	_s.add(ForAll([_n2,_n2],Implies(_n2>=0,Implies(_n2 < _N2,((((i18(_n2))<(array_length(senders))))==(True))))))
	_s.add(_N2 >= 0)
	_s.add(ForAll([_n2],Implies(_n2>=0,((_N1(_n2))==(2)))))
	_s.add(((_N2)==(2)))
	_s.add(ForAll([_n2,_y66],Implies(_n2>=0,((d1FaddressTuint256mapping18(EtherStore_balances(_y66),d1Taddressarray(senders,i18(_n2)),_n2))>(0)))))
	_s.add(Not(And((_1reentrancy_verifier1==1),(_2reentrancy_verifier1==1))))
except Exception as e:
	print("Error1(Z3Query)"+str(e))
	sys.exit(1)
try:
	result=_s.check()
	if sat==result:
		print("Counter Example")
		print (_s.model())
	elif unsat==result:
		print ("Successfully Proved")
	else:
		print("Failed To Prove")
except Exception as e:
	print("Error2(Z3Query)"+str(e))
	sys.exit(1)