import json
import sys
import os
currentdirectory = os.path.dirname(os.path.realpath(__file__))
import logging
import subprocess
from collections import defaultdict
from crytic_compile.cryticparser.defaults import (
    DEFAULTS_FLAG_IN_CONFIG as DEFAULTS_FLAG_IN_CONFIG_CRYTIC_COMPILE,
)

from slither.detectors.abstract_detector import classification_txt
from slither.utils.colors import yellow, red
from slither.utils.myprettytable import MyPrettyTable

logger = logging.getLogger("Slither")

# Those are the flags shared by the command line and the config file
defaults_flag_in_config_translator = {
    "translators_to_run": None,
}

    
def output_translators(translators_classes):
    translators_list = []
    for translator in translators_classes:
        argument = translator.ARGUMENT
        help_info = translator.HELP
        translator_list.append((argument, help_info))
    table = MyPrettyTable(["Num", "Translator", "What it Does"])

    # Sort by impact, confidence, and name
    translators_list = sorted(translators_list, key=lambda element: (element[0]))
    idx = 1
    for (argument, help_info) in translators_list:
        table.add_row([idx, argument, help_info])
        idx = idx + 1
    print(table)


def output_translators_json(translator_classes):
    translators_list = []
    for translator in translators_classes:
        argument = translator.ARGUMENT
        help_info = translator.HELP

        translators_list.append((argument, help_info))

    # Sort by name
    translators_list = sorted(translators_list, key=lambda element: (element[0]))
    idx = 1
    table = []
    for (argument, help_info) in translators_list:
        table.append({"index": idx, "check": argument, "title": help_info})
        idx = idx + 1
    return table
    
    
    
def run_command():
        status=None
        try :
            proc = subprocess.Popen('python3 '+currentdirectory+'/z3query.py', stdout=subprocess.PIPE,shell=True)
            output = proc.stdout.read()
            status=output
        except OSError  as err:
            print('Not able to run')
            
        return status

