
import sys
import os
currentdirectory = os.path.dirname(os.path.realpath(__file__))
"""
Reading the contain of the file 
"""
def readingFile( filename ):
    content=None
    with open(currentdirectory+"/"+filename) as f:
        content = f.readlines()
    return content
 
"""
Wrtitting the contain on file 
"""
def writtingFile( filename , content ):
	file = open(currentdirectory+"/"+filename, "w")
	file.write(str(content))
	file.close()

"""
Appending the contain on file 
"""
def appendingFile( filename , content ):
	file = open(currentdirectory+"/"+filename, "a")
	file.write(str(content))
	file.close()

