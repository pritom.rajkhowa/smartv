// SPDX-License-Identifier: MIT
pragma solidity >=0.5.0 <0.9.0;


contract IfElse {
    function foo(uint x) public pure returns (uint) {
    
        x=x+1;
        if (x < 10) {
            x++;
            
        } else if (x < 20) {
            x++;
            
        } else {
            x++;
            
        }
        
        return x;
    }
}
