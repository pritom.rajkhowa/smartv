// SPDX-License-Identifier: MIT
pragma solidity >=0.5.0 <0.9.0;

contract Loop {
    function loop() public {
        // for loop
        for (uint i = 0; i < 10; i++) {
        
           i=i+2;
           i=i-1;
           i=i-1;
    
        }

        // while loop
        uint j;
        while (j < 10) {
            j++;
            j=j+2;
            j=j-1;
            j=j-1;
        }
    }
}
