0
======================////////////////////////////
	if(((total<=100)==True)){
		bal=d1FaddressTuint256mapping(EtherStore_balances(_c1),msg_sender)
		if((bal>0)){
				d1FaddressTuint256mapping(EtherStore_balances(_c1),msg_sender)=0
				_1reentrancy_verifier=1
				if(((total<=100)==True)){
								bal=d1FaddressTuint256mapping(EtherStore_balances(_c1),msg_sender)
								if((bal>0)){
																d1FaddressTuint256mapping(EtherStore_balances(_c1),msg_sender)=0
																_2reentrancy_verifier=1
																TUPLE_0=LOW_LEVEL_CALL_call(msg_sender,bal)
																sent=UNPACK2DTUPLETbool(TUPLE_0,0)
																if(sent){
																																total=(total+2)
																}
								}
				} else {
								total=(total+1)
				}
				TUPLE_0=LOW_LEVEL_CALL_call(msg_sender,bal)
				sent=UNPACK2DTUPLETbool(TUPLE_0,0)
				if(sent){
								total=(total+2)
				}
		}
	} else {
		total=(total+1)
	}
======================////////////////////////////
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Contract : EtherStore
Type : contract
FUNCTION NAME : EtherStore_withdraw
FUNCTION DEFINITION : CONRT_EtherStore : _c1 X uint256 : total --> void
Contract : EtherStore

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
============================
----------------------------
Output for 

~~~~~~~~~~~~~~Details Start~~~~~~~~~~~~~~~~~
EtherStore_balances : CONRT_EtherStore --> map
total : uint256
msg_sender : address
d1FaddressTuint256mapping : map X address --> uint256
bal : uint256
_1solidity : void
TUPLE_0 : TUPLE
LOW_LEVEL_CALL_call : address X uint256 --> TUPLE
sent : bool
UNPACK2DTUPLETbool : TUPLE X uint256 --> bool
_2solidity : void
_1reentrancy_verifier : uint256
_2reentrancy_verifier : uint256
IsEtherStore : CONRT_EtherStore --> boolean
~~~~~~~~~~~~~~Details End~~~~~~~~~~~~~~~~~
~~~~~~~~~~~~~~Translation Start~~~~~~~~~~~~~~~~~

Output in normal notation:

1. Frame axioms:
IsEtherStore(_x1) -> EtherStore_balances1(_x1) = EtherStore_balances(_x1)
_1solidity1 = _1solidity
LOW_LEVEL_CALL_call1(_x1,_x2) = LOW_LEVEL_CALL_call(_x1,_x2)
UNPACK2DTUPLETbool1(_x1,_x2) = UNPACK2DTUPLETbool(_x1,_x2)
_2solidity1 = _2solidity
_1user_assert1(_x1) = _1user_assert(_x1)
_2user_assert1(_x1) = _2user_assert(_x1)


2. Output equations:
IsEtherStore(_c1) -> total1 = ite(((total<=100)==True),ite((d1FaddressTuint256mapping(EtherStore_balances(_c1),msg_sender)>0),ite(UNPACK2DTUPLETbool(LOW_LEVEL_CALL_call(msg_sender,ite(((total<=100)==True),ite(((EtherStore_balances(_c1)=EtherStore_balances(_c1)) and (msg_sender=msg_sender)),0,d1FaddressTuint256mapping(EtherStore_balances(_c1),msg_sender)),d1FaddressTuint256mapping(EtherStore_balances(_c1),msg_sender))),0),(ite(((total<=100)==True),ite((ite(((EtherStore_balances(_c1)=EtherStore_balances(_c1)) and (msg_sender=msg_sender)),0,d1FaddressTuint256mapping(EtherStore_balances(_c1),msg_sender))>0),ite(UNPACK2DTUPLETbool(LOW_LEVEL_CALL_call(msg_sender,ite(((EtherStore_balances(_c1)=EtherStore_balances(_c1)) and (msg_sender=msg_sender)),0,d1FaddressTuint256mapping(EtherStore_balances(_c1),msg_sender))),0),(total+2),total),total),(total+1))+2),ite(((total<=100)==True),ite((ite(((EtherStore_balances(_c1)=EtherStore_balances(_c1)) and (msg_sender=msg_sender)),0,d1FaddressTuint256mapping(EtherStore_balances(_c1),msg_sender))>0),ite(UNPACK2DTUPLETbool(LOW_LEVEL_CALL_call(msg_sender,ite(((EtherStore_balances(_c1)=EtherStore_balances(_c1)) and (msg_sender=msg_sender)),0,d1FaddressTuint256mapping(EtherStore_balances(_c1),msg_sender))),0),(total+2),total),total),(total+1))),total),(total+1))
IsEtherStore(_c1) -> d1FaddressTuint256mapping1(_x1,_x2) = ite(((total<=100)==True),ite((d1FaddressTuint256mapping(EtherStore_balances(_c1),msg_sender)>0),ite(((total<=100)==True),ite((ite(((EtherStore_balances(_c1)=EtherStore_balances(_c1)) and (msg_sender=msg_sender)),0,d1FaddressTuint256mapping(EtherStore_balances(_c1),msg_sender))>0),ite(((_x1=EtherStore_balances(_c1)) and (_x2=msg_sender)),0,ite(((_x1=EtherStore_balances(_c1)) and (_x2=msg_sender)),0,d1FaddressTuint256mapping(_x1,_x2))),ite(((_x1=EtherStore_balances(_c1)) and (_x2=msg_sender)),0,d1FaddressTuint256mapping(_x1,_x2))),ite(((_x1=EtherStore_balances(_c1)) and (_x2=msg_sender)),0,d1FaddressTuint256mapping(_x1,_x2))),d1FaddressTuint256mapping(_x1,_x2)),d1FaddressTuint256mapping(_x1,_x2))
IsEtherStore(_c1) -> bal1 = ite(((total<=100)==True),ite((d1FaddressTuint256mapping(EtherStore_balances(_c1),msg_sender)>0),ite(((total<=100)==True),ite(((EtherStore_balances(_c1)=EtherStore_balances(_c1)) and (msg_sender=msg_sender)),0,d1FaddressTuint256mapping(EtherStore_balances(_c1),msg_sender)),d1FaddressTuint256mapping(EtherStore_balances(_c1),msg_sender)),d1FaddressTuint256mapping(EtherStore_balances(_c1),msg_sender)),bal)
IsEtherStore(_c1) -> TUPLE_01 = ite(((total<=100)==True),ite((d1FaddressTuint256mapping(EtherStore_balances(_c1),msg_sender)>0),LOW_LEVEL_CALL_call(msg_sender,ite(((total<=100)==True),ite(((EtherStore_balances(_c1)=EtherStore_balances(_c1)) and (msg_sender=msg_sender)),0,d1FaddressTuint256mapping(EtherStore_balances(_c1),msg_sender)),d1FaddressTuint256mapping(EtherStore_balances(_c1),msg_sender))),TUPLE_0),TUPLE_0)
IsEtherStore(_c1) -> sent1 = ite(((total<=100)==True),ite((d1FaddressTuint256mapping(EtherStore_balances(_c1),msg_sender)>0),UNPACK2DTUPLETbool(LOW_LEVEL_CALL_call(msg_sender,ite(((total<=100)==True),ite(((EtherStore_balances(_c1)=EtherStore_balances(_c1)) and (msg_sender=msg_sender)),0,d1FaddressTuint256mapping(EtherStore_balances(_c1),msg_sender)),d1FaddressTuint256mapping(EtherStore_balances(_c1),msg_sender))),0),sent),sent)
IsEtherStore(_c1) -> _1reentrancy_verifier1 = ite(((total<=100)==True),ite((d1FaddressTuint256mapping(EtherStore_balances(_c1),msg_sender)>0),1,0),0)
IsEtherStore(_c1) -> _2reentrancy_verifier1 = ite(((total<=100)==True),ite((d1FaddressTuint256mapping(EtherStore_balances(_c1),msg_sender)>0),ite(((total<=100)==True),ite((ite(((EtherStore_balances(_c1)=EtherStore_balances(_c1)) and (msg_sender=msg_sender)),0,d1FaddressTuint256mapping(EtherStore_balances(_c1),msg_sender))>0),1,0),0),0),0)


3. Other axioms:

4. Assumption :


5. Assertion :


5. User Defined Assertion :


~~~~~~~~~~~~~~Translation End~~~~~~~~~~~~~~~~~

FUNCTION IS SINGLE REENTERNCEY
