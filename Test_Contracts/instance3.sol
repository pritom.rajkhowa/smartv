pragma solidity >=0.5.0 <0.9.0;

contract C {
    uint private data;
    
    uint public D;

    function f(uint a) private pure returns(uint b) {  a = a + a + 1; return a; }
    function setData(uint a) public { D = D+1; data = a; }
    function getData() public view returns(uint) {  return data; }
    function compute(uint a, uint b) internal pure returns (uint) { return a + b; }
}


