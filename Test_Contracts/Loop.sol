// SPDX-License-Identifier: MIT
pragma solidity >=0.5.0 <0.9.0;

contract Loop {
    function loop() public {
        // for loop
        for (uint i = 0; i < 10; i++) {
    
        }

        // while loop
        uint j;
        while (j < 10) {
            j++;
        }
    }
}
