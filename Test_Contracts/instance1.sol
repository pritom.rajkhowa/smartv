// SPDX-License-Identifier: MIT
pragma solidity >=0.5.0 <0.9.0;

contract C {
    uint public data = 42;
}

contract Caller {
    C c = new C();
    function f() public view returns (uint) {
        return c.data();
    }
}
