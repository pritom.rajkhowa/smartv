~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Contract : EtherStore
Type : contract
FUNCTION NAME : EtherStore_deposit
FUNCTION DEFINITION : CONRT_EtherStore : _c1 --> void
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Output for 

~~~~~~~~~~~~~~Details Start~~~~~~~~~~~~~~~~~
EtherStore_balances : CONRT_EtherStore --> map
msg_sender : address
d1FaddressTuint256mapping : map X address --> uint256
msg_value : uint256
IsEtherStore : CONRT_EtherStore --> boolean
~~~~~~~~~~~~~~Details End~~~~~~~~~~~~~~~~~

~~~~~~~~~~~~~~Translation Start~~~~~~~~~~~~~~~~~


Output in normal notation:

1. Frame axioms:
IsEtherStore(_x1) -> EtherStore_balances1(_x1) = EtherStore_balances(_x1)
msg_sender1 = msg_sender
msg_value1 = msg_value


2. Output equations:
IsEtherStore(_c1) -> d1FaddressTuint256mapping1(_x1,_x2) = ite(((_x1=EtherStore_balances(_c1)) and (_x2=msg_sender)),(d1FaddressTuint256mapping(EtherStore_balances(_c1),msg_sender)+msg_value),d1FaddressTuint256mapping(_x1,_x2))


3. Other axioms:

4. Assumption :


5. Assertion :


5. User Defined Assertion :


~~~~~~~~~~~~~~Translation End~~~~~~~~~~~~~~~~~

=======================================================================================================
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Contract : EtherStore
Type : contract
FUNCTION NAME : EtherStore_withdraw
FUNCTION DEFINITION : CONRT_EtherStore : _c1 X address : sender1 X address : sender2 --> void
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Output for 

~~~~~~~~~~~~~~Details Start~~~~~~~~~~~~~~~~~
EtherStore_balances : CONRT_EtherStore --> map
sender1 : address
d1FaddressTuint256mapping : map X address --> uint256
bal : uint256
_1solidity : void
TUPLE_0 : TUPLE
LOW_LEVEL_CALL_call : address X string --> TUPLE
sent1 : bool
UNPACK2DTUPLETbool : TUPLE X uint256 --> bool
_2solidity : void
sender2 : address
_3solidity : void
TUPLE_1 : TUPLE
sent2 : bool
_4solidity : void
_1IsReentrancy : uint256
_1reentrancy : uint256
_2IsReentrancy : uint256
_2reentrancy : uint256
IsEtherStore : CONRT_EtherStore --> boolean
~~~~~~~~~~~~~~Details End~~~~~~~~~~~~~~~~~

~~~~~~~~~~~~~~Translation Start~~~~~~~~~~~~~~~~~


Output in normal notation:

1. Frame axioms:
IsEtherStore(_x1) -> EtherStore_balances1(_x1) = EtherStore_balances(_x1)
sender11 = sender1
LOW_LEVEL_CALL_call1(_x1,_x2) = LOW_LEVEL_CALL_call(_x1,_x2)
UNPACK2DTUPLETbool1(_x1,_x2) = UNPACK2DTUPLETbool(_x1,_x2)
sender21 = sender2


2. Output equations:
IsEtherStore(_c1) -> d1FaddressTuint256mapping1(_x1,_x2) = ite(((_x1=EtherStore_balances(_c1)) and (_x2=sender2)),0,ite(((_x1=EtherStore_balances(_c1)) and (_x2=sender1)),0,d1FaddressTuint256mapping(_x1,_x2)))
IsEtherStore(_c1) -> bal1 = ite(((EtherStore_balances(_c1)=EtherStore_balances(_c1)) and (sender2=sender1)),0,d1FaddressTuint256mapping(EtherStore_balances(_c1),sender2))
IsEtherStore(_c1) -> TUPLE_01 = LOW_LEVEL_CALL_call(sender1,d1FaddressTuint256mapping(EtherStore_balances(_c1),sender1),"")
IsEtherStore(_c1) -> sent11 = UNPACK2DTUPLETbool(LOW_LEVEL_CALL_call(sender1,d1FaddressTuint256mapping(EtherStore_balances(_c1),sender1),""),0)
IsEtherStore(_c1) -> TUPLE_11 = LOW_LEVEL_CALL_call(sender2,ite(((EtherStore_balances(_c1)=EtherStore_balances(_c1)) and (sender2=sender1)),0,d1FaddressTuint256mapping(EtherStore_balances(_c1),sender2)),"")
IsEtherStore(_c1) -> sent21 = UNPACK2DTUPLETbool(LOW_LEVEL_CALL_call(sender2,ite(((EtherStore_balances(_c1)=EtherStore_balances(_c1)) and (sender2=sender1)),0,d1FaddressTuint256mapping(EtherStore_balances(_c1),sender2)),""),0)
IsEtherStore(_c1) -> _1IsReentrancy1 = ite((_1reentrancy==d1FaddressTuint256mapping(EtherStore_balances(_c1),sender1)),0,-1)
IsEtherStore(_c1) -> _1reentrancy1 = (d1FaddressTuint256mapping(EtherStore_balances(_c1),sender1)-ite(((EtherStore_balances(_c1)=EtherStore_balances(_c1)) and (sender1=sender1)),0,d1FaddressTuint256mapping(EtherStore_balances(_c1),sender1)))
IsEtherStore(_c1) -> _2IsReentrancy1 = ite((_2reentrancy==ite(((EtherStore_balances(_c1)=EtherStore_balances(_c1)) and (sender2=sender1)),0,d1FaddressTuint256mapping(EtherStore_balances(_c1),sender2))),0,-1)
IsEtherStore(_c1) -> _2reentrancy1 = (ite(((EtherStore_balances(_c1)=EtherStore_balances(_c1)) and (sender2=sender1)),0,d1FaddressTuint256mapping(EtherStore_balances(_c1),sender2))-ite(((EtherStore_balances(_c1)=EtherStore_balances(_c1)) and (sender2=sender2)),0,ite(((EtherStore_balances(_c1)=EtherStore_balances(_c1)) and (sender2=sender1)),0,d1FaddressTuint256mapping(EtherStore_balances(_c1),sender2))))


3. Other axioms:

4. Assumption :
IsEtherStore(_c1) -> _1solidity1 = SOLIDITY_CALL_require((d1FaddressTuint256mapping(EtherStore_balances(_c1),sender1)>0))
IsEtherStore(_c1) -> _2solidity1 = SOLIDITY_CALL_require(UNPACK2DTUPLETbool(LOW_LEVEL_CALL_call(sender1,d1FaddressTuint256mapping(EtherStore_balances(_c1),sender1),""),0),"Failed to send Ether to Sender1")
IsEtherStore(_c1) -> _3solidity1 = SOLIDITY_CALL_require((ite(((EtherStore_balances(_c1)=EtherStore_balances(_c1)) and (sender2=sender1)),0,d1FaddressTuint256mapping(EtherStore_balances(_c1),sender2))>0))
IsEtherStore(_c1) -> _4solidity1 = SOLIDITY_CALL_require(UNPACK2DTUPLETbool(LOW_LEVEL_CALL_call(sender2,ite(((EtherStore_balances(_c1)=EtherStore_balances(_c1)) and (sender2=sender1)),0,d1FaddressTuint256mapping(EtherStore_balances(_c1),sender2)),""),0),"Failed to send Ether to Sender2")


5. Assertion :


5. User Defined Assertion :
IsEtherStore(_c1) -> (ite((_1reentrancy==d1FaddressTuint256mapping(EtherStore_balances(_c1),sender1)),0,-1)==0)
IsEtherStore(_c1) -> (ite((_2reentrancy==ite(((EtherStore_balances(_c1)=EtherStore_balances(_c1)) and (sender2=sender1)),0,d1FaddressTuint256mapping(EtherStore_balances(_c1),sender2))),0,-1)==0)


~~~~~~~~~~~~~~Translation End~~~~~~~~~~~~~~~~~

=======================================================================================================
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Contract : EtherStore
Type : contract
FUNCTION NAME : EtherStore_getBalance
FUNCTION DEFINITION : CONRT_EtherStore : _c1-->  uint256
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Output for 

~~~~~~~~~~~~~~Details Start~~~~~~~~~~~~~~~~~
EtherStore_balances : CONRT_EtherStore --> map
this : CONRT_EtherStore
convertToaddress : string --> address
BALANCE : address --> uint256
ret : uint256
IsEtherStore : CONRT_EtherStore --> boolean
~~~~~~~~~~~~~~Details End~~~~~~~~~~~~~~~~~

~~~~~~~~~~~~~~Translation Start~~~~~~~~~~~~~~~~~


Output in normal notation:

1. Frame axioms:
IsEtherStore(_x1) -> EtherStore_balances1(_x1) = EtherStore_balances(_x1)
this1 = this
convertToaddress1(_x1) = convertToaddress(_x1)
BALANCE1(_x1) = BALANCE(_x1)


2. Output equations:
ret1 = BALANCE(convertToaddress(this))


3. Other axioms:

4. Assumption :


5. Assertion :


5. User Defined Assertion :


~~~~~~~~~~~~~~Translation End~~~~~~~~~~~~~~~~~

=======================================================================================================
