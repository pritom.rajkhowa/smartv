// SPDX-License-Identifier: MIT
pragma solidity ^0.8.3;

/*
EtherStore is a contract where you can deposit and withdraw ETH.
This contract is vulnerable to re-entrancy attack.
Let's see why.

1. Deploy EtherStore
2. Deposit 1 Ether each from Account 1 (Alice) and Account 2 (Bob) into EtherStore
3. Deploy Attack with address of EtherStore
4. Call Attack.attack sending 1 ether (using Account 3 (Eve)).
   You will get 3 Ethers back (2 Ether stolen from Alice and Bob,
   plus 1 Ether sent from this contract).

What happened?
Attack was able to call EtherStore.withdraw multiple times before
EtherStore.withdraw finished executing.

Here is how the functions were called
- Attack.attack
- EtherStore.deposit
- EtherStore.withdraw
- Attack fallback (receives 1 Ether)
- EtherStore.withdraw
- Attack.fallback (receives 1 Ether)
- EtherStore.withdraw
- Attack fallback (receives 1 Ether)
*/

contract EtherStore {
    mapping(address => uint) public balances;

    function deposit() public payable {
        balances[msg.sender] += msg.value;
    }

    function withdraw(address sender1, address sender2) public {
    
           uint bal = balances[sender1];
           require(bal > 0);
           
           balances[sender1] = 0;

           (bool sent1, ) = sender1.call{value: bal}("");
           require(sent1, "Failed to send Ether to Sender1");
           
           //balances[sender1] = 0;
           
           bal = balances[sender2];
           require(bal > 0);
           
           balances[sender2] = 0;

           (bool sent2, ) = sender2.call{value: bal}("");
           require(sent2, "Failed to send Ether to Sender2");
           
           //balances[sender2] = 0;


           
    }

    // Helper function to check the balance of this contract
    function getBalance() public view returns (uint) {
        return address(this).balance;
    }
}


