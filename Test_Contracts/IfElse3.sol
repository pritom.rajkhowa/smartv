// SPDX-License-Identifier: MIT
pragma solidity >=0.5.0 <0.9.0;


contract IfElse {
    function foo(uint x) public pure returns (uint) {
        x=x+1;
        if (x < 10) {
            x++;
            return 0;
        } else {
            x++;
            return 2;
        }
        x=x+2;
    }
}
