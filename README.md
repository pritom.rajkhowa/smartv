# smartV

The goal of this project is to design and implement a fully automatic smart contract verifier which can be easily integrated with the popular Solidity smart contract development environment.

The automatic verification tool is SmartV. It focuses to detect all known vulnerabilities using a formal verification technique.

### System Requirements and Installation

In practice we have run smartV on standard Mac OS and Ubuntu 20.04 LTS distribution. smartV is provided as a set of binaries and libraries for Mac Os and Ubuntu 20.04 LTS distribution. 

#### Download ##### 

Clone over HTTPS: $ git clone https://gitlab.com/pritom.rajkhowa/test-smartv.git


#### Running 

smartV software verifier is run using the `smartV.py` tool in the smartV directory.For a given input solidity program, the tool checks for all known vulnerabilities in input program. 

#### Running Command

PATH_TO_smartV/smartV.py file


### Detectors


Num | Detector | What it Detects | Impact | Confidence
--- | --- | --- | --- | ---
1 | `abiencoderv2-array` | [Storage abiencoderv2 array](https://gitlab.com/pritom.rajkhowa/test-smartv/-/wikis/Detector-Documentation#storage-abiencoderv2-array) | High | High
2 | `array-by-reference` | [Modifying storage array by value](https://gitlab.com/pritom.rajkhowa/test-smartv/-/wikis/Detector-Documentation#modifying-storage-array-by-value) | High | High
3 | `incorrect-shift` | [The order of parameters in a shift instruction is incorrect.](https://gitlab.com/pritom.rajkhowa/test-smartv/-/wikis/Detector-Documentation#shift-parameter-mixup) | High | High
4 | `multiple-constructors` | [Multiple constructor schemes](https://gitlab.com/pritom.rajkhowa/test-smartv/-/wikis/Detector-Documentation#multiple-constructor-schemes) | High | High
5 | `name-reused` | [Contract's name reused](https://gitlab.com/pritom.rajkhowa/test-smartv/-/wikis/Detector-Documentation#name-reused) | High | High
6 | `public-mappings-nested` | [Public mappings with nested variables](https://gitlab.com/pritom.rajkhowa/test-smartv/-/wikis/Detector-Documentation#public-mappings-with-nested-variables) | High | High
7 | `rtlo` | [Right-To-Left-Override control character is used](https://gitlab.com/pritom.rajkhowa/test-smartv/-/wikis/Detector-Documentation#right-to-left-override-character) | High | High
8 | `shadowing-state` | [State variables shadowing](https://gitlab.com/pritom.rajkhowa/test-smartv/-/wikis/Detector-Documentation#state-variable-shadowing) | High | High
9 | `suicidal` | [Functions allowing anyone to destruct the contract](https://gitlab.com/pritom.rajkhowa/test-smartv/-/wikis/Detector-Documentation#suicidal) | High | High
10 | `uninitialized-state` | [Uninitialized state variables](https://gitlab.com/pritom.rajkhowa/test-smartv/-/wikis/Detector-Documentation#uninitialized-state-variables) | High | High
11 | `uninitialized-storage` | [Uninitialized storage variables](https://gitlab.com/pritom.rajkhowa/test-smartv/-/wikis/Detector-Documentation#uninitialized-storage-variables) | High | High
12 | `unprotected-upgrade` | [Unprotected upgradeable contract](https://gitlab.com/pritom.rajkhowa/test-smartv/-/wikis/Detector-Documentation#unprotected-upgradeable-contract) | High | High
13 | `arbitrary-send` | [Functions that send Ether to arbitrary destinations](https://gitlab.com/pritom.rajkhowa/test-smartv/-/wikis/Detector-Documentation#functions-that-send-ether-to-arbitrary-destinations) | High | Medium
14 | `controlled-array-length` | [Tainted array length assignment](https://gitlab.com/pritom.rajkhowa/test-smartv/-/wikis/Detector-Documentation#array-length-assignment) | High | Medium
15 | `controlled-delegatecall` | [Controlled delegatecall destination](https://gitlab.com/pritom.rajkhowa/test-smartv/-/wikis/Detector-Documentation#controlled-delegatecall) | High | Medium
16 | `reentrancy-eth` | [Reentrancy vulnerabilities (theft of ethers)](https://gitlab.com/pritom.rajkhowa/test-smartv/-/wikis/Detector-Documentation#reentrancy-vulnerabilities) | High | Medium
17 | `storage-array` | [Signed storage integer array compiler bug](https://gitlab.com/pritom.rajkhowa/test-smartv/-/wikis/Detector-Documentation#storage-signed-integer-array) | High | Medium
18 | `unchecked-transfer` | [Unchecked tokens transfer](https://gitlab.com/pritom.rajkhowa/test-smartv/-/wikis/Detector-Documentation#unchecked-transfer) | High | Medium
19 | `weak-prng` | [Weak PRNG](https://gitlab.com/pritom.rajkhowa/test-smartv/-/wikis/Detector-Documentation#weak-PRNG) | High | Medium
20 | `enum-conversion` | [Detect dangerous enum conversion](https://gitlab.com/pritom.rajkhowa/test-smartv/-/wikis/Detector-Documentation#dangerous-enum-conversion) | Medium | High
21 | `erc20-interface` | [Incorrect ERC20 interfaces](https://gitlab.com/pritom.rajkhowa/test-smartv/-/wikis/Detector-Documentation#incorrect-erc20-interface) | Medium | High
22 | `erc721-interface` | [Incorrect ERC721 interfaces](https://gitlab.com/pritom.rajkhowa/test-smartv/-/wikis/Detector-Documentation#incorrect-erc721-interface) | Medium | High
23 | `incorrect-equality` | [Dangerous strict equalities](https://gitlab.com/pritom.rajkhowa/test-smartv/-/wikis/Detector-Documentation#dangerous-strict-equalities) | Medium | High
24 | `locked-ether` | [Contracts that lock ether](https://gitlab.com/pritom.rajkhowa/test-smartv/-/wikis/Detector-Documentation#contracts-that-lock-ether) | Medium | High
25 | `mapping-deletion` | [Deletion on mapping containing a structure](https://gitlab.com/pritom.rajkhowa/test-smartv/-/wikis/Detector-Documentation#deletion-on-mapping-containing-a-structure) | Medium | High
26 | `shadowing-abstract` | [State variables shadowing from abstract contracts](https://gitlab.com/pritom.rajkhowa/test-smartv/-/wikis/Detector-Documentation#state-variable-shadowing-from-abstract-contracts) | Medium | High
27 | `tautology` | [Tautology or contradiction](https://gitlab.com/pritom.rajkhowa/test-smartv/-/wikis/Detector-Documentation#tautology-or-contradiction) | Medium | High
28 | `write-after-write` | [Unused write](https://gitlab.com/pritom.rajkhowa/test-smartv/-/wikis/Detector-Documentation#write-after-write) | Medium | High
29 | `boolean-cst` | [Misuse of Boolean constant](https://gitlab.com/pritom.rajkhowa/test-smartv/-/wikis/Detector-Documentation#misuse-of-a-boolean-constant) | Medium | Medium
30 | `constant-function-asm` | [Constant functions using assembly code](https://gitlab.com/pritom.rajkhowa/test-smartv/-/wikis/Detector-Documentation#constant-functions-using-assembly-code) | Medium | Medium
31 | `constant-function-state` | [Constant functions changing the state](https://gitlab.com/pritom.rajkhowa/test-smartv/-/wikis/Detector-Documentation#constant-functions-changing-the-state) | Medium | Medium
32 | `divide-before-multiply` | [Imprecise arithmetic operations order](https://gitlab.com/pritom.rajkhowa/test-smartv/-/wikis/Detector-Documentation#divide-before-multiply) | Medium | Medium
33 | `reentrancy-no-eth` | [Reentrancy vulnerabilities (no theft of ethers)](https://gitlab.com/pritom.rajkhowa/test-smartv/-/wikis/Detector-Documentation#reentrancy-vulnerabilities-1) | Medium | Medium
34 | `reused-constructor` | [Reused base constructor](https://gitlab.com/pritom.rajkhowa/test-smartv/-/wikis/Detector-Documentation#reused-base-constructors) | Medium | Medium
35 | `tx-origin` | [Dangerous usage of `tx.origin`](https://gitlab.com/pritom.rajkhowa/test-smartv/-/wikis/Detector-Documentation#dangerous-usage-of-txorigin) | Medium | Medium
36 | `unchecked-lowlevel` | [Unchecked low-level calls](https://gitlab.com/pritom.rajkhowa/test-smartv/-/wikis/Detector-Documentation#unchecked-low-level-calls) | Medium | Medium
37 | `unchecked-send` | [Unchecked send](https://gitlab.com/pritom.rajkhowa/test-smartv/-/wikis/Detector-Documentation#unchecked-send) | Medium | Medium
38 | `uninitialized-local` | [Uninitialized local variables](https://gitlab.com/pritom.rajkhowa/test-smartv/-/wikis/Detector-Documentation#uninitialized-local-variables) | Medium | Medium
39 | `unused-return` | [Unused return values](https://gitlab.com/pritom.rajkhowa/test-smartv/-/wikis/Detector-Documentation#unused-return) | Medium | Medium
40 | `incorrect-modifier` | [Modifiers that can return the default value](https://gitlab.com/pritom.rajkhowa/test-smartv/-/wikis/Detector-Documentation#incorrect-modifier) | Low | High
41 | `shadowing-builtin` | [Built-in symbol shadowing](https://gitlab.com/pritom.rajkhowa/test-smartv/-/wikis/Detector-Documentation#builtin-symbol-shadowing) | Low | High
42 | `shadowing-local` | [Local variables shadowing](https://gitlab.com/pritom.rajkhowa/test-smartv/-/wikis/Detector-Documentation#local-variable-shadowing) | Low | High
43 | `uninitialized-fptr-cst` | [Uninitialized function pointer calls in constructors](https://gitlab.com/pritom.rajkhowa/test-smartv/-/wikis/Detector-Documentation#uninitialized-function-pointers-in-constructors) | Low | High
44 | `variable-scope` | [Local variables used prior their declaration](https://gitlab.com/pritom.rajkhowa/test-smartv/-/wikis/Detector-Documentation#pre-declaration-usage-of-local-variables) | Low | High
45 | `void-cst` | [Constructor called not implemented](https://gitlab.com/pritom.rajkhowa/test-smartv/-/wikis/Detector-Documentation#void-constructor) | Low | High
46 | `calls-loop` | [Multiple calls in a loop](https://gitlab.com/pritom.rajkhowa/test-smartv/-/wikis/Detector-Documentation/#calls-inside-a-loop) | Low | Medium
47 | `events-access` | [Missing Events Access Control](https://gitlab.com/pritom.rajkhowa/test-smartv/-/wikis/Detector-Documentation#missing-events-access-control) | Low | Medium
48 | `events-maths` | [Missing Events Arithmetic](https://gitlab.com/pritom.rajkhowa/test-smartv/-/wikis/Detector-Documentation#missing-events-arithmetic) | Low | Medium
49 | `incorrect-unary` | [Dangerous unary expressions](https://gitlab.com/pritom.rajkhowa/test-smartv/-/wikis/Detector-Documentation#dangerous-unary-expressions) | Low | Medium
50 | `missing-zero-check` | [Missing Zero Address Validation](https://gitlab.com/pritom.rajkhowa/test-smartv/-/wikis/Detector-Documentation#missing-zero-address-validation) | Low | Medium
51 | `reentrancy-benign` | [Benign reentrancy vulnerabilities](https://gitlab.com/pritom.rajkhowa/test-smartv/-/wikis/Detector-Documentation#reentrancy-vulnerabilities-2) | Low | Medium
52 | `reentrancy-events` | [Reentrancy vulnerabilities leading to out-of-order Events](https://gitlab.com/pritom.rajkhowa/test-smartv/-/wikis/Detector-Documentation#reentrancy-vulnerabilities-3) | Low | Medium
53 | `timestamp` | [Dangerous usage of `block.timestamp`](https://gitlab.com/pritom.rajkhowa/test-smartv/-/wikis/Detector-Documentation#block-timestamp) | Low | Medium
54 | `assembly` | [Assembly usage](https://gitlab.com/pritom.rajkhowa/test-smartv/-/wikis/Detector-Documentation#assembly-usage) | Informational | High
55 | `assert-state-change` | [Assert state change](https://gitlab.com/pritom.rajkhowa/test-smartv/-/wikis/Detector-Documentation#assert-state-change) | Informational | High
56 | `boolean-equal` | [Comparison to boolean constant](https://gitlab.com/pritom.rajkhowa/test-smartv/-/wikis/Detector-Documentation#boolean-equality) | Informational | High
57 | `deprecated-standards` | [Deprecated Solidity Standards](https://gitlab.com/pritom.rajkhowa/test-smartv/-/wikis/Detector-Documentation#deprecated-standards) | Informational | High
58 | `erc20-indexed` | [Un-indexed ERC20 event parameters](https://gitlab.com/pritom.rajkhowa/test-smartv/-/wikis/Detector-Documentation#unindexed-erc20-event-parameters) | Informational | High
59 | `function-init-state` | [Function initializing state variables](https://gitlab.com/pritom.rajkhowa/test-smartv/-/wikis/Detector-Documentation#function-initializing-state-variables) | Informational | High
60 | `low-level-calls` | [Low level calls](https://gitlab.com/pritom.rajkhowa/test-smartv/-/wikis/Detector-Documentation#low-level-calls) | Informational | High
61 | `missing-inheritance` | [Missing inheritance](https://gitlab.com/pritom.rajkhowa/test-smartv/-/wikis/Detector-Documentation#missing-inheritance) | Informational | High
62 | `naming-convention` | [Conformity to Solidity naming conventions](https://gitlab.com/pritom.rajkhowa/test-smartv/-/wikis/Detector-Documentation#conformance-to-solidity-naming-conventions) | Informational | High
63 | `pragma` | [If different pragma directives are used](https://gitlab.com/pritom.rajkhowa/test-smartv/-/wikis/Detector-Documentation#different-pragma-directives-are-used) | Informational | High
64 | `redundant-statements` | [Redundant statements](https://gitlab.com/pritom.rajkhowa/test-smartv/-/wikis/Detector-Documentation#redundant-statements) | Informational | High
65 | `solc-version` | [Incorrect Solidity version](https://gitlab.com/pritom.rajkhowa/test-smartv/-/wikis/Detector-Documentation#incorrect-versions-of-solidity) | Informational | High
66 | `unimplemented-functions` | [Unimplemented functions](https://gitlab.com/pritom.rajkhowa/test-smartv/-/wikis/Detector-Documentation#unimplemented-functions) | Informational | High
67 | `unused-state` | [Unused state variables](https://gitlab.com/pritom.rajkhowa/test-smartv/-/wikis/Detector-Documentation#unused-state-variables) | Informational | High
68 | `costly-loop` | [Costly operations in a loop](https://gitlab.com/pritom.rajkhowa/test-smartv/-/wikis/Detector-Documentation#costly-operations-inside-a-loop) | Informational | Medium
69 | `dead-code` | [Functions that are not used](https://gitlab.com/pritom.rajkhowa/test-smartv/-/wikis/Detector-Documentation#dead-code) | Informational | Medium
70 | `reentrancy-unlimited-gas` | [Reentrancy vulnerabilities through send and transfer](https://gitlab.com/pritom.rajkhowa/test-smartv/-/wikis/Detector-Documentation#reentrancy-vulnerabilities-4) | Informational | Medium
71 | `similar-names` | [Variable names are too similar](https://gitlab.com/pritom.rajkhowa/test-smartv/-/wikis/Detector-Documentation#variable-names-are-too-similar) | Informational | Medium
72 | `too-many-digits` | [Conformance to numeric notation best practices](https://gitlab.com/pritom.rajkhowa/test-smartv/-/wikis/Detector-Documentation#too-many-digits) | Informational | Medium
73 | `constable-states` | [State variables that could be declared constant](https://gitlab.com/pritom.rajkhowa/test-smartv/-/wikis/Detector-Documentation#state-variables-that-could-be-declared-constant) | Optimization | High
74 | `external-function` | [Public function that could be declared external](https://gitlab.com/pritom.rajkhowa/test-smartv/-/wikis/Detector-Documentation#public-function-that-could-be-declared-external) | Optimization | High