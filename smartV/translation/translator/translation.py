import inspect
import copy
import slither

from collections import defaultdict
from typing import Set, Dict, Union

from smartV.translation.abstract_translation import AbstractTranslator
from slither.slither import Slither
import smartV.FOLTranslation as folTras
import smartV.utils.file_operations as fileOp
import smartV.utils.command_line as cmd_line
from smartV.translation.detectors import all_detectors
from smartV.translation.detectors.abstract_detectors import AbstractDetectorAssert
from slither.core.cfg.node import NodeType
from slither.core.cfg.node import recheable
from slither.analyses.data_dependency.data_dependency import get_dependencies

from slither.slithir.operations import (
    HighLevelCall,
    Index,
    LowLevelCall,
    Send,
    SolidityCall,
    Transfer,
)



from slither.core.cfg.node import NodeType, Node
from slither.core.declarations import Function
from slither.core.expressions import UnaryOperation, UnaryOperationType
from slither.core.variables.variable import Variable
from slither.slithir.operations import Call, EventCall
from slither.core.declarations.solidity_variables import SolidityFunction




def _contract_subgraph(contract):
    return f"cluster_{contract.id}_{contract.name}"


# return unique id for contract function to use as node name
def _function_node(contract, function):

    return f"{contract.name}_{function.name}_{len(function.parameters)+1}"


# return unique id for solidity function to use as node name
def _solidity_function_node(solidity_function):
    return f"{solidity_function.name}"


# return dot language string to add graph edge
def _edge(from_node, to_node):
    return f'{from_node}->{to_node}'


# return dot language string to add graph node (with optional label)
def _node(node, label=None):
    return " ".join(
        (
            f'{node}',
        )
    )


# pylint: disable=too-many-arguments
def _process_internal_call(
    contract,
    function,
    internal_call,
    contract_calls,
    solidity_functions,
    solidity_calls,
):
    if isinstance(internal_call, (Function)):
        contract_calls[contract].add(
            _edge(
                _function_node(contract, function),
                _function_node(contract, internal_call),
            )
        )
    elif isinstance(internal_call, (SolidityFunction)):
        solidity_functions.add(
            _node(_solidity_function_node(internal_call)),
        )
        solidity_calls.add(
            _edge(
                _function_node(contract, function),
                _solidity_function_node(internal_call),
            )
        )


def _render_external_calls(external_calls):


    return "\n".join(external_calls)


def _render_internal_calls(contract, contract_functions, contract_calls):
   
    vertex_edge = {}

    for x in contract_functions[contract]:
        vertex_edge[x]=[]

    for x in contract_calls[contract]:
        if '->' in x:
           nodes = x.split('->')
           if nodes[0] in vertex_edge:
              vertexs=vertex_edge[nodes[0]]
              vertexs.append(nodes[1])
              vertex_edge[nodes[0]]=vertexs

    return vertex_edge


def _render_solidity_calls(solidity_functions, solidity_calls):
    lines = []

    lines.append("subgraph cluster_solidity {")
    lines.append('label = "[Solidity]"')

    lines.extend(solidity_functions)
    lines.extend(solidity_calls)

    lines.append("}")

    return "\n".join(lines)


def _process_external_call(
    contract,
    function,
    external_call,
    contract_functions,
    external_calls,
    all_contracts,
):
    external_contract, external_function = external_call

    if not external_contract in all_contracts:
        return

    # add variable as node to respective contract
    if isinstance(external_function, (Variable)):
        contract_functions[external_contract].add(
            _node(
                _function_node(external_contract, external_function),
                external_function.name,
            )
        )

    external_calls.add(
        _edge(
            _function_node(contract, function),
            _function_node(external_contract, external_function),
        )
    )


# pylint: disable=too-many-arguments
def _process_function(
    contract,
    function,
    contract_functions,
    contract_calls,
    solidity_functions,
    solidity_calls,
    external_calls,
    all_contracts,
):
    contract_functions[contract].add(
        _node(_function_node(contract, function), function.name),
    )

    for internal_call in function.internal_calls:
        _process_internal_call(
            contract,
            function,
            internal_call,
            contract_calls,
            solidity_functions,
            solidity_calls,
        )
    for external_call in function.high_level_calls:
        _process_external_call(
            contract,
            function,
            external_call,
            contract_functions,
            external_calls,
            all_contracts,
        )


def _process_functions(functions):
    contract_functions = defaultdict(set)  # contract -> contract functions nodes
    contract_calls = defaultdict(set)  # contract -> contract calls edges

    solidity_functions = set()  # solidity function nodes
    solidity_calls = set()  # solidity calls edges
    external_calls = set()  # external calls edges

    all_contracts = set()

    for function in functions:
        all_contracts.add(function.contract_declarer)
    for function in functions:
        _process_function(
            function.contract_declarer,
            function,
            contract_functions,
            contract_calls,
            solidity_functions,
            solidity_calls,
            external_calls,
            all_contracts,
        )

    render_internal_calls = {}
    for contract in all_contracts:
        render_internal_calls = {**render_internal_calls, **_render_internal_calls(
            contract, contract_functions, contract_calls
        )}

    render_solidity_calls = _render_solidity_calls(solidity_functions, solidity_calls)

    render_external_calls = _render_external_calls(external_calls)
    

    return render_internal_calls



def union_dict(d1, d2):
    d3 = {k: d1.get(k, set()) | d2.get(k, set()) for k in set(list(d1.keys()) + list(d2.keys()))}
    return defaultdict(set, d3)


def dict_are_equal(d1, d2):
    if set(list(d1.keys())) != set(list(d2.keys())):
        return False
    return all(set(d1[k]) == set(d2[k]) for k in d1.keys())


def is_subset(
    new_info: Dict[Union[Variable, Node], Set[Node]],
    old_info: Dict[Union[Variable, Node], Set[Node]],
):
    for k in new_info.keys():
        if k not in old_info:
            return False
        if not new_info[k].issubset(old_info[k]):
            return False
    return True


def to_hashable(d: Dict[Node, Set[Node]]):
    list_tuple = list(
        tuple((k, tuple(sorted(values, key=lambda x: x.node_id)))) for k, values in d.items()
    )
    return tuple(sorted(list_tuple, key=lambda x: x[0].node_id))


class AbstractState:
    def __init__(self):
        # send_eth returns the list of calls sending value
        # calls returns the list of calls that can callback
        # read returns the variable read
        # read_prior_calls returns the variable read prior a call
        self._send_eth: Dict[Node, Set[Node]] = defaultdict(set)
        self._calls: Dict[Node, Set[Node]] = defaultdict(set)
        self._reads: Dict[Variable, Set[Node]] = defaultdict(set)
        self._reads_prior_calls: Dict[Node, Set[Variable]] = defaultdict(set)
        self._events: Dict[EventCall, Set[Node]] = defaultdict(set)
        self._written: Dict[Variable, Set[Node]] = defaultdict(set)

    @property
    def send_eth(self) -> Dict[Node, Set[Node]]:
        """
        Return the list of calls sending value
        :return:
        """
        return self._send_eth

    @property
    def calls(self) -> Dict[Node, Set[Node]]:
        """
        Return the list of calls that can callback
        :return:
        """
        return self._calls

    @property
    def reads(self) -> Dict[Variable, Set[Node]]:
        """
        Return of variables that are read
        :return:
        """
        return self._reads

    @property
    def written(self) -> Dict[Variable, Set[Node]]:
        """
        Return of variables that are written
        :return:
        """
        return self._written

    @property
    def reads_prior_calls(self) -> Dict[Node, Set[Variable]]:
        """
        Return the dictionary node -> variables read before any call
        :return:
        """
        return self._reads_prior_calls

    @property
    def events(self) -> Dict[EventCall, Set[Node]]:
        """
        Return the list of events
        :return:
        """
        return self._events

    def merge_fathers(self, node, skip_father, detector):
        for father in node.fathers:
            if detector.KEY in father.context:
                self._send_eth = union_dict(
                    self._send_eth,
                    {
                        key: values
                        for key, values in father.context[detector.KEY].send_eth.items()
                        if key != skip_father
                    },
                )
                self._calls = union_dict(
                    self._calls,
                    {
                        key: values
                        for key, values in father.context[detector.KEY].calls.items()
                        if key != skip_father
                    },
                )
                self._reads = union_dict(self._reads, father.context[detector.KEY].reads)
                self._reads_prior_calls = union_dict(
                    self.reads_prior_calls,
                    father.context[detector.KEY].reads_prior_calls,
                )

    def analyze_node(self, node, detector):

        state_vars_read: Dict[Variable, Set[Node]] = defaultdict(
            set, {v: {node} for v in node.state_variables_read}
        )

        # All the state variables written
        state_vars_written: Dict[Variable, Set[Node]] = defaultdict(
            set, {v: {node} for v in node.state_variables_written}
        )
        slithir_operations = []
        # Add the state variables written in internal calls
        for internal_call in node.internal_calls:
            # Filter to Function, as internal_call can be a solidity call
            if isinstance(internal_call, Function):
                for internal_node in internal_call.all_nodes():
                    for read in internal_node.state_variables_read:
                        state_vars_read[read].add(internal_node)
                    for write in internal_node.state_variables_written:
                        state_vars_written[write].add(internal_node)
                slithir_operations += internal_call.all_slithir_operations()

        contains_call = False

        self._written = state_vars_written
        for ir in node.irs + slithir_operations:
            if detector.can_callback(ir):
                self._calls[node] |= {ir.node}
                self._reads_prior_calls[node] = set(
                    self._reads_prior_calls.get(node, set())
                    | set(node.context[detector.KEY].reads.keys())
                    | set(state_vars_read.keys())
                )
                contains_call = True

            if detector.can_send_eth(ir):
                self._send_eth[node] |= {ir.node}

            if isinstance(ir, EventCall):
                self._events[ir] |= {ir.node, node}

        self._reads = union_dict(self._reads, state_vars_read)

        return contains_call

    def add(self, fathers):
        self._send_eth = union_dict(self._send_eth, fathers.send_eth)
        self._calls = union_dict(self._calls, fathers.calls)
        self._reads = union_dict(self._reads, fathers.reads)
        self._reads_prior_calls = union_dict(self._reads_prior_calls, fathers.reads_prior_calls)

    def does_not_bring_new_info(self, new_info):
        if is_subset(new_info.calls, self.calls):
            if is_subset(new_info.send_eth, self.send_eth):
                if is_subset(new_info.reads, self.reads):
                    if dict_are_equal(new_info.reads_prior_calls, self.reads_prior_calls):
                        return True
        return False


def _filter_if(node):
    """
    Check if the node is a condtional node where
    there is an external call checked
    Heuristic:
        - The call is a IF node
        - It contains a, external call
        - The condition is the negation (!)

    This will work only on naive implementation
    """
    return (
        isinstance(node.expression, UnaryOperation)
        and node.expression.type == UnaryOperationType.BANG
    )



class FOLTraslation(AbstractTranslator):


    ARGUMENT = "fol"
    
    HELP = "Translate the CFG of each functions to a set of first order logic"

    WIKI = "https://github.com/trailofbits/slither/wiki/Printer-documentation#cfg"
    
       
    map_delete_statements={}
    
    stack_infec_call = []
    
    solidity_call = []
    
    map_REF={}
    
    map_TMP={}
    
    map_NEW={}
    
    map_NEW_ELEMENT={}
    
    map_all_function={}
    
    dummy_count = 0
    
    modifier_count = 0
        
    emit_count = 0
    
    solidity_count = 0
    
    user_assert_count = 0
    
    reenterncy_var_count = 0
    
    link_count = 0
    
    current_contract = None
    
    current_contract_type = None
    
    user_assert_list = []
    
    user_assert_fun_call_list = []
    
    reentrancy_address_map = {}

    arbitrary_address_map = {}    
    
    Map_Class = {}
    
    Map_Updated_Name = {}
    
    Map_inheritance = {}
    
    Map_Fun_Summaries = {}
    
    Map_Simplify_Fun = {}

    
    
    def slithir_cfg_to_IRFOL(self) -> str:
        """
        Traslate the CFG to a set of first order logic. The nodes includes the Solidity expressions and the IRs
        :return: the list 
        :rtype: str
        """
        from slither.core.cfg.node import NodeType
        
        
        output_info=''
                

                
        # Avoid dupplicate funcitons due to different compilation unit
        all_functionss = [
                compilation_unit.functions for compilation_unit in self.slither.compilation_units
        ]
        all_functions = [item for sublist in all_functionss for item in sublist]
        all_functions_as_dict = {
                function.canonical_name: function for function in all_functions
        }

        func_call_graph = _process_functions(all_functions_as_dict.values())
        
        
        for contract in self.contracts:
            if contract.is_top_level:
                continue
                
                            
            (name, inheritance, var, func_summaries, modif_summaries) = contract.get_summary()
            
            self.Map_inheritance[name] = inheritance
                    
            for (
                _c_name,
                f_name,
                visi,
                modifiers,
                read,
                write,
                internal_calls,
                external_calls,
            ) in func_summaries:
            

              fun_details=[]
              
              fun_details.append(f_name)

              fun_details.append(_c_name)
                            
              fun_details.append(internal_calls)
            
              fun_details.append(external_calls)

              fun_details.append(visi)
              
              fun_details.append(modifiers)
              
              
              self.Map_Fun_Summaries[f_name] = fun_details
              
              
                          
            self.current_contract = contract
                        
            self.map_REF={}
    
            self.map_TMP={}
            
            #print('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~')
            
            #print('Contract : '+contract.name)
            
            #print('Type : '+contract.kind)
            

            Contract_Var_Facts={}
            
            for var in contract.variables:
            
                self.variableHandling(var, Contract_Var_Facts)
                
            for enum in contract.enums:
            
                print(enum)
                
            for structure in contract.structures:
            
                print(structure)
              
            #print('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~')            
            

            for function in contract.functions + contract.modifiers:
           
            
                stmts={}
                
                parameters =[]
                
                vfact_parameters=[]
                
                vfact_parameter_names=[]
                
                fun_name = None
                
                fun_def = None
                
                self.stack_infec_call=[]
                
               
                #print(function.slithir_cfg_to_dot_str())
                
                #print('----------------------------------------------------------')
                
                if format(function.contract.kind)=='library':
 
                   fun_name = 'FUNCTION NAME : '+contract.name+'_'+str(function.name)
                
                   parameters.append(format(function.name))               
                
                else:
                                
                   fun_name = 'FUNCTION NAME : '+contract.name+'_'+str(function.name)
                
                   parameters.append(contract.name+'_'+format(function.name))
                
                if function.name in self.Map_Class:
                
                   list_class = self.Map_Class[function.name]
                   
                   list_class.append(contract.name)
                   
                   self.Map_Class[function.name] = list_class
                   
                else:
                   
                   list_class = []
                   
                   list_class.append(contract.name)   
                   
                   self.Map_Class[function.name] = list_class                
                
                
                current_contract_type = format(function.contract.kind)
                
                if format(function.contract.kind)=='library':
                
                    fun_def=None
                    
                else:
                
                    fun_def='CONRT_'+format(self.current_contract)+' : _c1'
                    
                    vfact_parameters.append('CONRT_'+format(self.current_contract))
                    
                    vfact_parameter_names.append(format(self.current_contract))
                
                for parameter in function.parameters:
                
                    if fun_def is None:
                    
                        fun_def=' '+self.extractingType(parameter.type)+' : '+parameter.name
                        
                        parameters.append([parameter.name])
                        
                        vfact_parameters.append(self.extractingType(parameter.type))
                        
                        vfact_parameter_names.append(parameter.name)
                    
                    else:
                    
                        fun_def+=' X '+self.extractingType(parameter.type)+' : '+parameter.name
                        
                        parameters.append([parameter.name])
                        
                        vfact_parameters.append(self.extractingType(parameter.type))
                        
                        vfact_parameter_names.append(parameter.name)
                        
                    
                fun_ret=None
                
                for parameter in function.returns:
                
                    if fun_ret is None:
                    
                       fun_ret='-->  '+self.extractingType(parameter.type)
                       
                       vfact_parameters.append(self.extractingType(parameter.type))
                       
                    else:
                    
                       fun_ret=' X '+self.extractingType(parameter.type)
                       
                       vfact_parameters.append(self.extractingType(parameter.type))
                       
                if fun_def is None and fun_ret is None:

                     fun_def = 'FUNCTION DEFINITION : void --> void'
                     
                     vfact_parameters.append('void')
                     
                     vfact_parameters.append('void')
                
                elif fun_def is not None and fun_ret is None:

                     fun_def = 'FUNCTION DEFINITION : '+fun_def+' --> void'
                     
                     vfact_parameters.append('void')
                
                else:
                
                     fun_def = 'FUNCTION DEFINITION : '+fun_def+fun_ret
                     
                #print('============================================================')   
                
                #print(fun_name)
                
                #print('------------------------------------------------------------')
                
                #print(fun_def)  
                     
                #print('============================================================')                      
                Var_Facts={**Contract_Var_Facts}
                
                #print('============================================================') 
                #for node in function.nodes:
                #    for ir in node.irs:
                #        print(ir)
                #        if isinstance(ir, (HighLevelCall, LowLevelCall, Transfer, Send)):
                #           print('###################$$$$$$$$$$$$$$$$')                        
                #           print(ir.call_value)
                #           print(ir.destination)
                #           print('--------------------/////////////')
                #           for xx in get_dependencies(ir.call_value, function.contract):
                #               print(xx)
                #           print('--------------------/////////////')
                #           print('--------------------/////////////')
                #           for xx in get_dependencies(ir.destination, function.contract):
                #               print(xx)
                #           print('--------------------/////////////')                          
                #           print('###################$$$$$$$$$$$$$$$$')
                #print('============================================================') 
                
                
                
                
                statements = self.getLoopNode(function.nodes)
                
                IR_code = self.contructNodes(statements, Var_Facts)
                
                Var_Facts = self.vfactFilter(Var_Facts)

                     

                parameter_info = []
                
                parameter_info.append(vfact_parameters) #0
                
                parameter_info.append(vfact_parameter_names) #1
                
                parameter_info.append(IR_code) #2
                
                parameter_info.append(Var_Facts) #3
                
                parameter_info.append(fun_name) #4
                
                parameter_info.append(fun_def) #5
                
                parameter_info.append('Contract : '+contract.name)#6
            
                parameter_info.append('Type : '+contract.kind)#7
                
                parameter_info.append(parameters)#8
                
                parameter_info.append(contract.name)#9
                
                parameter_info.append(function.is_protected())#10
                
                parameter_info.append(function.visibility)#11
                
                self.map_all_function[contract.name+'_'+str(function.name)+'_'+str(len(vfact_parameter_names))]=parameter_info
                
                
                
                #if contract.name+'_'+str(function.name)+'_'+str(len(vfact_parameter_names)) not in self.map_all_function:
                
                #   self.map_all_function[contract.name+'_'+str(function.name)+'_'+str(len(vfact_parameter_names))]=parameter_info
                   
                #else:
                
                #   existing_parameter_info = self.map_all_function[contract.name+'_'+str(function.name)+'_'+str(len(vfact_parameter_names))]
                   
                #   parameter_info[2]=['-1','seq', existing_parameter_info[2], parameter_info[2]]
                                                          
                #   parameter_info[3]={**existing_parameter_info[3], **parameter_info[3]}
                   
                #   self.map_all_function[contract.name+'_'+str(function.name)+'_'+str(len(vfact_parameter_names))]=parameter_info
                           

        for fun_name in self.map_all_function:
        
        
            if fun_name not in self.Map_Simplify_Fun:
        
 
               if fun_name in func_call_graph.keys():
 
                  self.recSubstitutionProcess(fun_name, func_call_graph)
                



                  
        

        for fun_name in self.map_all_function:
                
            parameter_info = self.map_all_function[fun_name]
            
            #print('@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@')
            #print(fun_name)
            #print('$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$*')
            #print(parameter_info[2])
            #print('------------------------------------------')
            #print(parameter_info[3])
            #print('$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$*')
                          
            
            
            
            #if fun_name not in self.Map_Simplify_Fun:
            
               
            #   if parameter_info[2] is not None and len(parameter_info[2])>0:
               
            #      parameter_info[2], parameter_info[3] = self.functionSubstitutionProcess(parameter_info[2], parameter_info[3])
                  
            #      self.map_all_function[fun_name] = parameter_info
                  
            #      self.Map_Simplify_Fun[fun_name]=fun_name
                  
               #print('@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@')
               #print(fun_name)
               #print('$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$')
               #print(parameter_info[2])
               #print('------------------------------------------')
               #print(parameter_info[3])
               #print('$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$')
               
               
            
            MODIDER = None 
            
            MODIDER_FACTS = {}    
            
            FUN_DETAILS = ''
               
            
            
            #print(parameter_info[6])
            
            FUN_DETAILS+=parameter_info[6]+"\n"
            
            #print(parameter_info[7])
            
            FUN_DETAILS+=parameter_info[7]+"\n"
            
            #print(parameter_info[4])
            
            FUN_DETAILS+=parameter_info[4]+"\n"
            
            #print(parameter_info[5])
            
            FUN_DETAILS+=parameter_info[5]+"\n"
           
            #print(parameter_info[6])
            
            FUN_DETAILS+=parameter_info[6]+"\n"
            
            #print(parameter_info[10])
            
            #if parameter_info[10]:
            
            #   MODIDER = ['-1','=',['IsProtected'],['True']]
               
            #   MODIDER_FACTS['IsProtected']=['_y'+str(len(MODIDER_FACTS)+len(parameter_info[3])),'boolean']
               
            #else:
            
            #   MODIDER = ['-1','=',['IsProtected'],['False']]
               
            #   MODIDER_FACTS['IsProtected']=['_y'+str(len(MODIDER_FACTS)+len(parameter_info[3])),'boolean']
            
            #if parameter_info[11]=='private':
            
            #   MODIDER = ['-1','seq',MODIDER,['-1','seq',['-1','=',['IsExternal'],['False']],['-1','seq',['-1','=',['IsPublic'],['False']],['-1','=',['IsPrivate'],['True']]]]]
               
            #   MODIDER_FACTS['IsPublic']=['_y'+str(len(MODIDER_FACTS)+len(parameter_info[3])),'boolean']
               
            #   MODIDER_FACTS['IsPrivate']=['_y'+str(len(MODIDER_FACTS)+len(parameter_info[3])),'boolean']
               
            #   MODIDER_FACTS['IsExternal']=['_y'+str(len(MODIDER_FACTS)+len(parameter_info[3])),'boolean']
               
            #elif parameter_info[11]=='public':
            
            #   MODIDER = ['-1','seq',MODIDER,['-1','seq',['-1','=',['IsExternal'],['False']],['-1','seq',['-1','=',['IsPublic'],['True']],['-1','=',['IsPrivate'],['False']]]]]

            #   MODIDER_FACTS['IsPublic']=['_y'+str(len(MODIDER_FACTS)+len(parameter_info[3])),'boolean']
               
            #   MODIDER_FACTS['IsPrivate']=['_y'+str(len(MODIDER_FACTS)+len(parameter_info[3])),'boolean']
               
            #   MODIDER_FACTS['IsExternal']=['_y'+str(len(MODIDER_FACTS)+len(parameter_info[3])),'boolean']                        
            
            #elif parameter_info[11]=='external':
            
            #   MODIDER = ['-1','seq',MODIDER,['-1','seq',['-1','=',['IsExternal'],['True']],['-1','seq',['-1','=',['IsPublic'],['False']],['-1','=',['IsPrivate'],['False']]]]]
            
            #   MODIDER_FACTS['IsPublic']=['_y'+str(len(MODIDER_FACTS)+len(parameter_info[3])),'boolean']
               
            #   MODIDER_FACTS['IsPrivate']=['_y'+str(len(MODIDER_FACTS)+len(parameter_info[3])),'boolean']
               
            #   MODIDER_FACTS['IsExternal']=['_y'+str(len(MODIDER_FACTS)+len(parameter_info[3])),'boolean']

            
            
            
            IR_code = parameter_info[2]
            
            Var_Facts = parameter_info[3]
            
            #print(IR_code)
            
            LIST_EQ=[]
            
            
            LIST_EQ = self.updateSEQProcess(IR_code,LIST_EQ)
            

            
            IR_code = self.IRTranslation(LIST_EQ, Var_Facts)
            
                        
            #print(LIST_EQ)
            
            IR_code, Var_Facts = self.updateSolidityCallProcess(IR_code, Var_Facts)
                        
            IR_code, Var_Facts = self.require2IfProcess(IR_code, Var_Facts)
            

                        
            
            
            #print(IR_code)
            
            #print(Var_Facts)
            #if MODIDER is not None:
            
            #   IR_code = ['-1','seq',IR_code,MODIDER]
                              
            #Var_Facts={**Var_Facts,**MODIDER_FACTS}
            
            
            if len(self.reentrancy_address_map)>0:
            
               #print('////////////////=================*******************')
               
               self.reentrancy_address_map.clear()
               
               self.checkReenterncy(IR_code, Var_Facts,FUN_DETAILS)
               
               self.reentrancy_address_map.clear()
               
               
               #print('////////////////=================*******************')
        
              
            
            #if IR_code is not None:
            
            #    main_prog=['-1','fun',parameter_info[8], IR_code]
                
                
            #    self.translate_output(main_prog, Var_Facts, FUN_DETAILS)
                    

            
            #print('=======================================================================================================')        
        
        
       
    
        #self.constructSolidityCall()
        #for x in self.map_REF:
        
        #    print(x+" : "+str(self.map_REF[x]))
    
        #for x in self.map_TMP:
        
        #    print(x+" : "+str(self.map_TMP[x]))                 
                  
        #for x in  self.map_NEW:
        
        #    print(x+" : "+str(self.map_NEW[x])) 
        output_val=''
        return output_val







    '''
    #
    #Translate and Display Output
    #
    '''
    def translate_output(self, main_prog, Var_Facts, FUN_DETAILS):
    
    
        print('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~')
        
        print(FUN_DETAILS)
        
        print('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~')
        print('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~')
                   
        print('============================')
                    
        #print(Var_Facts)
                          
        #print(main_prog)
        print('----------------------------')
                   
        assert_map_o={}
        assert_map_a=[]
                   
        output_map_o={}
        output_map_a=[]
                   
        assume_map_o={}
        assume_map_a=[] 
                   
        user_defined_map_o={}
        user_defined_map_a=[]
               
        f,o,a,cm,assert_list,assume_list,assert_key,output_val = folTras.translate1(main_prog,Var_Facts,1)
                   
                      
        Output_val=''
        Output_val+='\nOutput in normal notation:\n'
        Output_val+='\n1. Frame axioms:\n'
        Output_val+=folTras.eqset2string1Print1(f, Var_Facts, self.map_all_function)+"\n"
                   
        for x in o:
            #output_map_o[x]=o[x]
        
            if 'solidity' in x and 'SOLIDITY_CALL_assert' in  folTras.expr2string1(o[x][2]):
                       
                assert_map_o[x]=o[x]

            elif 'solidity' in x and 'SOLIDITY_CALL_require' in  folTras.expr2string1(o[x][2]):
                       
                assume_map_o[x]=o[x]

                           
        #    elif 'user_assert' in x:
                       
        #        stmt=[]
                           
        #        cond = self.filterUserAssertion(o[x][2],stmt,[o[x][1][0][:-1]]+o[x][1][1:])
                           
        #        if cond is not None :
                           
        #           if len(stmt)>0 :
                              
        #              if len(cond)==0:
                                 
        #                 user_defined_map_o[x]=['c1',stmt[0]]
                                 
        #              else:
                           
        #                 user_defined_map_o[x]=['c1',['implies',cond,stmt[0]]]
                               
        #           else:
                            
        #                if o[x][1][0][:-1]!=o[x][2][0][:-1]:
                              
        #                   user_defined_map_o[x]=o[x]
        #        else:
                           
        #             user_defined_map_o[x]=o[x]
                           
            else:
                       
                output_map_o[x]=o[x]
                           
        for x in a:
        #     output_map_a.append(x)
            if x[0]=='i1' or x[0]=='i0':
                        
               if x[0]=='i1' and 'solidity' in x[3][0] and 'SOLIDITY_CALL_assert' in  folTras.expr2string1(x[4]):
                        
                  assert_map_a.append(x)
                           
               elif x[0]=='i1' and 'solidity' in x[3][0] and 'SOLIDITY_CALL_require' in folTras.expr2string1(x[4]):
                        
                  assume_map_a.append(x)
                  
               else:
               
                  output_map_a.append(x)
                           
        #       elif x[0]=='i1' and 'user_assert' in x[3][0]:
                        
                        
        #           stmt=[]
                           
                           
        #           left_exp=copy.deepcopy(x[3])
                           
        #           left_exp=folTras.expr_replace(left_exp,x[3][-1],[x[2]])
                           
                                                                              
        #           cond = self.filterUserAssertion(x[4],stmt,left_exp)
                           
        #           if cond is not None:
                           
        #              if len(stmt)>0:
                              
        #                 if len(cond)==0:
                                 
        #                    user_defined_map_a.append(['c1',['implies',['<=',[x[2]],[cm[x[2]]]],stmt[0]]])
                                 
        #                 else:
                                                      
        #                    user_defined_map_a.append(['c1',['implies',['<=',[x[2]],[cm[x[2]]]],['implies',cond,stmt[0]]]])
                             
        #              else:
                           
        #                 user_defined_map_a.append(x)
                                 
        #           else:
                           
        #              user_defined_map_a.append(x)
                                                       

            elif x[0]=='i0' and 'solidity' not in x[2][0] and 'user_assert' not in x[2][0]:
                        
                output_map_a.append(x)
                           
            elif x[0]=='i1' and 'solidity' not in x[3][0] and 'user_assert' not in x[3][0]:

                output_map_a.append(x)    
                           
            else:
                        
                output_map_a.append(x)               
                           

        Output_val+='\n2. Output equations:\n'
        Output_val+=folTras.eqset2string1Print1(output_map_o, Var_Facts, self.map_all_function)+"\n"
        Output_val+='\n3. Other axioms:\n'
        for x in output_map_a: 
                   
            parameters={}
                       
            Output_str = folTras.wff2string2(x, Var_Facts, parameters, self.map_all_function)
                       
            conditions = folTras.quantifer_list(parameters)
                       
                       
            if conditions is None:
        
               Output_val+=Output_str+"\n"
           
            else:
        
               Output_val+=conditions+' -> '+Output_str+"\n"
                       
                       
                       
        Output_val+='\n4. Assumption :\n'
                   
        Output_val+=folTras.eqset2string1Print1(assume_map_o, Var_Facts, self.map_all_function)+"\n"
                   
        for x in assume_map_a: 
                   
              parameters={}
                       
              Output_str = folTras.wff2string2(x, Var_Facts, parameters, self.map_all_function)
                       
              conditions = folTras.quantifer_list(parameters)
                       
              if conditions is None:
        
                  Output_val+=Output_str+"\n"
           
              else:
        
                  Output_val+=conditions+' -> '+Output_str+"\n"
                       
                       
        Output_val+='\n5. Assertion :\n'
                   
        Output_val+=folTras.eqset2string1Print1(assert_map_o, Var_Facts, self.map_all_function)+"\n"
                   
        for x in assert_map_a: 
                   
            parameters={}
                       
            Output_str = folTras.wff2string2(x, Var_Facts, parameters, self.map_all_function)
                       
            conditions = folTras.quantifer_list(parameters)
                       
            if conditions is None:
        
                Output_val+=Output_str+"\n"
           
            else:
        
                Output_val+=conditions+' -> '+Output_str+"\n"
                          
        Output_val+='\n5. User Defined Assertion :\n'
                   
        Output_val+=folTras.eqset2string1Print1(user_defined_map_o, Var_Facts, self.map_all_function)+"\n"
                   
        for x in user_defined_map_a: 
                   
           parameters={}
                       
           Output_str = folTras.wff2string2(x, Var_Facts, parameters, self.map_all_function)
                       
           conditions = folTras.quantifer_list(parameters)
                       
           if conditions is None:
        
              Output_val+=Output_str+"\n"
           
           else:
        
              Output_val+=conditions+' -> '+Output_str+"\n"

                          
        #output_info+='\n'+Output_val+'\n' 
           
        #output_info+='\n'+output_val+'\n'
                   
        print('')
        print('~~~~~~~~~~~~~~Details Start~~~~~~~~~~~~~~~~~')
        for vfact in Var_Facts:
                   
            if 'user_assert' not in vfact:
                   
               fact_str = None 
                       
               for efact in Var_Facts[vfact][1:-1]:
                       
                   if fact_str is None:
                                 
                      fact_str =  efact
                              
                   else:  

                      fact_str =  fact_str+' X '+efact                                 
                   
               if fact_str is None:
                       
                   fact_str = Var_Facts[vfact][-1]
                          
               else:
                       
                   fact_str = fact_str+' --> '+Var_Facts[vfact][-1]
                   
                          
               print(vfact+" : "+fact_str)
                     
        print('~~~~~~~~~~~~~~Details End~~~~~~~~~~~~~~~~~')  
        print('~~~~~~~~~~~~~~Translation Start~~~~~~~~~~~~~~~~~')
        #print('')
        print(Output_val)
        #print(output_val)
        print('~~~~~~~~~~~~~~Translation End~~~~~~~~~~~~~~~~~')
        print('')
        return Var_Facts, output_map_o, output_map_a,f




    '''
    #
    #Construct declaration for z3 query 
    #
    '''
    def constructDeclaration(self, Var_Facts):
    
        V_DECLARATION_MAP={}
    
        for vfact in Var_Facts:
        
            if 'user_assert' not in vfact:
            
               if len(Var_Facts[vfact][1:])==1:
               
                  for efact in Var_Facts[vfact][1:]:
               
                     if vfact!=efact:
                                                            
                        if efact !='int256' and efact !='uint256' and efact !='boolean' and efact !='bool':
                     
                           if efact not in V_DECLARATION_MAP:
                        
                              V_DECLARATION_MAP[efact]= efact+"= DeclareSort('"+efact+"')"
                           
                           if vfact not in V_DECLARATION_MAP:
                           
                              V_DECLARATION_MAP[vfact]= vfact+"= Const('"+vfact+"',"+efact+")"
                     
                     
                        else:
                     
                           if efact =='int256' or efact =='uint256':
                                 
                              if vfact not in V_DECLARATION_MAP:
                        
                                 V_DECLARATION_MAP[vfact]= vfact+"= Int('"+vfact+"')"
                     
                           elif efact =='boolean' or efact =='bool':
                        
                              if vfact not in V_DECLARATION_MAP:
                           
                                 V_DECLARATION_MAP[vfact]= vfact+"= Const('"+vfact+"',BoolSort())"
               
               
               else:
               
                  if vfact not in V_DECLARATION_MAP:
               
                     V_DEC_FUN = vfact+"= Function('"+vfact+"'" 
                  
                     V_DEC_PARA= None
            
                     for efact in Var_Facts[vfact][1:]:
                    
                         if V_DEC_PARA is None:
                      
                            if efact !='int256' and efact !='uint256' and efact !='boolean' and efact !='bool':
                            
                               if efact not in V_DECLARATION_MAP:
                        
                                  V_DECLARATION_MAP[efact]= efact+"= DeclareSort('"+efact+"')"
                         
                               V_DEC_PARA=","+efact
                            
                            else:
                         
                               if efact =='int256' or efact =='uint256':
                            
                                  V_DEC_PARA=",IntSort()"
                            
                               elif efact =='boolean' or efact !='bool':
                            
                                  V_DEC_PARA=",BoolSort()"
                               
                         else:
 
                            if efact !='int256' and efact !='uint256' and efact !='boolean' and efact !='bool':
                            
                               if efact not in V_DECLARATION_MAP:
                        
                                  V_DECLARATION_MAP[efact]= efact+"= DeclareSort('"+efact+"')"
                         
                               V_DEC_PARA=V_DEC_PARA+","+efact
                            
                            else:
                         
                               if efact =='int256' or efact =='uint256':
                            
                                  V_DEC_PARA=V_DEC_PARA+",IntSort()"
                            
                               elif efact =='boolean' or efact =='bool':
                            
                                  V_DEC_PARA=V_DEC_PARA+",BoolSort()" 
                               
                     V_DEC_FUN = vfact+"= Function('"+vfact+"'"+V_DEC_PARA+")" 
                  
                     V_DECLARATION_MAP[vfact] = V_DEC_FUN 
                     
        return V_DECLARATION_MAP
    
    






    '''
    #
    #Construct z3 query 
    #
    '''
    def Z3QUERY_CONSTRUCT(self, Var_Facts, output_map_o, output_map_a, f, assert_str):
    
        
        Z3QUERY_STATEMENT=None
        
        Z3DEC_STATEMENTS='from z3 import *\n'+'set_param(proof=True)\ntry:\n'
        
        MODEL_STATEMENTS='\t_s=Solver()\n'+'\t_s.set("timeout",50000)\n'
        
        END_STATEMENTS="except Exception as e:\n\tprint(\"Error1(Z3Query)\"+str(e))\n\tsys.exit(1)\ntry:\n\tresult=_s.check()\n\tif sat==result:\n\t\tprint(\"Counter Example\")\n\t\tprint (_s.model())\n\telif unsat==result:\n\t\tprint (\"Successfully Proved\")\n\telse:\n\t\tprint(\"Failed To Prove\")\nexcept Exception as e:\n\tprint(\"Error2(Z3Query)\"+str(e))\n\tsys.exit(1)"
           
        
        STATEMENTS=None
        
        u_Var_Facts=copy.deepcopy(Var_Facts)
        
        list_of_array_map={}
        
        contraints_map = {}
        
        for x in u_Var_Facts:
        
            if u_Var_Facts[x][-1]=='map' or u_Var_Facts[x][-1]=='array':
        
               list_of_array_map[x]=u_Var_Facts[x]
            
            
            
            
        if len(list_of_array_map)>1:

           for x1 in list_of_array_map:
        
               for x2 in list_of_array_map:   
           
                  if x1!=x2 and list_of_array_map[x1][-1] == list_of_array_map[x2][-1]:
                  
                     exp_add = ['a',['not',['==',f[x1][-1],f[x2][-1]]]]
               
                     if exp_add not in output_map_a:
                     
                        output_map_a.append(exp_add)
        
        
        
        
        
        update_Var_Facts = folTras.updateVFACTS(u_Var_Facts)


        V_DECLARATION_MAP = self.constructDeclaration(update_Var_Facts)
        
        
        for e in f:
           w=f[e]
           
           if u_Var_Facts[e][-1]!='expression':
              #print(w)
              #print(folTras.wff2z3_update_postCond(w))
              parameters={}
              statement, declaration_stmt = folTras.wff2z3_update2(w, update_Var_Facts, parameters, self.map_all_function)
           
              V_DECLARATION_MAP = {**V_DECLARATION_MAP, **declaration_stmt}
           
              if STATEMENTS is None:
           
                 if Var_Facts[e][-1]!='string':
           
                    STATEMENTS="\t_s.add("+statement+")\n"
           
              else:
           
                 if Var_Facts[e][-1]!='string':

                    STATEMENTS+='\t_s.add('+statement+")\n"          
        
        

        for e in output_map_o:
           w=output_map_o[e]
           #print(w)
           #print(folTras.wff2z3_update_postCond(w))
           parameters={}
           statement, declaration_stmt = folTras.wff2z3_update2(w, update_Var_Facts, parameters, self.map_all_function)
           
           V_DECLARATION_MAP = {**V_DECLARATION_MAP, **declaration_stmt}
           
           if STATEMENTS is None:
           
              if Var_Facts[e][-1]!='string':
           
                 STATEMENTS="\t_s.add("+statement+")\n"
           
           else:
           
              if Var_Facts[e][-1]!='string':

                STATEMENTS+='\t_s.add('+statement+")\n"          


                      
        for w in output_map_a:
           #print(w)
           #print(folTras.wff2z3_update_postCond(w))
           parameters={}
           
           if w[0]=='i0':
           
              if len(update_Var_Facts[w[-2][0]][1:]) <= len(w[-2][1:]):
              
                 temp_Var_Facts={}
                 count_parameter=[]
                 
                 noterm=len(w[-2][1:])-len(update_Var_Facts[w[-2][0]][1:])
              
                 for para in range(0,noterm+1):
                 
                     count_parameter.append('uint256')
                     
                 
                 v_fact_term = update_Var_Facts[w[-2][0]][:-1]+count_parameter+[update_Var_Facts[w[-2][0]][-1]]
                     
                 update_Var_Facts[w[-2][0]] = v_fact_term
                 
                 temp_Var_Facts[w[-2][0]]= v_fact_term
                 
                 
                 declaration_map = self.constructDeclaration(temp_Var_Facts)
                 
                 V_DECLARATION_MAP = {**V_DECLARATION_MAP, **declaration_map}
                 
              
           statement,declaration_stmt = folTras.wff2z3_update2(w, update_Var_Facts, parameters, self.map_all_function)
           
           
           

           
           V_DECLARATION_MAP = {**V_DECLARATION_MAP, **declaration_stmt}
           
           if STATEMENTS is None:
           
              STATEMENTS="\t_s.add("+statement+")\n"
           
           else:

              STATEMENTS+='\t_s.add('+statement+")\n"
              
              
           if w[0]=='s1':

               contraints_map[w[1][1][1][0]]=w[1][1][2]
               
               tem_w=['a',['>=',w[1][1][2],['0']]]
               
               constrain_statement,constrain_declaration_stmt = folTras.wff2z3_update2(tem_w, update_Var_Facts, parameters, self.map_all_function)
               
               STATEMENTS+='\t_s.add('+constrain_statement+")\n"
               

              
        if assert_str is not None:
        
           STATEMENTS+='\t_s.add('+assert_str+")\n"
         
             
              
        Z3QUERY_STATEMENT = Z3DEC_STATEMENTS  
        
        
                 
        for V_DEC in V_DECLARATION_MAP:
          
             Z3QUERY_STATEMENT +="\t"+V_DECLARATION_MAP[V_DEC]+"\n"
             
        Z3QUERY_STATEMENT +=MODEL_STATEMENTS
        Z3QUERY_STATEMENT +=STATEMENTS 
        Z3QUERY_STATEMENT +=END_STATEMENTS
        
        fileOp.writtingFile( "z3query.py" , Z3QUERY_STATEMENT )
        
        status = cmd_line.run_command()

        if 'Successfully Proved' in status.decode("utf-8"): 
        
           return 'success' 
        
        else:
        
       
           return 'failed'       
          


    '''
    #
    #Get Assertion Condition
    #
    '''
    def makePrime(self, expression):

        op = folTras.expr_op(expression)
        
        args = folTras.expr_args(expression)
        
        if op not in ['ite','and','or']:
        
          expression[0]=expression[0]+'1'
        
          return expression
           
        elif op in folTras._infix_op:
        
           return [op]+list(self.makePrime(arg) for arg in args)
           
        else:  
        
           return expression



    '''
    #
    #Get Assertion Condition
    #
    '''
    def filterUserAssertion(self, expression, stmt, left_stmt):
    
    
        op = folTras.expr_op(expression)
        
        args = folTras.expr_args(expression)
        
        if op=='ite':
        
           if args[0][:2] == ['=',['_x1']] and left_stmt == args[2]:
           
                      
              stmt.append(['==',args[0][2],args[1]])          
              #if args[0][2][0]=='ite':
              
                 #print('#############################')
              
                 #print(args[0][2])
              
                 #print('#############################')
                 
              #   stmt.append(['==',args[0][2][-1],args[1]])
                 #stmt.append(['==',self.makePrime(args[0][2][-1]),args[1]])
                 
              #else:
              
              #   stmt.append(['==',args[0][2],args[1]])
                 #stmt.append(['==',self.makePrime(args[0][2]),args[1]])              
              return []
              
           elif args[0][:2] == ['=',['_x1']]:
           
           
               print('/////////////////////')
               
               print(args[0][2][3])
               print('/////////////////////')
           
               if args[0][2][3][0]=='ite':
               
                  for x in self.user_assert_list:
                                                          
                      if x[:2]==args[0][2][3][-1][:2]:
                      
                         args[0][2][3][-1][0]=args[0][2][3][-1][0]+'1'
               
                  #stmt.append(['==',args[0][2][3][-1],args[0][2][2]])
                  stmt.append(['==',self.makePrime(args[0][2][3][-1]),args[0][2][2]])                  
                  
                    
               else:
               
                  for x in self.user_assert_list:
                  
                      if x[:2]==args[0][2][3][:2]:
                      
                         args[0][2][3][0]=args[0][2][3][0]+'1'
               
                  #stmt.append(['==',args[0][2][3],args[0][2][2]])
                  stmt.append(['==',self.makePrime(args[0][2][3]),args[0][2][2]]) 
                 
           else:
            
               arg1 = self.filterUserAssertion(args[0], stmt, left_stmt)
               
                                            
               if args[1][0]=='ite' and left_stmt == args[2]:
               
                  arg2 = self.filterUserAssertion(args[1], stmt, left_stmt)
 
                  if len(arg2)==0:  
                  
                     return arg1
                  
                  elif arg2 is None:
                  
                     return arg1
                  
                  else:
                  
                     return ['and',arg1,arg2] 
                     
               elif args[2][0]=='ite' and left_stmt == args[1]:
               
                  arg2 = self.filterUserAssertion(args[2], stmt, left_stmt)
 
                  if len(arg2)==0:  
                  
                     return ['not',arg1]
                  
                  elif arg2 is None:
                  
                     return ['not',arg1]
                  
                  else:
                  
                     return ['and',['not',arg1],arg2] 

                  
               else:
               
                  return args[0]              

        else:
        
           return expression
        
        
    

  
            

    '''
    #
    #Get Assertion Left part
    #
    '''
    
    def filterUserAssertionLeft(self, expression):
    
        if expression[0]=='ite':
        
           if expression[-1][0]=='ite':
        
              #return self.filterUserAssertionLeft(expression[-1])
              return expression
              
           else:
           
              return expression[-1]
           
        else:
        
           return expression
        

    '''
    #
    #Recursive Substitution Process
    #
    '''
    def recSubstitutionProcess(self, fun_name, func_call_graph):
    
            parameter_info = self.map_all_function[fun_name]
            
            
            
            fun_calls = func_call_graph[fun_name]
            
            if len(fun_calls)>0:
            
               for sub_fun_name in fun_calls:
               
                  self.recSubstitutionProcess(sub_fun_name, func_call_graph)
                  
                  
               parameter_info = self.map_all_function[fun_name]
               
               if parameter_info[2] is not None and len(parameter_info[2])>0:
               
                   parameter_info[2], parameter_info[3] = self.functionSubstitutionProcess(parameter_info[2], parameter_info[3])
                  
                   self.map_all_function[fun_name] = parameter_info
                                                   
            self.Map_Simplify_Fun[fun_name]=fun_name
        

    '''
    #
    #Function Substitution Process
    #
    '''
    def updateSEQProcess(self, IRCode,LIST_EQ):
    
        if IRCode[1]=='seq':
        
           if IRCode[2][1]=='seq':
           
              self.updateSEQProcess(IRCode[2],LIST_EQ)
           
           else:
           
              LIST_EQ.append(IRCode[2])
              
              
           if IRCode[3][1]=='seq':
           
              self.updateSEQProcess(IRCode[3],LIST_EQ)
           
           else:
           
              LIST_EQ.append(IRCode[3])
              

        elif IRCode[1]=='while':
        
            TEMP_LIST_EQ =[]
            
            self.updateSEQProcess(IRCode[3], TEMP_LIST_EQ)
            
            TEMP_LIST_EQ = self.IRTranslation( TEMP_LIST_EQ, Var_Facts)
            
            LIST_EQ.append(['-1','while', IRCode[2], TEMP_LIST_EQ])
            
               
        elif IRCode[1]=='if1':
        
            TEMP_LIST_EQ =[]  
              
            self.updateSEQProcess(IRCode[3], TEMP_LIST_EQ)
            
            TEMP_LIST_EQ = self.IRTranslation( TEMP_LIST_EQ, Var_Facts)
            
            LIST_EQ.append(['-1','if1', IRCode[2], TEMP_LIST_EQ])
            
                        
        elif IRCode[1]=='if2':
        
            Var_Facts={}
        
            TEMP_LIST_EQ1 =[] 
        
            self.updateSEQProcess(IRCode[3], TEMP_LIST_EQ1)
            
            TEMP_LIST_EQ1 = self.IRTranslation( TEMP_LIST_EQ1, Var_Facts)
            
            TEMP_LIST_EQ2 =[] 
              
            self.updateSEQProcess(IRCode[4], TEMP_LIST_EQ2) 
            
            TEMP_LIST_EQ2 = self.IRTranslation( TEMP_LIST_EQ2, Var_Facts)
            
            LIST_EQ.append(['-1','if2', IRCode[2], TEMP_LIST_EQ1, TEMP_LIST_EQ2])
            
            
        else:
        
           LIST_EQ.append(IRCode)
           
        return LIST_EQ
        


    '''
    #
    #Is Adddress Variables as right hand expression
    #
    '''
    def findIsSuicdal(self, expression):
    
        if 'solidity' in expression[-2][0] and  'solidity' in expression[-2][0]:   
        
               return True
                          
        return False



    '''
    #
    #Add Arbitrary Address assigment 
    #
    '''
    def addIsSuicdal(self, Vfact):
    
        self.user_assert_count=self.user_assert_count+1
                   
        expr_assert = ['_'+str(self.user_assert_count)+'user_assert', ['or',['==',['IsProtected'],['True']],['==',['IsPrivate'],['True']]]]
        
        if '_'+str(self.user_assert_count)+'user_assert' not in Vfact:
               
            list_fact=[]
                    
            list_fact.append('_y'+str(len(Vfact)+1))
               
            list_fact.append('expression') 
                                
            list_fact.append('expression') 
            
            Vfact['_'+str(self.user_assert_count)+'user_assert']=list_fact


        USER_ASSERT = ['-1','=', expr_assert, ['True']]
        

        return USER_ASSERT, Vfact





    '''
    #
    #Is Adddress Variables as right hand expression
    #
    '''
    def findAddressAsStmt(self, expr_right):
    
        for term in self.user_assert_fun_call_list:
        
            address = term[0]
            
            if address[0]=='convertToaddress':
            
               address = address[1]
                        
            if address==expr_right:
               
               return True
               
            elif address[0].endswith('array') and expr_right[0].endswith('array') and address[0]==expr_right[0] and address[1]==expr_right[1]:
            
               return True
                          
        return False
        

    '''
    #
    #Is Adddress Variables is local 
    #
    '''
    def isLocalVarAddress(self, address, Vfact):
    
        if address[0]=='convertToaddress':
        
           address = address[1]
           
           
        if address[0].endswith('array') and (address[1][0].split('_')[0] in list(self.Map_inheritance.keys())):
        
           return None, Vfact
        
        elif (address[0].split('_')[0] in list(self.Map_inheritance.keys())):

           return None, Vfact        
        
        else:

           self.user_assert_count=self.user_assert_count+1
                   
           expr_assert = ['_'+str(self.user_assert_count)+'user_assert', ['or',['==',['IsProtected'],['True']],['or',['==',['IsPrivate'],['True']],['!=',address,['msg_sender']]]]]

           if '_'+str(self.user_assert_count)+'user_assert' not in Vfact:
               
              list_fact=[]
                    
              list_fact.append('_y'+str(len(Vfact)+1))
               
              list_fact.append('expression') 
                                
              list_fact.append('expression') 
            
              Vfact['_'+str(self.user_assert_count)+'user_assert']=list_fact

           if 'msg_sender' not in Vfact:
               
              list_fact=[]
                    
              list_fact.append('_y'+str(len(Vfact)+1))
                  
              list_fact.append('address') 
                                                         
              Vfact['msg_sender']=list_fact

           USER_ASSERT = ['-1','=', expr_assert, ['True']]

           return USER_ASSERT, Vfact 
           
    
        return None, Vfact
           
        

    '''
    #
    #Add Arbitrary Address assigment 
    #
    '''
    def addArbitraryAddress(self, expr_right, Vfact):
    
        self.user_assert_count=self.user_assert_count+1
                   
        expr_assert = ['_'+str(self.user_assert_count)+'user_assert', ['or',['==',['IsProtected'],['True']],['!=',expr_right,['msg_sender']]]]
        
        
        if '_'+str(self.user_assert_count)+'user_assert' not in Vfact:
               
            list_fact=[]
                    
            list_fact.append('_y'+str(len(Vfact)+1))
               
            list_fact.append('expression') 
                                
            list_fact.append('expression') 
            
            Vfact['_'+str(self.user_assert_count)+'user_assert']=list_fact

        if 'msg_sender' not in Vfact:
               
            list_fact=[]
                    
            list_fact.append('_y'+str(len(Vfact)+1))
                  
            list_fact.append('address') 
                                                         
            Vfact['msg_sender']=list_fact


        USER_ASSERT = ['-1','=', expr_assert, ['True']]
        

        return USER_ASSERT, Vfact




    '''
    #
    #Is Adddress present as parameters
    #
    '''
    def findAddress(self, parameters):
    
        for term in self.user_assert_fun_call_list:
        
            address = term[0]
                        
            for parameter in parameters:
        
                if address in parameter or address==parameter:
                
                   #self.reentrancy_address_map[self.reenterncy_var_count+1]=address
                              
                   return True
               
        return False
        

    '''
    #
    #Is LOWER LEVEL CALL, SENT AND TRANSFER
    #
    '''        
    def IsLowerLevelfunctionCall(self, expression, Vfact):
    
        
        if expression[3][0]=='LOW_LEVEL_CALL_call' or expression[3][0]=='LOW_LEVEL_CALL_send' or  expression[3][0]=='LOW_LEVEL_CALL_Transfer':
        
          
          
        
           self.reentrancy_address_map[self.reenterncy_var_count+1]=str(self.reenterncy_var_count+1)+'reentrancy_verifier'
        
          
           #address=expression[3][1]
           
           #user_assert,Vfact = self.isLocalVarAddress(address, Vfact)
           
           #for term in self.user_assert_fun_call_list:
        
           #    parameter = term[0]
               
           #    if address in parameter or address==parameter: 
                              
           #       new_expression, Vfact = self.addReenterncyCondCase(expression, Vfact)
                                  
           #       if user_assert is None:
                    
           #          return new_expression, Vfact
                     
           #       else:
                  
           #          return ['-1','seq',user_assert,new_expression], Vfact
                     
                  
           #if user_assert is None:
           
           #   return expression, Vfact
           
           #else:
           
           #return ['-1','seq',user_assert,new_expression], Vfact

           
           return expression, Vfact
              
        elif expression[3][0]=='LOW_LEVEL_CALL_delegatecall':
        
           address=expression[3][1]
           
           user_assert,Vfact = self.isLocalVarAddress(address, Vfact)

           if user_assert is None:
           
              return expression, Vfact
           
           else:
           
              return ['-1','seq',user_assert,expression], Vfact           
           
        else:
        
           return expression, Vfact
               

    '''
    #
    #GET REENTERNCEY  CONDITION FOR CHECK
    #
    '''
    def getReenterncyCondition(self,output_map_o, output_map_a, rec_ax_list,flag_var):
    
    
        if len(rec_ax_list):
        
           for ax in rec_ax_list:
           
               if flag_var in ax:
                              
                  return self.getListOfConditionRec(rec_ax_list[ax][-1])
    
        for x in output_map_o:
          
            ex = output_map_o[x]
            
            if flag_var in ex[-2]:
            
               return self.getListOfConditionRec(ex[-1])
               




    def getListOfConditionRec(self, ex):
    
        args=folTras.expr_args(ex)
        
        op=folTras.expr_op(ex)
                
        COND=None
        
        if op=='ite':
                   
           if args[1]==['1']:
           
              COND=args[0]
              
           elif args[2]==['1']:
           
              COND=['not',args[0]]   
              
           else:
         
              op1=folTras.expr_op(args[1])          

              op2=folTras.expr_op(args[2]) 
              
              if op1=='ite':     
              
                 RET_COND = self.getListOfConditionRec(args[1])
                 
                 if RET_COND is not None:             
           
                    COND=['and', args[0], RET_COND]
                    
                 else:
                 
                    COND= None
                 


              if op2=='ite':     
              
                 RET_COND = self.getListOfConditionRec(args[1])
                 
                 if RET_COND is not None:             
           
                    COND=['and', ['not',args[0]], RET_COND]
                    
                 else:
                 
                    COND= None

        return  COND       



    '''
    #
    #GET REENTERNCEY ELSE CONDITION FOR CHECK
    #
    '''
    def getReenterncyConditionElse(self,output_map_o, output_map_a, rec_ax_list,flag_var):
    
        if len(rec_ax_list):
        
           for ax in rec_ax_list:
           
               if flag_var in ax:
               
                  return self.getListOfConditionRecElse(rec_ax_list[ax][-1])
               
    
        for x in output_map_o:
          
            ex = output_map_o[x]
            
            if flag_var in ex[-2]:
            
               return self.getListOfConditionRecElse(ex[-1])
               




    def getListOfConditionRecElse(self, ex):
    
        args=folTras.expr_args(ex)
        
        op=folTras.expr_op(ex)
                
        COND=None
        
        if op=='ite':
                   
           if args[1]==['0']:
           
              COND=args[0]
              
           elif args[2]==['0']:
           
              COND=['not',args[0]]   
              
           else:
         
              op1=folTras.expr_op(args[1])          

              op2=folTras.expr_op(args[2]) 
              
              if op1=='ite':     
              
                 RET_COND = self.getListOfConditionRecElse(args[1])
                 
                 if RET_COND is not None:             
           
                    COND=['and', args[0], RET_COND]
                    
                 else:
                 
                    COND= None
                 


              if op2=='ite':     
              
                 RET_COND = self.getListOfConditionRecElse(args[1])
                 
                 if RET_COND is not None:             
           
                    COND=['and', ['not',args[0]], RET_COND]
                    
                 else:
                 
                    COND= None

        return  COND       





    '''
    #
    #GET CONDITION 
    #
    '''
    def extractCondition(self, cond1, cond2):
    
        args=folTras.expr_args(cond2)
        
        op=folTras.expr_op(cond2)
      
        if args[0]==cond1:
        
           return args[1]
           
        elif args[1]==cond1:
        
           return args[0]

        else:
        
           return None
         


        



    '''
    #
    #DETECT REENTERNCEY UPDATE
    #
    '''
    def checkReenterncy(self, program, Vfact, FUN_DETAILS):
            
        copy_program = copy.deepcopy(program)
        
        list_of_assertion = {}
        
        list_of_assertion2 = {}
        
        olther_var = {}
        
                
        
        update_program, Vfact, ReenterncyFound = self.updateReenterncyRec(copy_program, copy_program, Vfact)
        

        
        print('======================////////////////////////////')
        folTras.displayBody(update_program, '\t')
        print('======================////////////////////////////')        
       
        
        for x in self.reentrancy_address_map:
        
            update_program=['-1','seq',['-1','=',[self.reentrancy_address_map[x]],['0']],['-1','seq',['-1','=',[x],['0']],update_program]]
                        
            list_of_assertion[x+"1"]=folTras.expr2string1(['Not',['And',['==',[x+"1"],['1']],['==',[self.reentrancy_address_map[x]+"1"],['0']]]])
            list_of_assertion2[x+"1"]=folTras.expr2string1(['Not',['And',['==',[x+"1"],['1']],['==',[self.reentrancy_address_map[x]+"1"],['1']]]])  
            
            olther_var[x+"1"] = self.reentrancy_address_map[x]+"1"         
        
        main_prog=['-1','fun',['REENTERNCEY_VERIFICATION'],update_program]
        
        Var_Facts, output_map_o, output_map_a,f = self.translate_output(main_prog, Vfact, FUN_DETAILS)
        
        contraints_map={}
        
        rec_ax_list={}
        
        for w in output_map_a:
        
           if w[0]=='s1':

               contraints_map[w[1][1][1][0]]=w[1][1][2] 
               
           elif w[0]=='i1':
           
                if 'reentrancy_verifier' in w[-2][0]:
                
                   rec_ax_list[w[-2][0]]=w
           


        for ax in rec_ax_list:
        
            if rec_ax_list[ax][-3] in contraints_map:
            
               output_map_a.append(['a',['==',contraints_map[rec_ax_list[ax][-3]],['2']]])
               
               del contraints_map[rec_ax_list[ax][-3]]
        
        
        
        
        for flag_var in list_of_assertion:
        
            assert_str=list_of_assertion[flag_var]

            assert_str2=list_of_assertion2[flag_var]
        
            COND = self.getReenterncyCondition(output_map_o, output_map_a, rec_ax_list,flag_var)
            
            CONDELSE = self.getReenterncyConditionElse(output_map_o, output_map_a, rec_ax_list,flag_var)

            COND2 = self.getReenterncyCondition(output_map_o, output_map_a, rec_ax_list,olther_var[flag_var])
            
            #print('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>')
        
            #print(COND)
            
            #print(CONDELSE)
            
            #print(COND2)

            #print('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>')
                        
           
            if COND is not None:
                       
               output_map_a.append(['a',COND])
        
            status = self.Z3QUERY_CONSTRUCT(Var_Facts, output_map_o, output_map_a,f, assert_str)
            
            if status=='success':
            
               print('FUNCTION IS SINGLE REENTERNCEY')
               
            else:

               status = self.Z3QUERY_CONSTRUCT(Var_Facts, output_map_o, output_map_a,f, assert_str2) 
               
               if status=='success':
            
                  print('FUNCTION IS  VULNERABLE TO REENTERNCEY ATTACK')   
                  
                  break   
                  
               else:
                  
                  if COND is not None and COND2 is not None:
                  
                     cond_result = self.extractCondition( COND, COND2)
                     if cond_result is not None:
                        print('Stronger Condition Required : ')
                        print(folTras.expr2string1(cond_result))
                        
                  else:
                  
                        print('UNDECIDABLE')                  
        


        
        


    '''
    #
    #UPDATE REENTERNCEY PROGRAM
    #
    '''
    def updateReenterncyRecDuplicate(self, IRCode, Vfact, reentrancy_VAR):
    
        ReenterncyFound=None
    
        if IRCode[1]=='seq':

        
            if IRCode[2][1]=='=':
                               
                
                if reentrancy_VAR in IRCode[2][2][0]:
                
                
                   self.reenterncy_var_count = self.reenterncy_var_count+1
                   
                   IRCode[2][2][0]='_'+str(self.reenterncy_var_count)+'reentrancy_verifier'
                                      
                   self.reentrancy_address_map[reentrancy_VAR]='_'+str(self.reenterncy_var_count)+'reentrancy_verifier'
                   
                   if '_'+str(self.reenterncy_var_count)+'reentrancy_verifier' not in Vfact:
               
                      list_fact=[]
                    
                      list_fact.append('_y'+str(len(Vfact)+1))
                  
                      list_fact.append('uint256') 
                                                         
                      Vfact['_'+str(self.reenterncy_var_count)+'reentrancy_verifier']=list_fact

                
                                  
                                      
                IRCode[3], Vfact =self.updateReenterncyRecDuplicate(IRCode[3], Vfact, reentrancy_VAR)
                
                   
                                  
            else:
            
                IRCode[2], Vfact =self.updateReenterncyRecDuplicate(IRCode[2], Vfact, reentrancy_VAR)
                

                IRCode[3], Vfact =self.updateReenterncyRecDuplicate(IRCode[3], Vfact, reentrancy_VAR)
                            
           
        elif IRCode[1]=='while':
        
            
            IRCode[3],Vfact = self.updateReenterncyRecDuplicate(IRCode[3], Vfact, reentrancy_VAR)
            
               
        elif IRCode[1]=='if1':
        
              
            IRCode[3],Vfact = self.updateReenterncyRecDuplicate(IRCode[3], Vfact, reentrancy_VAR)
            
                        
        elif IRCode[1]=='if2':
        
            IRCode[3],Vfact = self.updateReenterncyRecDuplicate(IRCode[3], Vfact, reentrancy_VAR)
              
            IRCode[4],Vfact = self.updateReenterncyRecDuplicate(IRCode[4], Vfact, reentrancy_VAR) 
            
            
        else:
                           

            if IRCode[1]=='=':
            
                if reentrancy_VAR in IRCode[2][0]:
                
                
                   self.reenterncy_var_count = self.reenterncy_var_count+1
                   
                   IRCode[2][0]='_'+str(self.reenterncy_var_count)+'reentrancy_verifier'
                   
                   self.reentrancy_address_map[reentrancy_VAR]='_'+str(self.reenterncy_var_count+1)+'reentrancy_verifier'
                
                
                if reentrancy_VAR in IRCode[3][0]:
                
                   self.reenterncy_var_count = self.reenterncy_var_count+1
                   
                   IRCode[3][0]='_'+str(self.reenterncy_var_count)+'reentrancy_verifier'
                   
                   self.reentrancy_address_map[reentrancy_VAR]='_'+str(self.reenterncy_var_count+1)+'reentrancy_verifier'
                   
            

        return  IRCode, Vfact   





    
    '''
    #
    #UPDATE REENTERNCEY PROGRAM
    #
    '''
    def updateReenterncyRec(self, IRCode, program, Vfact):
    
        ReenterncyFound=None
    
        if IRCode[1]=='seq':

        
            if IRCode[2][1]=='=':
                               
                IRCode[2], Vfact, ReenterncyFound = self.updateReenterncyRec(IRCode[2], program, Vfact)
                
                if ReenterncyFound is not None:
                                  
                   ReenterncyFound=None
                   
                   sub_program = copy.deepcopy(program)
 
                   reentrancy_VAR = IRCode[2][2][2][0]
                   
                   sub_program, Vfact = self.updateReenterncyRecDuplicate(sub_program, Vfact, reentrancy_VAR)
                   
                   IRCO1=['-1', 'seq',IRCode[2][2],sub_program]
                   
                   IRCode[2] = ['-1', 'seq', IRCO1, IRCode[2][3]]
                   
                   #IRCO2=IRCode[2][3]
                   
                   #IRCode[2] = ['-1', 'seq', sub_program, IRCode[2]]
                   

                IRCode[3], Vfact, ReenterncyFound =self.updateReenterncyRec(IRCode[3], program, Vfact)
                
                if ReenterncyFound is not None:
                                   
                   ReenterncyFound=None
                   
                   sub_program = copy.deepcopy(program)
                   
                   IRCO1=['-1', 'seq',IRCode[3][2],sub_program]
                   
                   IRCode[3] = ['-1', 'seq', IRCO1, IRCode[3][3]]

                   
                   #IRCode[3] = ['-1', 'seq', sub_program, IRCode[3]]
                   
                                  
            else:
            
                IRCode[2], Vfact, ReenterncyFound =self.updateReenterncyRec(IRCode[2], program, Vfact)
                
                if ReenterncyFound is not None:
                
                   ReenterncyFound=None
                   
                   sub_program = copy.deepcopy(program)
                   
                   IRCO1=['-1', 'seq',IRCode[2][2],sub_program]
                   
                   IRCode[2] = ['-1', 'seq', IRCO1, IRCode[2][3]]
                   
                   #IRCode[2] = ['-1', 'seq', sub_program, IRCode[2]]
                   

                
                IRCode[3], Vfact, ReenterncyFound =self.updateReenterncyRec(IRCode[3], program, Vfact)
                
                if ReenterncyFound is not None:
                
                   ReenterncyFound=None

                   sub_program = copy.deepcopy(program)
                   
                   IRCO1=['-1', 'seq',IRCode[3][2],sub_program]
                   
                   IRCode[3] = ['-1', 'seq', IRCO1, IRCode[3][3]]
                   
                   #IRCode[3] = ['-1', 'seq', sub_program, IRCode[3]]                   

                   
            
           
        elif IRCode[1]=='while':
        
            
            IRCode[3],Vfact,ReenterncyFound = self.updateReenterncyRec(IRCode[3], program, Vfact)
            
               
        elif IRCode[1]=='if1':
        
              
            IRCode[3],Vfact,ReenterncyFound = self.updateReenterncyRec(IRCode[3], program, Vfact)
            
                        
        elif IRCode[1]=='if2':
        
            IRCode[3],Vfact,ReenterncyFound = self.updateReenterncyRec(IRCode[3], program, Vfact)
              
            IRCode[4],Vfact,ReenterncyFound = self.updateReenterncyRec(IRCode[4], program, Vfact) 
            
            
        else:
                           

            if IRCode[1]=='=':
            
                if 'LOW_LEVEL_CALL_call' in IRCode[2] or 'LOW_LEVEL_CALL_send' in IRCode[2] or 'LOW_LEVEL_CALL_Transfer' in IRCode[2] :
                
                
                   ReenterncyFound='FOUND'

                   self.reenterncy_var_count = self.reenterncy_var_count+1
                   
                   IRCode = ['-1','seq',['-1','=',['_'+str(self.reenterncy_var_count)+'reentrancy_verifier'],['1']],IRCode]
                   
                   self.reentrancy_address_map['_'+str(self.reenterncy_var_count)+'reentrancy_verifier']=None
                   
                   if '_'+str(self.reenterncy_var_count)+'reentrancy_verifier' not in Vfact:
               
                      list_fact=[]
                    
                      list_fact.append('_y'+str(len(Vfact)+1))
                  
                      list_fact.append('uint256') 
                                                         
                      Vfact['_'+str(self.reenterncy_var_count)+'reentrancy_verifier']=list_fact

                
                
                if 'LOW_LEVEL_CALL_call' in IRCode[3] or 'LOW_LEVEL_CALL_send' in IRCode[3] or 'LOW_LEVEL_CALL_Transfer' in IRCode[3] :
                
                
                   ReenterncyFound='FOUND'
                   
                   self.reenterncy_var_count = self.reenterncy_var_count+1
                   
                   IRCode = ['-1','seq',['-1','=',['_'+str(self.reenterncy_var_count)+'reentrancy_verifier'],['1']],IRCode]
                   
                   self.reentrancy_address_map['_'+str(self.reenterncy_var_count)+'reentrancy_verifier']=None

                   if '_'+str(self.reenterncy_var_count)+'reentrancy_verifier' not in Vfact:
               
                      list_fact=[]
                    
                      list_fact.append('_y'+str(len(Vfact)+1))
                  
                      list_fact.append('uint256') 
                                                         
                      Vfact['_'+str(self.reenterncy_var_count)+'reentrancy_verifier']=list_fact
                   
            

        return  IRCode, Vfact, ReenterncyFound   





    '''
    #
    #Add REENTERNCEY VARIABLE
    #
    '''
    def addReenterncyCondCase(self, expression, Vfact):
    
        REENTRENCY_COUNT=None
                       
        address=expression[-1][1]
        
        for key_reentrancy in self.reentrancy_address_map:
           
            parameter = self.reentrancy_address_map[key_reentrancy]
           
            if address in parameter or address==parameter:
            
               REENTRENCY_COUNT=key_reentrancy
    

        if REENTRENCY_COUNT is None:
        
           self.reenterncy_var_count=self.reenterncy_var_count+1
           
           self.reentrancy_address_map[self.reenterncy_var_count]=address
        
           expr_temp =['-1','if2',['==',['_'+str(self.reenterncy_var_count)+'reentrancy'],expression[3][2]],['-1','=',['_'+str(self.reenterncy_var_count)+'IsReentrancy'],['0']],['-1','=',['_'+str(self.reenterncy_var_count)+'IsReentrancy'],['-1']]]  
                   
           if '_'+str(self.reenterncy_var_count)+'IsReentrancy' not in Vfact:
               
                 list_fact=[]
                    
                 list_fact.append('_y'+str(len(Vfact)+1))
                  
                 list_fact.append('uint256') 
                                                         
                 Vfact['_'+str(self.reenterncy_var_count)+'IsReentrancy']=list_fact
                 
           self.user_assert_count=self.user_assert_count+1
                   
           expr_assert = ['_'+str(self.user_assert_count)+'user_assert', ['_'+str(self.reenterncy_var_count)+'IsReentrancy']]
                                                             
           if '_'+str(self.user_assert_count)+'user_assert' not in Vfact:
               
                 list_fact=[]
                    
                 list_fact.append('_y'+str(len(Vfact)+1))
               
                 list_fact.append('expression') 
                                
                 list_fact.append('expression') 
                                                         
                 Vfact['_'+str(self.user_assert_count)+'user_assert']=list_fact
                      
           USER_ASSERT = ['-1','=', expr_assert, ['0']]

           new_expression=['-1','seq',['-1','seq',expression,expr_temp],USER_ASSERT]
           
           return new_expression, Vfact
        
        
        else:
    
           expr_temp =['-1','if2',['==',['_'+str(REENTRENCY_COUNT)+'reentrancy'],expression[3][2]],['-1','=',['_'+str(REENTRENCY_COUNT)+'IsReentrancy'],['0']],['-1','=',['_'+str(REENTRENCY_COUNT)+'IsReentrancy'],['-1']]]  
                   
           if '_'+str(REENTRENCY_COUNT)+'IsReentrancy' not in Vfact:
               
                 list_fact=[]
                    
                 list_fact.append('_y'+str(len(Vfact)+1))
                  
                 list_fact.append('uint256') 
                                                         
                 Vfact['_'+str(REENTRENCY_COUNT)+'IsReentrancy']=list_fact

           self.user_assert_count=self.user_assert_count+1
                   
           expr_assert = ['_'+str(self.user_assert_count)+'user_assert', ['_'+str(REENTRENCY_COUNT)+'IsReentrancy']]
                                                             
           if '_'+str(self.user_assert_count)+'user_assert' not in Vfact:
               
                 list_fact=[]
                    
                 list_fact.append('_y'+str(len(Vfact)+1))
               
                 list_fact.append('expression') 
                                
                 list_fact.append('expression') 
                                                         
                 Vfact['_'+str(self.user_assert_count)+'user_assert']=list_fact
                      
           USER_ASSERT = ['-1','=', expr_assert, ['0']]

           new_expression=['-1','seq',['-1','seq',expression,expr_temp],USER_ASSERT]
           
           return new_expression, Vfact
         



    '''
    #
    #UPDATE REENTERNCEY VARIABLE
    #
    '''
    def addReenterncyVariable(self, expression, Vfact):
    
        REENTRENCY_COUNT=None
               
        #address=expression[-2][2]
        
                          
        #for key_reentrancy in self.reentrancy_address_map:
           
        #    parameter = self.reentrancy_address_map[key_reentrancy]
           
        #    if address in parameter or address==parameter:
            
        #       REENTRENCY_COUNT=key_reentrancy
    
        
        #if REENTRENCY_COUNT is None:
        
        #   self.reenterncy_var_count=self.reenterncy_var_count+1
           
        #   self.reentrancy_address_map[self.reenterncy_var_count]=address
        
        #   temp_expression1 = ['-1','=',['_'+str(self.reenterncy_var_count)+'reentrancy'],expression[-2]]
        
        #   temp_expression2 = ['-1','=',['_'+str(self.reenterncy_var_count)+'reentrancy'],['-',['_'+str(self.reenterncy_var_count)+'reentrancy'],expression[-2]]]
        
        #   update_expression = ['-1','seq',temp_expression1,['-1','seq',expression,temp_expression2]]
        

        #   if '_'+str(self.reenterncy_var_count)+'reentrancy' not in Vfact:
               
        #      list_fact=[]
                    
        #      list_fact.append('_y'+str(len(Vfact)+1))
                  
        #      list_fact.append(Vfact[expression[-2][0]][-1]) 
                                                         
        #      Vfact['_'+str(self.reenterncy_var_count)+'reentrancy']=list_fact
        
        
        #else:
    

        
        #   temp_expression1 = ['-1','=',['_'+str(REENTRENCY_COUNT)+'reentrancy'],expression[-2]]
        
        #   temp_expression2 = ['-1','=',['_'+str(REENTRENCY_COUNT)+'reentrancy'],['-',['_'+str(REENTRENCY_COUNT)+'reentrancy'],expression[-2]]]
        
        #   update_expression = ['-1','seq',temp_expression1,['-1','seq',expression,temp_expression2]]
        

        #   if '_'+str(REENTRENCY_COUNT)+'reentrancy' not in Vfact:
               
        #      list_fact=[]
                    
        #      list_fact.append('_y'+str(len(Vfact)+1))
                  
        #      list_fact.append(Vfact[expression[-2][0]][-1]) 
                                                         
        #      Vfact['_'+str(REENTRENCY_COUNT)+'reentrancy']=list_fact

        return expression, Vfact
        #return update_expression, Vfact




    '''
    #
    #Require convert to If
    #
    '''
    def require2IfProcess(self, IRCode, Vfact):
    
   
        if IRCode[1]=='seq':
        

        
            if IRCode[2][1]=='=':
            

                if 'solidity' in IRCode[2][2][0] and 'SOLIDITY_CALL_require' in IRCode[2][3][0]:
                
                    
                    req_parameters = folTras.expr_args(IRCode[2][3])
                    

 
                    
                    if len(req_parameters)==1:
                    
                       IRCode[1]='if1'
                    
                       IRCode[2]=req_parameters[0]
                    
                       IRCode[3], Vfact =self.require2IfProcess(IRCode[3], Vfact)
                       

                       
                    else:
               
               
                       IRCode[1]='if1'
                    
                       IRCode[2]=req_parameters[0]
                    
                       IRCode[3], Vfact =self.require2IfProcess(IRCode[3], Vfact)
                       
                    
                else:               
                
                    IRCode[3], Vfact =self.require2IfProcess(IRCode[3], Vfact)
                                
            else:
            
                IRCode[2], Vfact =self.require2IfProcess(IRCode[2], Vfact)
                
                IRCode[3], Vfact =self.require2IfProcess(IRCode[3], Vfact)
            
           
        elif IRCode[1]=='while':
        

            IRCode[3],Vfact = self.require2IfProcess(IRCode[3], Vfact)
           
               
        elif IRCode[1]=='if1':
        
              
            IRCode[3],Vfact = self.require2IfProcess(IRCode[3], Vfact)
            
                        
        elif IRCode[1]=='if2':
        
            IRCode[3],Vfact = self.require2IfProcess(IRCode[3], Vfact)
              
            IRCode[4],Vfact = self.require2IfProcess(IRCode[4], Vfact) 
            
            
        #else:
                           
        #    if IRCode[1]=='=':
                            
        #        if 'solidity' in IRCode[2][0] and 'SOLIDITY_CALL_require' in IRCode[3][0]:
                
        #            print('________________________//////////////')
                    
        #            print(IRCode[3])
                
        #            print('________________________//////////////')
                
                          
        return  IRCode, Vfact     





    '''
    #
    #Function Substitution Process
    #
    '''
    def updateSolidityCallProcess(self, IRCode, Vfact):
    
   
        if IRCode[1]=='seq':

        
            if IRCode[2][1]=='=':
            
            
                parameters = folTras.expr_args(IRCode[2][2])
                

                if 'solidity' in IRCode[2][2][0] and 'SOLIDITY_CALL_require' in IRCode[2][3][0]:
                
                    #print('________________________//////////////')
                    
                    req_parameters = folTras.expr_args(IRCode[2][3])
                    
                    #print(req_parameters)
                    
                    #print(IRCode[3])
                
                    #print('________________________//////////////')                
                
                
                
                if self.findIsSuicdal(IRCode[2]):
                

                   USER_ASSERT, Vfact = self.addIsSuicdal(Vfact)
                   
                   #IRCode[2] = ['-1', 'seq', USER_ASSERT, IRCode[2]]
                   
                
                elif self.findAddressAsStmt(IRCode[2][2]):
                
                   USER_ASSERT, Vfact = self.addArbitraryAddress(IRCode[2][2], Vfact)
                   
                   IRCode[2] = ['-1', 'seq', IRCode[2], USER_ASSERT]
                   
                
                
                elif self.findAddress(parameters):
                                   
                   IRCode[2], Vfact = self.addReenterncyVariable(IRCode[2], Vfact)
                   
                else:
                   
                   IRCode[2], Vfact = self.IsLowerLevelfunctionCall(IRCode[2], Vfact)
                   

                IRCode[3], Vfact =self.updateSolidityCallProcess(IRCode[3], Vfact)
                
                                      
                  
            else:
            
                IRCode[2], Vfact =self.updateSolidityCallProcess(IRCode[2], Vfact)
                
                IRCode[3], Vfact =self.updateSolidityCallProcess(IRCode[3], Vfact)
            
           
        elif IRCode[1]=='while':
        
            
            IRCode[3],Vfact = self.updateSolidityCallProcess(IRCode[3], Vfact)
            
               
        elif IRCode[1]=='if1':
        
              
            IRCode[3],Vfact = self.updateSolidityCallProcess(IRCode[3], Vfact)
            
                        
        elif IRCode[1]=='if2':
        
            IRCode[3],Vfact = self.updateSolidityCallProcess(IRCode[3], Vfact)
              
            IRCode[4],Vfact = self.updateSolidityCallProcess(IRCode[4], Vfact) 
            
            
        else:
                           

            if IRCode[1]=='=':
            
            
                parameters = folTras.expr_args(IRCode[2])
                

                #if 'solidity' in IRCode[2][0] and 'SOLIDITY_CALL_require' in IRCode[3][0]:
                
                #    print('________________________//////////////')
                    
                #    print(IRCode[3])
                
                #    print('________________________//////////////')
                
                if self.findIsSuicdal(IRCode):
                
                   USER_ASSERT, Vfact = self.addIsSuicdal(Vfact)
                   
                   #IRCode = ['-1', 'seq', USER_ASSERT, IRCode]
                   
                   
                
                elif self.findAddressAsStmt(IRCode[2]):
                
                   USER_ASSERT, Vfact = self.addArbitraryAddress(IRCode[2], Vfact)
                   
                   IRCode = ['-1', 'seq', IRCode, USER_ASSERT]
                  
                
 
                elif self.findAddress(parameters):
                
                
                   IRCode, Vfact = self.addReenterncyVariable(IRCode, Vfact)
                   
                else:
                   
                   IRCode, Vfact = self.IsLowerLevelfunctionCall(IRCode, Vfact)
                   


       
        return  IRCode, Vfact     


 

    '''
    #
    #Function Substitution Process
    #
    '''
    def functionSubstitutionProcess(self, IRCode, Vfact):
   
        if IRCode[1]=='seq':
        
            if IRCode[2][1]=='=':
            
               RET_IRCode=[]
               

               IRCode[2][2], Vfact = self.stmtSubstitutionProcess(IRCode[2][2], Vfact, RET_IRCode)
                              
               IRCode[2][3], Vfact = self.stmtSubstitutionProcess(IRCode[2][3], Vfact, RET_IRCode)
               
               if len(RET_IRCode)>0:
               
                  UPDATE_RET_IRCode = self.IRTranslation(RET_IRCode, Vfact) 
                  
                  if 'dummy' in IRCode[2][2][0]:
                  
                     if IRCode[2][2][0] in Vfact:
                  
                        del Vfact[IRCode[2][2][0]]
                                              
                     IRCode[2] = UPDATE_RET_IRCode
                  
                  else:
                  
                     IRCode[2] = ['-1', 'seq', UPDATE_RET_IRCode, IRCode[2]]

            
            IRCode[3], Vfact =self.functionSubstitutionProcess(IRCode[3], Vfact)
                         
        elif IRCode[1]=='while':
        
            RET_IRCode=[]
           
            IRCode[2],Vfact = self.stmtSubstitutionProcess(IRCode[2], Vfact, RET_IRCode)
            
            IRCode[3],Vfact = self.functionSubstitutionProcess(IRCode[3], Vfact)
            
            if len(RET_IRCode)>0:
            
               UPDATE_RET_IRCode = self.IRTranslation(RET_IRCode, Vfact) 
               
               IRCode = ['-1', 'seq', UPDATE_RET_IRCode, IRCode]
               
               
        elif IRCode[1]=='if1':
        
            RET_IRCode=[]
           
            IRCode[2],Vfact = self.stmtSubstitutionProcess(IRCode[2], Vfact, RET_IRCode)
              
            IRCode[3],Vfact = self.functionSubstitutionProcess(IRCode[3], Vfact)
            
            if len(RET_IRCode)>0:
            
               UPDATE_RET_IRCode = self.IRTranslation(RET_IRCode, Vfact)   
               
               IRCode = ['-1', 'seq', UPDATE_RET_IRCode, IRCode]            
               
               
              
        elif IRCode[1]=='if2':
        
            RET_IRCode=[]
           
            IRCode[2],Vfact = self.stmtSubstitutionProcess(IRCode[2], Vfact, RET_IRCode)
              
            IRCode[3],Vfact = self.functionSubstitutionProcess(IRCode[3], Vfact)
              
            IRCode[4],Vfact = self.functionSubstitutionProcess(IRCode[4], Vfact) 
            
            if len(RET_IRCode)>0:
            
               UPDATE_RET_IRCode = self.IRTranslation(RET_IRCode, Vfact)
               
               IRCode = ['-1', 'seq', UPDATE_RET_IRCode, IRCode]
               
       
        else:
           
            if IRCode[1]=='=':
            
               RET_IRCode=[]
                           
               IRCode[2], Vfact = self.stmtSubstitutionProcess(IRCode[2], Vfact, RET_IRCode)
                              
               IRCode[3], Vfact = self.stmtSubstitutionProcess(IRCode[3], Vfact, RET_IRCode)
               
               if len(RET_IRCode)>0:
               
                  UPDATE_RET_IRCode = self.IRTranslation(RET_IRCode, Vfact)
                  
                  if 'dummy' in IRCode[2][0]:
                  
                     if IRCode[2][0] in Vfact:
                  
                        del Vfact[IRCode[2][0]]
                     
                     IRCode = UPDATE_RET_IRCode
                     
                  else:
                  
                     IRCode = ['-1', 'seq', UPDATE_RET_IRCode, IRCode]
                     
               

        return  IRCode, Vfact     
 
    '''
    #
    #Function Replace Parameter Process
    #
    '''
    def functionReplaceProcess(self, IRCode, e1, e2):
    
         
        if IRCode[1]=='seq':
                   
            if IRCode[2][1]=='=':
                                                           
               IRCode[2][2] = folTras.expr_replace(IRCode[2][2],e1,e2) 
               
               IRCode[2][3] = folTras.expr_replace(IRCode[2][3],e1,e2) 
           
            IRCode[3]=self.functionReplaceProcess(IRCode[3],e1,e2)
                         
        elif IRCode[1]=='while':
           
            IRCode[2] = folTras.expr_replace(IRCode[2],e1,e2) 
              
            IRCode[3] = self.functionReplaceProcess(IRCode[3],e1,e2)
           
        elif IRCode[1]=='if1':
           
            IRCode[2] = folTras.expr_replace(IRCode[2],e1,e2) 
              
            IRCode[3] = self.functionReplaceProcess(IRCode[3],e1,e2)
              
        elif IRCode[1]=='if2':
           
            IRCode[2] = folTras.expr_replace(IRCode[2],e1,e2) 
              
            IRCode[3] = self.functionReplaceProcess(IRCode[3],e1,e2)
              
            IRCode[4] = self.functionReplaceProcess(IRCode[4],e1,e2)        
        else:
           
            if IRCode[1]=='=':
            

            
               IRCode[2] = folTras.expr_replace(IRCode[2],e1,e2) 
               
               IRCode[3] = folTras.expr_replace(IRCode[3],e1,e2) 

            
        return  IRCode     
   


    '''
    #
    #Function Each Statement  Process
    #
    '''
    def stmtSubstitutionProcess(self, expression, Vfact, RET_IRCode):
    
        args = folTras.expr_args(expression)
        
        op = folTras.expr_op(expression)
        
        if len(args)==0:
        
           return expression,Vfact
           
        else:
        
           if op in folTras._infix_op:
           
              list_parameter=[]
           
              for x in args:
              
                 update_x, Vfact = self.stmtSubstitutionProcess(x, Vfact, RET_IRCode)
              
                 list_parameter.append(update_x)
           
              return expression[:1]+list_parameter, Vfact
           
           else:
           
              update_op = op+'_'+str(len(args))
              
              #print(update_op)
              
              if update_op in self.map_all_function:
              
                 parameter_info = self.map_all_function[update_op]
                 
                 if len(parameter_info[1])==len(args):
                 
                    IRCODE = copy.deepcopy(parameter_info[2])
                    
                    VFACT = copy.deepcopy(parameter_info[3])
                    
                    #if update_op not in self.Map_Simplify_Fun:
            
                    #   if IRCODE is not None and len(IRCODE)>0:
            
                    #      parameter_info[2], parameter_info[3] = self.functionSubstitutionProcess(IRCODE, VFACT)
                          
                    #      self.map_all_function[update_op]=parameter_info
                  
                    #      self.Map_Simplify_Fun[update_op] = update_op
                    
                    
                    
                    if IRCODE is not None and len(IRCODE)>0:
                    
                       #print('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
                       #print(update_op)
                       #print('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%1')
                       #print(parameter_info[2])
                       #print('------------------------------------') 
                       #print(parameter_info[3])                   
                       #print('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%1')
                                        
                       for index in range(1,len(args)):
                    
                           #print(str(args[index])+'--------------------'+str([parameter_info[1][index]]))
                        
                           if parameter_info[1][index] in VFACT:
                        
                              del VFACT[parameter_info[1][index]]
                        

                           IRCODE = self.functionReplaceProcess(IRCODE, [parameter_info[1][index]], args[index])

                           
                       if 'ret' in VFACT:
                       
                          self.link_count=self.link_count+1
                       
                          link_var='lv_'+str(self.link_count)+'var'
                          
                          VFACT[link_var] = VFACT['ret']
                          
                          IRCODE = self.functionReplaceProcess(IRCODE, ['ret'], [link_var])
                       
                          del VFACT['ret']
                          
                          Vfact={**Vfact,**VFACT}
                          
                          #print('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%2')
                          #print(IRCODE)
                          #print('-------------------------------------') 
                          #print(VFACT)          
                          #print('-------------------------------------')   
                          #print(Vfact)       
                          #print('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%2')
                          #print('MATCH')
                          
                          RET_IRCode.append(IRCODE)
                          
                          
                          
                          return [link_var], Vfact
                          
                          #return expression[:1]+list(self.stmtSubstitutionProcess(x, Vfact, RET_IRCode) for x in args)
                          
                          
                       else:
                       
                          Vfact={**Vfact,**VFACT}
                       
                          #print('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%2')
                          #print(IRCODE)
                          #print('------------------------------------') 
                          #print(VFACT)    
                          #print('-------------------------------------')   
                          #print(Vfact)                      
                          #print('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%2')
                          #print('MATCH')
                          
                          RET_IRCode.append(IRCODE)
                                                    
                          list_parameter=[]
           
                          for x in args:
              
                             update_x, Vfact = self.stmtSubstitutionProcess(x, Vfact, RET_IRCode)
              
                             list_parameter.append(update_x)
              
                          return expression[:1]+list_parameter, Vfact
                          
                       
                       
                       #print('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%2')
                       #print(IRCODE)
                       #print('------------------------------------') 
                       #print(VFACT)                   
                       #print('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%2')
                       #print('MATCH')
                       
                       #return expression[:1]+list(self.stmtSubstitutionProcess(x, Vfact) for x in args)
                       
                    else:
                    
                       list_parameter=[]
           
                       for x in args:
              
                          update_x, Vfact = self.stmtSubstitutionProcess(x, Vfact, RET_IRCode)
              
                          list_parameter.append(update_x)
              
                       return expression[:1]+list_parameter, Vfact
                                           
                 else:
                 
                      list_parameter=[]
           
                      for x in args:
              
                          update_x, Vfact = self.stmtSubstitutionProcess(x, Vfact, RET_IRCode)
              
                          list_parameter.append(update_x)
              
                      return expression[:1]+list_parameter, Vfact
                                             
              else:
              
                 list_parameter=[]
           
                 for x in args:
              
                    update_x, Vfact = self.stmtSubstitutionProcess(x, Vfact, RET_IRCode)
              
                    list_parameter.append(update_x)
                    
              
                 return expression[:1]+list_parameter, Vfact
           
           
           
        
        
    
        
    '''
    #
    #Contruct Solidity CALL
    #
    '''
    def constructSolidityCall(self):
    
    
        Var_Facts={}
        
        
        Var_Facts['SOLIDITY_CALL_interfaceId']=['_y1','TYPE','bytes4']

        Var_Facts['SOLIDITY_CALL_require']=['_y1','bool','string','uint256']
        
        Var_Facts['SOLIDITY_CALL_revert_string']=['_y1','string','uint256']    
        
        Var_Facts['SOLIDITY_CALL_revert_uint256']=['_y1','uint256','uint256','uint256']       
 
        Var_Facts['SOLIDITY_CALL_assert']=['_y1','bool','uint256'] 
        
        
        self.solidity_call.append(['-1','=',['SOLIDITY_CALL_require',['False'],['_s1']],['-1']]) 
        
        self.solidity_call.append(['-1','=',['SOLIDITY_CALL_revert_string',['_s1']],['-2']]) 
        
        self.solidity_call.append(['-1','=',['SOLIDITY_CALL_revert_uint256',['_i1'],['_i2']],['-2']])  
        
        self.solidity_call.append(['-1','=',['SOLIDITY_CALL_assert',['False']],['-3']])        
    
        IR_code = self.IRTranslation(self.solidity_call, Var_Facts)
        
    
    
        if IR_code is not None:
                    
           main_prog=['-1','fun',['solidity_call'],IR_code]
                   
           print('')
           print('~~~~~~~~~~~~~~Details Start~~~~~~~~~~~~~~~~~')
           for vfact in Var_Facts:
                   
               fact_str = None 
                       
               for efact in Var_Facts[vfact][1:-1]:
                       
                   if fact_str is None:
                                 
                       fact_str =  efact
                              
                   else:  

                       fact_str =  fact_str+' X '+efact                                 
                   
                   if fact_str is None:
                       
                       fact_str = Var_Facts[vfact][-1]
                          
                   else:
                       
                       fact_str = fact_str+' --> '+Var_Facts[vfact][-1]
                          
                   print(vfact+" : "+fact_str)
                     
           print('~~~~~~~~~~~~~~Details End~~~~~~~~~~~~~~~~~')
           print('')
           assert_map_o={}
           assert_map_a=[]
                   
           output_map_o={}
           output_map_a=[]
                   
           assume_map_o={}
           assume_map_a=[] 
           
           
           print('!!!!!!!!!!!!!!!!!!!!!!!===================')
           print(main_prog)
           print('!!!!!!!!!!!!!!!!!!!!!!!===================')           
           
               
           f,o,a,cm,assert_list,assume_list,assert_key,output_val = folTras.translate1(main_prog,Var_Facts,1)
                   
     
                   
           Output_val=''
           Output_val+='\nOutput in normal notation:\n'
           Output_val+='\n1. Frame axioms:\n'
           Output_val+=folTras.eqset2string1Print(f)+"\n"
                                           

           Output_val+='\n2. Output equations:\n'
           Output_val+=folTras.eqset2string1Print(o)+"\n"
           Output_val+='\n3. Other axioms:\n'
           for x in a: 
               Output_val+=folTras.wff2string1(x)+"\n"
           print('~~~~~~~~~~~~~~Translation Start~~~~~~~~~~~~~~~~~')
           print('')
           print(Output_val)
           print('~~~~~~~~~~~~~~Translation End~~~~~~~~~~~~~~~~~')
           print('')
    
    
    


    '''
    #
    #Extract DATA TYPE OF VARIABLE 
    #
    '''
    def extractingType(self,stmt_type):
    
        if type(stmt_type)==slither.core.solidity_types.array_type.ArrayType:
        
           return 'array'
        
        elif type(stmt_type)==slither.core.solidity_types.mapping_type.MappingType:
        
           return 'map'
        
        else:
        
           return format(stmt_type)
    
 


        
    def getLoopNode(self, nodes_statements):
    
        stack_if=[]
        
        stack_loop=[]
        
        stack_loop_begin=[]
        
        stack_loop_end=[]
        
        stack_if_end=[]
        
        stack_expression = []
        
        update_nodes_statements = []
        
        current_nodes = []
        
        for node in nodes_statements:
        
            if node.type in [NodeType.IF]:
            
               stack_if.append(node)
                              
            elif node.type in [NodeType.IFLOOP]:
            
               stack_loop.append(node)
                
            elif node.type in [NodeType.STARTLOOP]:
            
               stack_loop_begin.append(node)
               
            elif node.type in [NodeType.ENDLOOP]:
            
               stack_loop_end.append(node)
               
            elif node.type in [NodeType.ENDIF]:
            
               stack_if_end.append(node)

            #print('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
            #print(node)   
            #for x in node.sons:
            #   print(x)                        
            #print('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
            stack_expression.append(node)
            
            
        filter_nodes = []

        for node in nodes_statements:


            if node not in filter_nodes:
        
               if node.type in [NodeType.IFLOOP]:
               

                  node_loop_start, node_loop_end = self.getLoopNodes(node, stack_loop, stack_loop_begin, stack_loop_end)
                  
                  nodes_block , filter_nodes = self.getLoopNodesBlocks(nodes_statements, filter_nodes, node_loop_start, node, node_loop_end, stack_loop, stack_loop_begin, stack_loop_end, stack_if, stack_if_end)
                                    
                  update_nodes_statements.append(['WHILE', node_loop_start, node, nodes_block, node_loop_end])
                  
                  
               elif node.type in [NodeType.IF]:
               
               
                  node_if_end = self.getIfLoopNodes(node, stack_if, stack_if_end)

                                                
                  nodes_if , filter_nodes = self.getIfLoopNodesBlock(nodes_statements, filter_nodes, node, node_if_end, stack_if, stack_if_end, stack_loop, stack_loop_begin, stack_loop_end)
                  
                  if nodes_if is not None:
                  
                                   
                     update_nodes_statements.append(nodes_if)
                     
                  else:
                  
                     print('Error if Case')
               
               else:
               
                 if node.type not in [NodeType.STARTLOOP, NodeType.ENDLOOP]:
            
                    update_nodes_statements.append(node)
                    
                     
        #self.displayBlockOFNodes(update_nodes_statements)
        
        return update_nodes_statements
         

           
        

        
    '''
    #
    #Get If Loop Nodes
    #
    '''
    def getIfLoopNodesBlock(self, nodes_statements, filterNodes, ifNode, node_if_end, stack_if, stack_if_end, stack_loop, stack_loop_begin, stack_loop_end):
    
        #print('@@@@@@@@@@@@@@@@@==================================start')
        
        next_node_If = None
        
        next_node_Else = None
        
        loop_body_If = []
        
        loop_body_Else = []
        
        filterNodes.append(ifNode)
        

        
        if node_if_end is None:
        
         
           next_node_If = ifNode.sons[0]
            
                     
           while next_node_If is not None:
                         
                 node = next_node_If
                 

                                                                    
                 if node not in filterNodes:
                 
                 
                    if node.type in [NodeType.IFLOOP]:
                 
                        node_loop_start, node_loop_end = self.getLoopNodes(node, stack_loop, stack_loop_begin, stack_loop_end)
                  
                        nodes_block, filterNodes =self.getLoopNodesBlocks(nodes_statements, filterNodes, node_loop_start, node, node_loop_end, stack_loop, stack_loop_begin, stack_loop_end, stack_if, stack_if_end)
                                                            
                        loop_body_If.append(['WHILE', node_loop_start, node, nodes_block, node_loop_end])
                        
                        
                        if len(node_loop_end.sons)==1:
                  
                            next_node_If = node_loop_end.sons[0]
                            
                            #print('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~1')
                                                
                        else:
                        
                            print('please checked the case 1')

                 
                    elif node.type in [NodeType.IF]:
                    
                    
                       node_if_end = self.getIfLoopNodes(node, stack_if, stack_if_end)
                              
                       nodes_if , filterNodes = self.getIfLoopNodesBlock(nodes_statements, filterNodes, node, node_if_end, stack_if, stack_if_end, stack_loop, stack_loop_begin, stack_loop_end)
                  
                       loop_body_If.append(nodes_if)
                       
                       if node_if_end is None:
                       
                            next_node_If = None
                       
                       elif len(node_if_end.sons)==1:
                  
                            next_node_If = node_loop_end.sons[0]
                            
                            #print('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~2')
                                                
                       else:
                        
                            print('please checked the case 1')
                            
                    elif node.type in [NodeType.RETURN]:
                        
                         loop_body_If.append(node)
                           
                         filterNodes.append(node)
                  
                         next_node_If = None 

                 
                    else:
                    
                 
                        loop_body_If.append(node)
                  
                        filterNodes.append(node)
                 
                        if len(node.sons)==1 and node.sons[0].type not in [NodeType.RETURN]:
                  
                           next_node_If = node.sons[0]
                
                        elif len(node.sons)==1 and node.sons[0].type in [NodeType.RETURN]:
                        
                           loop_body_If.append(node.sons[0])
                           
                           filterNodes.append(node.sons[0])
                  
                           next_node_If = None                        
           
          

          
           next_node_Else = ifNode.sons[1]
           
           while next_node_Else is not None:
                         
                 node = next_node_Else
                 
                                                   
                 if node not in filterNodes: 
                 
                 
                    if node.type in [NodeType.IFLOOP]:
                 
                        node_loop_start, node_loop_end = self.getLoopNodes(node, stack_loop, stack_loop_begin, stack_loop_end)
                  
                        nodes_block, filterNodes =self.getLoopNodesBlocks(nodes_statements, filterNodes, node_loop_start, node, node_loop_end, stack_loop, stack_loop_begin, stack_loop_end, stack_if, stack_if_end)
                                                            
                        loop_body_Else.append(['WHILE', node_loop_start, node, nodes_block, node_loop_end])
                        
                        
                        if len(node_loop_end.sons)==1:
                  
                            next_node_Else = node_loop_end.sons[0]
                            
                            #print('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~3')
                                                
                        else:
                        
                            print('please checked the case 1')

                 
                    elif node.type in [NodeType.IF]:
                    
                       node_if_end = self.getIfLoopNodes(node, stack_if, stack_if_end)
                              
                       nodes_if , filterNodes = self.getIfLoopNodesBlock(nodes_statements, filterNodes, node, node_if_end, stack_if, stack_if_end, stack_loop, stack_loop_begin, stack_loop_end)
                  
                       loop_body_Else.append(nodes_if)
                       
                       if node_if_end is None:
                       
                             next_node_Else = None
                       
                       elif len(node_if_end.sons)==1:
                  
                            next_node_Else = node_loop_end.sons[0]
                            
                            #print('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~4')
                                                
                       else:
                        
                            print('please checked the case 1')

                    elif node.type in [NodeType.RETURN]:
                        
                         loop_body_Else.append(node)
                           
                         filterNodes.append(node)
                  
                         next_node_Else = None 
                                            
                    else:
                    
                    
                       loop_body_Else.append(node)
                  
                       filterNodes.append(node)
                 
                       if len(node.sons)==1 and node.sons[0].type not in [NodeType.RETURN]:
                  
                           next_node_Else = node.sons[0]
                
                       elif len(node.sons)==1 and node.sons[0].type in [NodeType.RETURN]:
                       
                           loop_body_Else.append(node.sons[0])
                           
                           filterNodes.append(node.sons[0])
                  
                           next_node_Else = None           
 
        else:
         
           next_node_If = ifNode.sons[0]
           
           while next_node_If is not None:
                         
                 node = next_node_If
                                                  
                                                  
                 if node not in filterNodes:
                 
                 
                    if node.type in [NodeType.IFLOOP]:
                 
                        node_loop_start, node_loop_end = self.getLoopNodes(node, stack_loop, stack_loop_begin, stack_loop_end)
                  
                        nodes_block, filterNodes =self.getLoopNodesBlocks(nodes_statements, filterNodes, node_loop_start, node, node_loop_end, stack_loop, stack_loop_begin, stack_loop_end, stack_if, stack_if_end)
                                                            
                        loop_body_If.append(['WHILE', node_loop_start, node, nodes_block, node_loop_end])
                        
                        
                        if len(node_loop_end.sons)==1:
                  
                            next_node_If = node_loop_end.sons[0]
                            
                            #print('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~5')
                                                
                        else:
                        
                            print('please checked the case 1')

                 
                    elif node.type in [NodeType.IF]:
                    
                       node_if_end = self.getIfLoopNodes(node, stack_if, stack_if_end)
                              
                       nodes_if , filterNodes = self.getIfLoopNodesBlock(nodes_statements, filterNodes, node, node_if_end, stack_if, stack_if_end,stack_loop, stack_loop_begin, stack_loop_end)
                  
                       loop_body_If.append(nodes_if)
                       
                       if node_if_end is None:
                       
                            next_node_If = None
                       
                       elif len(node_if_end.sons)==1:
                  
                            next_node_If = node_if_end.sons[0]
                            
                            #print('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~6')
                                                
                       else:
                        
                            print('please checked the case 1')

                 
                    else:
                    
                 
                        loop_body_If.append(node)
                  
                        if node!=node_if_end:
                                     
                           filterNodes.append(node)
                 
                        if len(node.sons)==1 and node.sons[0].type not in [NodeType.RETURN,NodeType.ENDIF]:
                  
                           next_node_If = node.sons[0]
                
                        elif len(node.sons)==1 and node.sons[0].type in [NodeType.RETURN]:
                        
                           loop_body_If.append(node.sons[0])
                           
                           filterNodes.append(node.sons[0])
                  
                           next_node_If = None  
                           
                        elif len(node.sons)==1 and node.sons[0].type in [NodeType.ENDIF]:
                        #elif len(node.sons)==1 and node.sons[0]==node_if_end:
                        
                           loop_body_If.append(node.sons[0])
                           
                           filterNodes.append(node.sons[0])
                           
                           next_node_If = None                       
                 else:
           
                   next_node_If = None 
              
          
           next_node_Else = ifNode.sons[1]
           
           
           if next_node_Else.type in [NodeType.IFLOOP]:
           
           
              node = next_node_Else
                      
              node_if_end = self.getIfLoopNodes(node, stack_if, stack_if_end)
                              
              nodes_if , filterNodes = self.getIfLoopNodesBlock(nodes_statements, filterNodes, node, node_if_end, stack_if, stack_if_end, stack_loop, stack_loop_begin, stack_loop_end)
                  
              loop_body_Else.append(nodes_if)
              
                          
           elif next_node_Else.type in [NodeType.ENDIF]:
           
           
              filterNodes.append(next_node_Else)
                                   
              next_node_Else = None
              
            
           else:
           
            
              while next_node_Else is not None:
              
                    node = next_node_Else
                    
                    if node not in filterNodes: 
                    
                       if node.type in [NodeType.IFLOOP]:
                       
                          node_loop_start, node_loop_end = self.getLoopNodes(node, stack_loop, stack_loop_begin, stack_loop_end)
                  
                          nodes_block, filterNodes =self.getLoopNodesBlocks(nodes_statements, filterNodes, node_loop_start, node, node_loop_end, stack_loop, stack_loop_begin, stack_loop_end, stack_if, stack_if_end)
                                                            
                          loop_body_Else.append(['WHILE', node_loop_start, node, nodes_block, node_loop_end])
                        
                          if len(node_loop_end.sons)==1:
                  
                              next_node_Else = node_loop_end.sons[0]
                            
                              #print('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~7')
                                                
                          else:
                        
                              print('please checked the case 1')


                       elif node.type in [NodeType.IF]:
                    
                           node_if_end = self.getIfLoopNodes(node, stack_if, stack_if_end)
                              
                           nodes_if , filterNodes = self.getIfLoopNodesBlock(nodes_statements, filterNodes, node, node_if_end, stack_if, stack_if_end, stack_loop, stack_loop_begin, stack_loop_end)
                  
                           loop_body_Else.append(nodes_if)
                       
                           if node_if_end is None:
                       
                               next_node_Else = None
                       
                           elif len(node_if_end.sons)==1:
                  
                                next_node_Else = node_if_end.sons[0]
                            
                                #print('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~8')
                                                
                           else:
                        
                                print('please checked the case 1')
                    
                       else:
                    
                          loop_body_Else.append(node)
                       
                          if node!=node_if_end:
                                     
                             filterNodes.append(node)
                 
                          if len(node.sons)==1 and node.sons[0].type not in [NodeType.RETURN,NodeType.ENDIF]:
                  
                             next_node_Else = node.sons[0]
                
                          elif len(node.sons)==1 and node.sons[0].type in [NodeType.RETURN]:
                       
                             loop_body_Else.append(node.sons[0])
                           
                             filterNodes.append(node.sons[0])
                  
                             next_node_Else = None  
                          
                          elif len(node.sons)==1 and node.sons[0].type in [NodeType.ENDIF]:
                        
                             loop_body_Else.append(node.sons[0])
                           
                             filterNodes.append(node.sons[0])
                           
                             next_node_Else = None 
                   
                    else:
                                        
                      next_node_Else = None
                       
           #filterNodes.append(node_if_end)               
         

        
        if_node = None
        
        if len(loop_body_If)>0 and len(loop_body_Else)>0:
        
        
           if len(loop_body_If)>0 and len(loop_body_Else)==1 and type(loop_body_Else[0])==list and loop_body_Else[0][0]=='IF2':

           
               if_node = ['IF2',ifNode,loop_body_If,loop_body_Else[0]]
               
           
           else:
           
               if_node = ['IF2',ifNode,loop_body_If,loop_body_Else]
               
           
        elif len(loop_body_If)>0 and len(loop_body_Else)==0:
        
           
           if_node = ['IF1',ifNode,loop_body_If]
           
           
        #print(if_node)
        #print('@@@@@@@@@@@@@@@@@==================================end')  
        
        return if_node, filterNodes
    

    '''
    #
    #Get Loop Nodes
    #
    '''
    def getLoopNodesBlocks(self, nodes_statements, filterNodes, startNode, loopNode, endNode, stack_loop, stack_loop_begin, stack_loop_end, stack_if, stack_if_end):
    
        #print('XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX--------------START')
    
        next_node = None
        
        loop_body = []
        
        
        if loopNode.sons[1]==endNode:
        
           next_node = loopNode.sons[0]
           
        filterNodes.append(startNode) 
         
        filterNodes.append(loopNode)  
        

        if next_node is not None:
        
        
           while next_node is not None:
                         
                 node = next_node
                 
                                                 
                 if node not in filterNodes:
           

                    if node.type in [NodeType.IFLOOP]:
                 
                        node_loop_start, node_loop_end = self.getLoopNodes(node, stack_loop, stack_loop_begin, stack_loop_end)
                  
                        nodes_block, filterNodes =self.getLoopNodesBlocks(nodes_statements, filterNodes, node_loop_start, node, node_loop_end, stack_loop, stack_loop_begin, stack_loop_end, stack_if, stack_if_end)
                                                            
                        loop_body.append(['WHILE', node_loop_start, node, nodes_block, node_loop_end])
                        
                        
                        if len(node_loop_end.sons)==1:
                  
                            next_node = node_loop_end.sons[0]
                            
                            #print('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~1')
                                                
                        else:
                        
                            print('please checked the case 1')
                            
                            
                    elif node.type in [NodeType.IF]:
                    
                         node_if_end = self.getIfLoopNodes(node, stack_if, stack_if_end)
                                                
                         nodes_if , filterNodes = self.getIfLoopNodesBlock(nodes_statements, filterNodes, node, node_if_end, stack_if, stack_if_end, stack_loop, stack_loop_begin, stack_loop_end)
                  
                                   
                         if nodes_if is not None:
                                    
                             loop_body.append(nodes_if)
                     
                         else:
                  
                             print('Error if Case')
                             
                         if node_if_end is None:
                         
                            next_node = None
                    
                         elif len(node_if_end.sons)==1:
                        
                            next_node = node_if_end.sons[0]    
    
                    else:

                        if node.type not in [NodeType.STARTLOOP, NodeType.ENDLOOP]:
                      
                           loop_body.append(node)
                  
                           filterNodes.append(node)
                          
                        if len(node.sons)==1 and node.sons[0]!=endNode:
                  
                            next_node = node.sons[0]
                     
                        elif len(node.sons)==1 and node.sons[0]==endNode:
                  
                            #print('------------loop end-----------1')
                            
                            next_node = None
                     
                            break;      
                                 
                        elif len(node.sons)==2 and node.sons[1]==endNode:
                        
                            #print('------------loop end-----------2')
                            
                            next_node = None
                     
                            break; 
                            
                 elif node in filterNodes:
                 
                     if node==loopNode:

                        #print('------------loop end-----------3')
                            
                        next_node = None
                     
                        break; 
                        
                     if len(node.sons)==1 and node.sons[0]==loopNode:

                        #print('------------loop end-----------3')
                            
                        next_node = None
                     
                        break; 
                        
                        
        unused_nodes = []                
                        
        for node in nodes_statements:
        
            if node not in filterNodes:
            
               if len(node.sons)>0 and node.sons[0]==loopNode:
               
                  unused_nodes.append(node)
                  
        if len(unused_nodes)==1:
        
           add_nodes=[]
           
           add_nodes.append(unused_nodes[0])
           
           filterNodes.append(unused_nodes[0])
        
           up_node = unused_nodes[0].fathers[0]
           
           while up_node is not None:
           
                if up_node not in filterNodes:
                
                   add_nodes.append(up_node)
                   
                   filterNodes.append(up_node) 
                
                   if len(up_node.fathers)==1:
                
                      up_node = up_node.fathers[0]
                   
                   else:
           
                      up_node = None
                      
                else:

                      up_node = None  
                      
           if len(add_nodes)>0:
            
               while add_nodes:
               
                    loop_body.append(add_nodes.pop())
       
        filterNodes.append(endNode)  
        #print('XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX--------------END')      
        return loop_body, filterNodes



    '''
    #
    #Get Index of Nodes
    #
    '''
    def getIfLoopNodes(self, startNode, stack_if, stack_if_end):
    
    
        index = stack_if.index(startNode)
        
        print(index)
        
        if len(stack_if_end)>=index and index>=0 and len(stack_if_end)>0:
                   
           node_if_end = stack_if_end[index]
                  
           return node_if_end
           
        return None


    '''
    #
    #Get Index of Nodes
    #
    '''
    def getLoopNodes(self, startNode, stack_loop, stack_loop_begin, stack_loop_end):
    
        index = stack_loop.index(startNode)
        
        if index>=0:
        
           node_loop_start =  stack_loop_begin[index]
           
           node_loop_end = stack_loop_end[index]
                  
           return node_loop_start, node_loop_end
           
        return None, None



    '''
    #
    #Display Node Block Statements
    #
    '''
    def displayBlockOFNodes(self, list_node_expressions):  
    
        for node in list_node_expressions:
        
            if type(node)==list and (node[0]=='IF1' or node[0]=='IF2'):
            
            
                 self.displayIfBlockOfNode(node)
            

            elif type(node)==list and (node[0]=='WHILE'):
                        
                 print('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%START-WHILE')
                 
                 print(node[2])
                 
                 print('----------------------------------------------')
                 
                 self.displayBlockOFNodes(node[3])
            
                 print('----------------------------------------------')

                 print(node[4])

                 print('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%END-WHILE')
                 
            elif type(node)==list:
            
                 self.displayBlockOFNodes(node)
           
            else:
            
                print(node)


    '''
    #
    #Display Node If Statements
    #
    '''
    def displayIfBlockOfNode(self, list_node_expressions):                
                 
        if list_node_expressions[0]=='IF2':
                 
            print('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%START')
                 
            print(list_node_expressions[1])
                    
            print('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%1_IF')
                     
            self.displayBlockOFNodes(list_node_expressions[2])
                        
            print('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%1_ELSE')
            
            if list_node_expressions[3][0]=='IF1' or list_node_expressions[3][0]=='IF2':
            
               self.displayIfBlockOfNode(list_node_expressions[3])
            
            else:
            
               self.displayBlockOFNodes(list_node_expressions[3])
                    
            print('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%IFEND')

        elif list_node_expressions[0]=='IF1':
                 
            print('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%START')
                 
            print(list_node_expressions[1])
                    
            print('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%1_IF')
                     
            self.displayBlockOFNodes(list_node_expressions[2])
                    
            print('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%IFEND')











    '''
    #
    #Display Block Statements
    #
    '''
    def displayBlock(self, list_node_expressions):  
    
        for node in list_node_expressions:
        
            if type(node)==list and (node[0]=='IF1' or node[0]=='IF2'):
            
            
                 self.displayIfBlock(node)
            

            elif type(node)==list and (node[0]=='WHILE'):
            
                 print('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%START-WHILE')
                 
                 print(node[1])
                 
                 print('----------------------------------------------')
                 
                 self.displayBlock(node[1:-1])
            
                 print('----------------------------------------------')

                 print(node[-1])

                 print('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%END-WHILE')
                 
            elif type(node)==list:
            
            
                 self.displayBlock(node)
            
            
            else:
            
                print(node)






                
    '''
    #
    #Display If Statements
    #
    '''
    def displayIfBlock(self, list_node_expressions):                
                 
        if list_node_expressions[0]=='IF2':
                 
            print('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%START')
                 
            print(list_node_expressions[1])
                    
            print('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%1_IF')
                     
            for elmt in list_node_expressions[2]:
                 
                print(elmt)
                        
            print('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%1_ELSE')
            
            if list_node_expressions[3][0]=='IF2':
            
            
               self.displayIfBlock(list_node_expressions[3])
            
            
            else: 
                        
                 for elmt in list_node_expressions[3]:
                 
                     print(elmt)
                        
                 print(list_node_expressions[4])
                    
            print('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%IFEND')

        elif list_node_expressions[0]=='IF1':
                 
            print('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%START')
                 
            print(list_node_expressions[1])
                    
            print('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%1_IF')
                     
            for elmt in list_node_expressions[2]:
                 
                 print(elmt)
                        
            print(list_node_expressions[3])
                    
            print('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%IFEND')
                 
 
 
                    

               
    '''
    #
    #Get all nodes between two nodes
    #
    '''
    def getAllNodesBetween(self, start_node, end_node, nodes_statements, stack_expression):
    
        start_flag=False
        
        list_of_nodes = []
    
        for node in nodes_statements:
        
            if node==start_node:
            
               start_flag=True
               
            if node==end_node:
            
               start_flag=False
               
               
            if start_flag==True:
            
               if node in stack_expression:
            
                  stack_expression.remove(node)
            
               list_of_nodes.append(node)
            
        return list_of_nodes
               
               
    '''
    #
    #Get all nodes between two nodes
    #
    '''
    def getRidOfCommonOne(self, list_main, list_sec):
        
        for node in list_sec:
        
            if node.type not in [NodeType.ENDIF, NodeType.IF] and node in list_main:
        
               list_main.remove(node)
               
        if len(list_sec)==2:
        
           if_else_expressions=['IF1',list_main[0],list_main[1:-1],list_main[-1]] 
        
        else:
        
           if_else_expressions=['IF2',list_main[0],list_main[1:-1],list_sec[1:-1],list_main[-1]]    
              
        return if_else_expressions
        
            
    '''
    #
    #Get all nodes between two nodes
    #
    '''
    def getRidOfCommonBlock(self, list_loop, stack_expression):
    
        key_list_expression={}
        
        update_list_expression=[]
        
        for exp in stack_expression:
        
            if type(exp)==list:
        
               key_list_expression[exp[1]]=exp
        
        check_list = None
        
        for node in list_loop:
        
           if node in key_list_expression:
           
              stack_expression.remove(key_list_expression[node])
           
              update_list_expression.append(key_list_expression[node])
             
              check_list = self.getIfBlock(key_list_expression[node])
        
           elif check_list is not None and node not in check_list: 
           
             update_list_expression.append(node)
             
           elif check_list is None:
           
             update_list_expression.append(node)
             
             
        return update_list_expression,stack_expression
        
            
        
             
   
    '''
    #
    #Constructiong IMC using context flow graph for If Case
    #
    '''
        
        
        
    def handleIfLoopCFG(self, if_loop_head, statements):
    
    
        body_nodes = if_loop_head[2]
        
        flag = False
        
                       
        while(body_nodes[0]!=None):
        
                
            if body_nodes[0] in statements or  body_nodes[0] in self.map_delete_statements:
            
            
               if body_nodes[0] in statements:
               
                   inner_if_loop_head = statements[body_nodes[0]]
                   
                   self.map_delete_statements[body_nodes[0]] = statements[body_nodes[0]]
                                      
                   del statements[body_nodes[0]]
                  
               elif body_nodes[0] in self.map_delete_statements:
               
                   inner_if_loop_head = self.map_delete_statements[body_nodes[0]]
                                 
              
               
               if inner_if_loop_head[0]=='EX':
        
                  if_loop_head[-3].append(inner_if_loop_head)
                  
                  if len(inner_if_loop_head[2])>0:
                  
                     body_nodes[0]=inner_if_loop_head[2][0]
                                       
                  else:
                  
                     body_nodes[0]=None
                               
                 
               elif inner_if_loop_head[0]=='IF':
               
                    update_inner_if_loop_head = self.handleIfLoopCFG(inner_if_loop_head, statements)
                    
                    if_loop_head[-3].append(update_inner_if_loop_head)
                    
                    body_nodes[0]=None
               
               
               elif inner_if_loop_head[0]=='LOOP':
                                 
                  update_inner_if_loop_head = self.handleLoopCFG(inner_if_loop_head, statements)
                  
                  
                  if_loop_head[2][0]=inner_if_loop_head[2][-1]
                  
                  if_loop_head[-3].append(update_inner_if_loop_head)
                  
                          
        while(body_nodes[1]!=None):
                
        
            if body_nodes[1] in statements or body_nodes[1] in self.map_delete_statements :
                           
               if body_nodes[1] in statements:
               
                   inner_if_loop_head = statements[body_nodes[1]]
                  
                   self.map_delete_statements[body_nodes[1]] = statements[body_nodes[1]]
                                   
                   
                   del statements[body_nodes[1]]
                  
               elif body_nodes[1] in self.map_delete_statements:
               
               
                   inner_if_loop_head = self.map_delete_statements[body_nodes[1]]
                   
                   if inner_if_loop_head[0]=='LOOP':
                   
                      flag = True
                   
                      inner_if_loop_head = statements[inner_if_loop_head[2][1]]
              
              
               
               if inner_if_loop_head[0]=='EX':
        
                  if flag==False:
                  
                     if_loop_head[-2].append(inner_if_loop_head)
                  
                  if len(inner_if_loop_head[2])>0:
                  
                     body_nodes[1]=inner_if_loop_head[2][0]
                  
                  else:
                  
                     if len(inner_if_loop_head[2])==0:
                     
                        statements[body_nodes[1]]=inner_if_loop_head
                     
                  
                     body_nodes[1]=None
           
                 
               elif inner_if_loop_head[0]=='IF':
               
                    update_inner_if_loop_head = self.handleIfLoopCFG(inner_if_loop_head, statements)
                    
                    if_loop_head[-2].append(update_inner_if_loop_head)
                    
                    body_nodes[1]=None
               
               
               elif inner_if_loop_head[0]=='LOOP':
                                 
                  update_inner_if_loop_head = self.handleLoopCFG(inner_if_loop_head, statements)
                  
                  
                  if_loop_head[2][0]=inner_if_loop_head[2][-1]
                  
                  if_loop_head[-2].append(update_inner_if_loop_head)

   
        return if_loop_head
        
    '''
    #
    #Constructiong IC using IMC
    #
    '''
    def contructNodes(self, statements, Var_Facts):
    
        update_statements=[]   
        
                        
        
        for statement in statements:
        
            if type(statement)==list and statement[0]=='IF1':
            
                #print('@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@')
                
                condition_statements=[]
                
                #print(statement)
                #print(statement.context)
                
                for ir in statement[1].irs:
                
                    #ir.context[self.KEY] = statement[1].context[self.KEY]
                    #print(type(ir))                
                    #print(ir)
                    #print(ir.context)
                                
                    condition_statements.append(self.processEachInstruction(ir, Var_Facts))
                    
                for stmt in condition_statements[:-1]:
                
                    update_statements.append(stmt)
     
                if_statements = self.contructNodes(statement[2], Var_Facts)
                
                update_statements.append(['-1','if1',condition_statements[-1][1:],if_statements])
                
                #print('@@@@@@@@@@@@@@@@@@@@@@@@@@@@@')

                 
                 
            elif type(statement)==list and statement[0]=='IF2':
            
                #print('@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@')
                
                condition_statements=[]
                
                #print(statement)
                #print(statement.context)
                #print('----------------------------')
                
                for ir in statement[1].irs:
                
                    #ir.context[self.KEY] = statement[1].context[self.KEY]
                    #print(type(ir))
                    #print(ir)
                    #print(ir.context)        
                    condition_statements.append(self.processEachInstruction(ir, Var_Facts))
                    
                    
                for stmt in condition_statements[:-1]:
                
                    update_statements.append(stmt)
                
                
                if_statements = self.contructNodes(statement[2], Var_Facts)
                
                
                if statement[3][0]=='IF1' or statement[3][0]=='IF2':
                
                   else_statements = self.contructNodes([statement[3]], Var_Facts)
                   
                   
                else:
                
                   else_statements = self.contructNodes(statement[3], Var_Facts)                
                
                
                if else_statements is None:
                
                   update_statements.append(['-1','if1',condition_statements[-1][1:],if_statements])

                elif if_statements is None:
                
                   condition_statements[-1][1:][0]='=='
                
                   update_statements.append(['-1','if1',condition_statements[-1][1:],else_statements])

                
                else:
                
                   update_statements.append(['-1','if2',condition_statements[-1][1:],if_statements,else_statements])
                
                #print('@@@@@@@@@@@@@@@@@@@@@@@@@@@@@')
            

            elif type(statement)==list and (statement[0]=='WHILE'):
            
            
                             
                #print('$$$$$$$$$$$$$$$$$$$$$$$$$$$$')
                
                condition_statements=[]
            
                #print(statement)
                #print(statement.context)
                
                for ir in statement[2].irs:
                
                    #ir.context[self.KEY] = statement[2].context[self.KEY]
                    #print(type(ir))                
                    #print(ir)
                    #print(ir.context)
                                                
                    condition_statements.append(self.processEachInstruction(ir, Var_Facts))
                    
                for stmt in condition_statements[:-1]:
                
                    update_statements.append(stmt)
                    
                
                block_statements = self.contructNodes(statement[3], Var_Facts)
                
               
                
                update_statements.append(['-1','while',condition_statements[-1][1:],block_statements])
                
                #print('$$$$$$$$$$$$$$$$$$$$$$$$$$$$')


                 
            elif type(statement)==list:
            
            
                 stmts = self.contructNodes(statement, Var_Facts)
            
            
            else:
            
                #print(statement)
                #print(statement.context)                
                for ir in statement.irs:
                    #if self.KEY in ir.context.keys():
                    #   ir.context[self.KEY] = statement.context[self.KEY]
                    #print(type(ir))               
                    #print(ir)
                    #print(ir.context)
                    #print(type(ir))
                    update_statements.append(self.processEachInstruction(ir, Var_Facts))
                    


        update_statements = self.IRFilter(update_statements, Var_Facts) 
         
        update_statements = self.IRTranslation(update_statements, Var_Facts)       
               
        return update_statements            
            




             
        
        
    '''
    #
    #Constructiong FOL formula
    #
    '''
    def processEachInstruction(self, statement, Var_Facts):
    
       #print('~~~~~~~~~~~~~~~~~~~~~~~~~~')
       #print(type(statement))
       #print('~~~~~~~~~~~~~~~~~~~~~~~~~~')    
        
       if type(statement)==slither.slithir.operations.binary.Binary:
                          
          statement_exp = self.binaryExpression(statement,Var_Facts)
                             
          if statement_exp is not None:
                        
              return statement_exp
                              
          else:
                             
             print('Unable to generate binary')
    
    
       elif type(statement)==slither.slithir.operations.return_operation.Return:
                        

            statement_exp = self.returnExpression(statement,Var_Facts)
                                         
            if statement_exp is not None:
                        
                return statement_exp
                              
            else:
                             
               print('Unable to generate return')
    
    
       elif type(statement)==slither.slithir.operations.assignment.Assignment:
                        
                             
            statement_exp = self.assigmentExpression(statement,Var_Facts)
            
                             
            if statement_exp is not None:
                        
               return statement_exp
                              
            else:
                             
               print('Unable to generate assignment')
               

       elif type(statement)==slither.slithir.operations.index.Index:
                        
                             
            statement_exp = self.arrayIndexExpression(statement,Var_Facts)
                             
            if statement_exp is not None:
                        
               return statement_exp
                              
            else:
                             
               print('Unable to generate Index')


       elif type(statement)==slither.slithir.operations.length.Length:
                        
                             
            statement_exp = self.arrayLengthExpression(statement,Var_Facts)
                             
            if statement_exp is not None:
                        
               return statement_exp
                              
            else:
                             
               print('Unable to generate length')
       
       elif type(statement)==slither.slithir.operations.init_array.InitArray:
                        
                             
            statement_exp = self.arrayInitaization(statement,Var_Facts)
                             
            if statement_exp is not None:
                        
               return statement_exp
                              
            else:
                             
               print('Unable to generate initArray')

       elif type(statement)==slither.slithir.operations.delete.Delete:
                        
                             
            statement_exp = self.arrayDelete(statement,Var_Facts)
                             
            if statement_exp is not None:
                        
               return statement_exp
                              
            else:
                             
               print('Unable to generate Delete')


       elif type(statement)==slither.slithir.operations.delete.Delete:
                        
                             
            statement_exp = self.arrayDelete(statement,Var_Facts)
                             
            if statement_exp is not None:
                        
               return statement_exp
                              
            else:
                             
               print('Unable to generate Delete')
               

       elif type(statement)==slither.slithir.operations.member.Member:
                        
                             
            statement_exp = self.memberOperationHandling(statement,Var_Facts)
                             
            if statement_exp is not None:
                        
               return statement_exp
                              
            else:
                             
               print('Unable to generate Member')
               

       elif type(statement)==slither.slithir.operations.new_contract.NewContract:
                        
                             
            statement_exp = self.newExpression(statement,Var_Facts)
                             
            if statement_exp is not None:
                        
               return statement_exp
                              
            else:
                             
               print('Unable to generate New')
               
       elif type(statement)==slither.slithir.operations.new_structure.NewStructure:
       
            #print(statement.__dict__)
                        
                             
            statement_exp = self.newExpressionStruct(statement,Var_Facts)
                             
            if statement_exp is not None:
                        
               return statement_exp
                              
            else:
                             
               print('Unable to generate New')

               

       elif type(statement)==slither.slithir.operations.internal_call.InternalCall:
                        
                             
            statement_exp = self.internalfunctionExpression(statement,Var_Facts)
                                         
            if statement_exp is not None:
                        
               return statement_exp
                              
            else:
                             
               print('Unable to generate InternalCall')
               

       elif type(statement)==slither.slithir.operations.high_level_call.HighLevelCall:
                        
                             
            statement_exp = self.highlevelfunctionExpression(statement,Var_Facts)
            
                                          
            if statement_exp is not None:
                        
               return statement_exp
                              
            else:
                             
               print('Unable to generate HighLevelCall')
               
       elif type(statement)==slither.slithir.operations.library_call.LibraryCall:
       
       
            statement_exp = self.libraryfunctionExpression(statement,Var_Facts)
                             
            if statement_exp is not None:
                        
               return statement_exp
                              
            else:
                             
               print('Unable to generate LibraryCall')
               
       elif type(statement)==slither.slithir.operations.solidity_call.SolidityCall:
       
       
            statement_exp = self.solidityCallExpression(statement,Var_Facts)
                             
            if statement_exp is not None:
                        
               return statement_exp
                              
            else:
                             
               print('Unable to generate LibraryCall')
               

       elif type(statement)==slither.slithir.operations.unpack.Unpack:
                        
                             
            statement_exp = self.tupleUnpackExpression(statement,Var_Facts)
                             
            if statement_exp is not None:
                        
               return statement_exp
                              
            else:
                             
               print('Unable to generate Unpack')
               
       elif type(statement)==slither.slithir.operations.condition.Condition:
       
      
            statement_exp = self.conditionExpression(statement,Var_Facts)
                             
            if statement_exp is not None:
                        
               return statement_exp
                              
            else:
                             
               print('Unable to generate Unpack')
               
               
       elif type(statement)==slither.slithir.operations.unary.Unary:
       
       
            statement_exp = self.unaryExpression(statement,Var_Facts)
                             
            if statement_exp is not None:
                        
               return statement_exp
                              
            else:
                             
               print('Unable to generate Unary Expression')
               
               
       elif type(statement)==slither.slithir.operations.type_conversion.TypeConversion:
    
            statement_exp = self.convertExpression(statement,Var_Facts)
                             
            if statement_exp is not None:
                        
               return statement_exp
                              
            else:
                             
               print('Unable to generate conversion')
               

       elif type(statement)==slither.slithir.operations.event_call.EventCall:       
       
            statement_exp = self.convertEmit(statement,Var_Facts)
                             
            if statement_exp is not None:
                        
               return statement_exp
                              
            else:
                             
               print('Unable to generate Emit')
                               
       elif type(statement)==slither.slithir.operations.low_level_call.LowLevelCall:  
 
            statement_exp = self.lowLevelCallFunction(statement,Var_Facts)
            
            self.user_assert_fun_call_list.append(statement_exp[-1][1:3])
            

            
            if statement_exp is not None:
                        
               return statement_exp
                              
            else:
                             
               print('Unable to generate LOW LEVEL CALL')       
       
       
       elif type(statement)==slither.slithir.operations.balance.Balance:
       
            statement_exp = self.constructBalance(statement,Var_Facts)
                             
            if statement_exp is not None:
                        
               return statement_exp
                              
            else:
                             
               print('Unable to generate Balance')
               
       elif type(statement)==slither.slithir.operations.send.Send: 
       
            statement_exp = self.constructSend(statement,Var_Facts)
            
            self.user_assert_fun_call_list.append(statement_exp[-1][1:3])

                             
            if statement_exp is not None:
                        
               return statement_exp
                              
            else:
                             
               print('Unable to generate Seend')

       elif type(statement)==slither.slithir.operations.transfer.Transfer: 
       
            statement_exp = self.constructTransfer(statement,Var_Facts)
            
            self.user_assert_fun_call_list.append(statement_exp[-1][1:3])

                             
            if statement_exp is not None:
                        
               return statement_exp
                              
            else:
                             
               print('Unable to generate Seend')
               
               
       elif type(statement)==slither.slithir.operations.codesize.CodeSize:
       
            statement_exp = self.constructCode(statement,Var_Facts)
                             
            if statement_exp is not None:
                        
               return statement_exp
                              
            else:
                             
               print('Unable to generate Code Size')

       elif type(statement)==slither.slithir.operations.new_elementary_type.NewElementaryType:
       
            statement_exp = self.constructElementNew(statement,Var_Facts)

            if statement_exp is not None:
                        
               return statement_exp
                              
            else:
                             
               print('Unable to generate New Elementary Type')
       
       else:
       
          print('------------------------------------------')
          
          print(type(statement))
          
          print(statement)
          
          print('------------------------------------------')
            
    


    def output(self, filename):
        """
        _filename is not used
        Args:
            _filename(string)
        """
        
        info = self.slithir_cfg_to_IRFOL()


        res = self.generate_output(info)

            
        return res
        
    '''
    #Handling Elementory New Creator
    '''
    def constructElementNew(self, ir_expression, Var_Facts):
    
    
        lvalue = ir_expression.lvalue
        
        parameters = []
        
        l_value = self.variableHandling(lvalue,Var_Facts)
        
        type_new = ir_expression.type.type
                
        for arg in ir_expression.arguments:
        
            update_arg = self.variableHandling(arg,Var_Facts)
        
            if 'TMP' in update_arg[0] and update_arg[0] in self.map_TMP:
           
               update_arg = self.map_TMP[update_arg[0]]

            if 'REF' in update_arg[0] and update_arg[0] in self.map_REF:
           
               update_arg = self.map_REF[update_arg[0]]
               
            parameters.append(update_arg)

        
        if l_value is not None and type_new is not None and len(parameters)>0:

            if 'TMP' in l_value[0] and l_value[0] not in self.map_NEW_ELEMENT:
           
               self.map_NEW_ELEMENT[l_value[0]] =  ['-1','new_element',l_value,[type_new],parameters]
            
            return ['-1','new_element',l_value,[type_new],parameters]
            
        else:
         
            print('IR construction Balance Error!')
    
            return None 

        
        
    '''
    #Handling Code Size 
    '''
        
    def constructCode(self, ir_expression, Var_Facts):
    
        lvalue = ir_expression.lvalue
        
        l_value = self.variableHandling(lvalue,Var_Facts)
        
        leftvalue = ir_expression.value
        
        left_value = self.variableHandling(leftvalue,Var_Facts)

        if l_value is not None and left_value is not None:
     
            if 'CODESIZE' not in Var_Facts:
               
               list_fact=[]
                    
               list_fact.append('_y'+str(len(Var_Facts)+1))

               list_fact.append(Var_Facts[left_value[0]][1])  
               
               list_fact.append(Var_Facts[l_value[0]][1]) 
                                                         
               Var_Facts['CODESIZE']=list_fact
               
               
            if 'TMP' in left_value[0] and left_value[0] in self.map_TMP:
           
               left_value = self.map_TMP[left_value[0]]

            if 'REF' in left_value[0] and left_value[0] in self.map_REF:
           
               left_value = self.map_REF[left_value[0]]
               
            if 'TMP' in l_value[0] and l_value[0] not in self.map_TMP:
           
               self.map_TMP[l_value[0]]=['CODESIZE', left_value]

            if 'REF' in l_value[0] and l_value[0] not in self.map_REF:
           
               self.map_REF[l_value[0]]=['CODESIZE', left_value]

               
            balance=['-1','=',l_value,['CODESIZE', left_value]]
            
            return balance
            
        else:
         
            print('IR construction Balance Error!')
    
            return None 
        
        


    '''
    #Handling Send 
    '''
        
    def constructSend(self, ir_expression, Var_Facts):
    
        arguments = []

        lvalue = ir_expression.lvalue
    
        l_term = self.variableHandling(lvalue,Var_Facts)
        
        FUNCTION_NAME = 'LOW_LEVEL_CALL_send'
                                                       
        #print('@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@')
        
        #print(self.map_TMP)
        
        #print(ir_expression.__dict__)
        
        #print('----------------------------------------')
        
        #print(FUNCTION_NAME)
        
        #print('----------------------------------------')
        
        #print(ir_expression.call_value)
        
        #print('@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@')
        
        arguments.append(FUNCTION_NAME)
        
        list_fact=[]
        
        if ir_expression.destination is not None:
        
           r_dest = self.variableHandling(ir_expression.destination,Var_Facts)
           
           list_fact.append(Var_Facts[r_dest[0]][1])
           
           if 'TMP' in r_dest[0] and r_dest[0] in self.map_TMP:
           
               r_dest = self.map_TMP[r_dest[0]]

           if 'REF' in r_dest[0] and r_dest[0] in self.map_REF:
           
               r_dest = self.map_REF[r_dest[0]]
        
           arguments.append(r_dest)
           
        if ir_expression.call_value is not None:
        
           #print(type(ir_expression.call_value))
        
           r_value = self.variableHandling(ir_expression.call_value,Var_Facts)
           
           #print(r_value)
           
           #print(Var_Facts)
           
           list_fact.append(Var_Facts[r_value[0]][1])
           
           if 'TMP' in r_value[0] and r_value[0] in self.map_TMP:
           
               r_value = self.map_TMP[r_value[0]]

           if 'REF' in r_value[0] and r_value[0] in self.map_REF:
           
               r_value = self.map_REF[r_value[0]]

        
           arguments.append(r_value)
        
                             
        if len(ir_expression.arguments)>0:
                                                      
           for argument in ir_expression.arguments:
        
               arg = self.variableHandling(argument,Var_Facts)
               
               if arg[0] != 'String':
               
                  if arg[0] in Var_Facts:
               
                     list_fact.append(Var_Facts[arg[0]][1])
                     
                  else:
                  
                     list_fact.append('uint256')
                  
                  
               else:
               
                  list_fact.append('string')
          
               if arg is not None:
               
                  if 'TMP' in arg[0] and arg[0] in self.map_TMP:
           
                      arg = self.map_TMP[arg[0]]

                  if 'REF' in arg[0] and arg[0] in self.map_REF:
           
                      arg = self.map_REF[arg[0]]
        
                  arguments.append(arg)
                  
               
           if l_term is not None and len(arguments)>0:
           
              if FUNCTION_NAME not in Var_Facts:
              
                 list_fact=['_y'+str(len(Var_Facts)+1)]+list_fact
              
                 list_fact.append(Var_Facts[l_term[0]][1])
              
                 Var_Facts[FUNCTION_NAME]=list_fact
                 
                 
       
              return ['-1','=',l_term,arguments]
           
           
           elif l_term is None and len(arguments)>0:
        
               if '_'+str(self.dummy_count+1)+'dummy' not in Var_Facts:
               
                  list_fact=[]
                    
                  list_fact.append('_y'+str(len(Var_Facts)+1))
           
                  list_fact.append('void')
                  
                  self.dummy_count=self.dummy_count+1
                     
                  Var_Facts['_'+str(self.dummy_count)+'dummy']=list_fact

        
               return ['-1','=',['_'+str(self.dummy_count)+'dummy'],arguments]
           
           else:
        
               print('IR construction Error!')
       
               return None 
           
        else: 
        
           #print('------------------------/////////////////////////')
           #print(l_term)
           #print('------------------------/////////////////////////')        
    
           if l_term is not None:
           
           
              if FUNCTION_NAME not in Var_Facts:
              
                 list_fact=['_y'+str(len(Var_Facts)+1)]+list_fact
              
                 list_fact.append(Var_Facts[l_term[0]][1])
              
                 Var_Facts[FUNCTION_NAME]=list_fact


           
              if len(arguments)>0:
              
              
                 if 'TMP' in l_term[0] and l_term[0] not in self.map_TMP:
           
                    self.map_TMP[l_term[0]]=arguments

                 if 'REF' in l_term[0] and l_term[0] not in self.map_REF:
           
                    self.map_REF[l_term[0]]=arguments
              
                 return ['-1','=',l_term,arguments]
                 
              else:
              
 
                 if 'TMP' in l_term[0] and l_term[0] not in self.map_TMP:
           
                    self.map_TMP[l_term[0]]=[FUNCTION_NAME]

                 if 'REF' in l_term[0] and l_term[0] not in self.map_REF:
           
                    self.map_REF[l_term[0]]=[FUNCTION_NAME]
              
                 return ['-1','=',l_term,[FUNCTION_NAME]]
           
           elif l_term is None:
        
              if '_'+str(self.dummy_count+1)+'dummy' not in Var_Facts:
               
                 list_fact=[]
                    
                 list_fact.append('_y'+str(len(Var_Facts)+1))
           
                 list_fact.append('void')
                 
                 self.dummy_count=self.dummy_count+1
                     
                 Var_Facts['_'+str(self.dummy_count)+'dummy']=list_fact
        
              return ['-1','=',['_'+str(self.dummy_count)+'dummy'],[FUNCTION_NAME]]
           
           else:
        
              print('IR construction Error!')
       
              return None       






    '''
    #Handling Transfer 
    '''
        
    def constructTransfer(self, ir_expression, Var_Facts):
    
        arguments = []

        lvalue = None
    
        l_term = self.variableHandling(lvalue,Var_Facts)
        
        FUNCTION_NAME = 'LOW_LEVEL_CALL_Transfer'
                                                       
        #print('@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@')
        
        #print(self.map_TMP)
        
        #print(ir_expression.__dict__)
        
        #print('----------------------------------------')
        
        #print(FUNCTION_NAME)
        
        #print('----------------------------------------')
        
        #print(ir_expression.call_value)
        
        #print('@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@')
        
        arguments.append(FUNCTION_NAME)
        
        list_fact=[]
        
        if ir_expression.destination is not None:
        
           r_dest = self.variableHandling(ir_expression.destination,Var_Facts)
           
           list_fact.append(Var_Facts[r_dest[0]][1])
           
           if 'TMP' in r_dest[0] and r_dest[0] in self.map_TMP:
           
               r_dest = self.map_TMP[r_dest[0]]

           if 'REF' in r_dest[0] and r_dest[0] in self.map_REF:
           
               r_dest = self.map_REF[r_dest[0]]
        
           arguments.append(r_dest)
           
        if ir_expression.call_value is not None:
        
           #print(type(ir_expression.call_value))
        
           r_value = self.variableHandling(ir_expression.call_value,Var_Facts)
           
           #print(r_value)
           
           #print(Var_Facts)
           
           list_fact.append(Var_Facts[r_value[0]][1])
           
           if 'TMP' in r_value[0] and r_value[0] in self.map_TMP:
           
               r_value = self.map_TMP[r_value[0]]

           if 'REF' in r_value[0] and r_value[0] in self.map_REF:
           
               r_value = self.map_REF[r_value[0]]

        
           arguments.append(r_value)
        
                             
        if len(ir_expression.arguments)>0:
                                                      
           for argument in ir_expression.arguments:
        
               arg = self.variableHandling(argument,Var_Facts)
               
               if arg[0] != 'String':
               
                  if arg[0] in Var_Facts:
               
                     list_fact.append(Var_Facts[arg[0]][1])
                     
                  else:
                  
                     list_fact.append('uint256')
                  
                  
               else:
               
                  list_fact.append('string')
          
               if arg is not None:
               
                  if 'TMP' in arg[0] and arg[0] in self.map_TMP:
           
                      arg = self.map_TMP[arg[0]]

                  if 'REF' in arg[0] and arg[0] in self.map_REF:
           
                      arg = self.map_REF[arg[0]]
        
                  arguments.append(arg)
                  
               
           if l_term is not None and len(arguments)>0:
           
              if FUNCTION_NAME not in Var_Facts:
              
                 list_fact=['_y'+str(len(Var_Facts)+1)]+list_fact
              
                 list_fact.append(Var_Facts[l_term[0]][1])
              
                 Var_Facts[FUNCTION_NAME]=list_fact
                 
                 
       
              return ['-1','=',l_term,arguments]
           
           
           elif l_term is None and len(arguments)>0:
        
               if '_'+str(self.dummy_count+1)+'dummy' not in Var_Facts:
               
                  list_fact=[]
                    
                  list_fact.append('_y'+str(len(Var_Facts)+1))
           
                  list_fact.append('void')
                  
                  self.dummy_count=self.dummy_count+1
                     
                  Var_Facts['_'+str(self.dummy_count)+'dummy']=list_fact

        
               return ['-1','=',['_'+str(self.dummy_count)+'dummy'],arguments]
           
           else:
        
               print('IR construction Error!')
       
               return None 
           
        else: 
        
           #print('------------------------/////////////////////////')
           #print(l_term)
           #print('------------------------/////////////////////////')        
    
           if l_term is not None:
           
           
              if FUNCTION_NAME not in Var_Facts:
              
                 list_fact=['_y'+str(len(Var_Facts)+1)]+list_fact
              
                 list_fact.append(Var_Facts[l_term[0]][1])
              
                 Var_Facts[FUNCTION_NAME]=list_fact


           
              if len(arguments)>0:
              
              
                 if 'TMP' in l_term[0] and l_term[0] not in self.map_TMP:
           
                    self.map_TMP[l_term[0]]=arguments

                 if 'REF' in l_term[0] and l_term[0] not in self.map_REF:
           
                    self.map_REF[l_term[0]]=arguments
              
                 return ['-1','=',l_term,arguments]
                 
              else:
              
 
                 if 'TMP' in l_term[0] and l_term[0] not in self.map_TMP:
           
                    self.map_TMP[l_term[0]]=[FUNCTION_NAME]

                 if 'REF' in l_term[0] and l_term[0] not in self.map_REF:
           
                    self.map_REF[l_term[0]]=[FUNCTION_NAME]
              
                 return ['-1','=',l_term,[FUNCTION_NAME]]
           
           elif l_term is None:
        
              if '_'+str(self.dummy_count+1)+'dummy' not in Var_Facts:
               
                 list_fact=[]
                    
                 list_fact.append('_y'+str(len(Var_Facts)+1))
           
                 list_fact.append('void')
                 
                 self.dummy_count=self.dummy_count+1
                     
                 Var_Facts['_'+str(self.dummy_count)+'dummy']=list_fact
                 
                 
              if len(arguments)>0:
              
                            
                 return ['-1','=',['_'+str(self.dummy_count)+'dummy'],arguments]
                 
              else:
              
               
                 return ['-1','=',['_'+str(self.dummy_count)+'dummy'],[FUNCTION_NAME]]
        

           
           else:
        
              print('IR construction Error!')
       
              return None       














        
    '''
    #Handling Balance 
    '''
        
    def constructBalance(self, ir_expression, Var_Facts):
    
        lvalue = ir_expression.lvalue
        
        l_value = self.variableHandling(lvalue,Var_Facts)
        
        leftvalue = ir_expression.value
        
        left_value = self.variableHandling(leftvalue,Var_Facts)

        if l_value is not None and left_value is not None:
     
            if 'BALANCE' not in Var_Facts:
               
               list_fact=[]
                    
               list_fact.append('_y'+str(len(Var_Facts)+1))

               list_fact.append(Var_Facts[left_value[0]][1])  
               
               list_fact.append(Var_Facts[l_value[0]][1]) 
                                                         
               Var_Facts['BALANCE']=list_fact
               
               
            if 'TMP' in left_value[0] and left_value[0] in self.map_TMP:
           
               left_value = self.map_TMP[left_value[0]]

            if 'REF' in left_value[0] and left_value[0] in self.map_REF:
           
               left_value = self.map_REF[left_value[0]]

               
            if 'TMP' in l_value[0] and l_value[0] not in self.map_TMP:
           
               self.map_TMP[l_value[0]]=['BALANCE', left_value]

            if 'REF' in l_value[0] and l_value[0] not in self.map_REF:
           
               self.map_REF[l_value[0]]=['BALANCE', left_value]

               
            balance=['-1','=',l_value,['BALANCE', left_value]]
            
            return balance
            
        else:
         
            print('IR construction Balance Error!')
    
            return None 
        
        
    '''
    #Handling Emit log 
    '''
    def convertEmit(self, ir_expression,Var_Facts):
               
        arguments=[]
    
        if len(ir_expression.arguments)>0:
        
           for argument in ir_expression.arguments:
        
               arg = self.variableHandling(argument,Var_Facts)
               
               if 'TMP' in arg[0] and arg[0] in self.map_TMP:
           
                   arg = self.map_TMP[arg[0]]

               if 'REF' in arg[0] and arg[0] in self.map_REF:
           
                   arg = self.map_REF[arg[0]]
          
               if arg is not None:
        
                  arguments.append(arg)
                  
                  
        if '_'+str(self.emit_count+1)+'emit' not in Var_Facts:
               
               list_fact=[]
                    
               list_fact.append('_y'+str(len(Var_Facts)+1))

               
               list_fact.append('Void')  
               
               self.emit_count = self.emit_count+1  
                                          
               Var_Facts['_'+str(self.emit_count)+'emit']=list_fact
               
        if len(arguments)>0:
        
           emitExper=['-1','=',['_'+str(self.emit_count)+'emit'],[ir_expression.name]+arguments]
           
        else:
        
           emitExper=['-1','=',['_'+str(self.emit_count)+'emit'],[ir_expression.name]]
         
        return emitExper
              
        
        #else:
    
        #   print('IR construction Conversion Error!')



    '''
    #Handling Convert Expression
    '''
    def convertExpression(self, ir_expression,Var_Facts):
    
        lvalue = ir_expression.lvalue
        
        l_value = self.variableHandling(lvalue,Var_Facts)

        if l_value is not None and ir_expression.variable is not None:
        
           if folTras.is_number(ir_expression.variable)==True:
           
              value_convert=str(ir_expression.variable)
              
           else:
           
              if format(ir_expression.variable)=='this':
              
                 if 'this' not in Var_Facts:
                 
                    list_fact=[]
                    
                    list_fact.append('_y'+str(len(Var_Facts)+1))
                    
                    list_fact.append('CONRT'+'_'+self.current_contract.name)
                    
                    Var_Facts['this']=list_fact
                     
                 value_convert=[format(ir_expression.variable)]
                    
              else:
              
                 if folTras.is_number(format(ir_expression.variable))==True:
                 
                    value_convert=[format(ir_expression.variable)]
                 
                 else:
                 
                 
                    r_value = self.variableHandling(ir_expression.variable,Var_Facts)
                    
                    
                    if 'TMP' in r_value[0] and r_value[0] in self.map_TMP:
                    
                        r_value = self.map_TMP[r_value[0]]
                    
                    if 'REF' in r_value[0] and r_value[0] in self.map_REF:

                        r_value = self.map_REF[r_value[0]]                    
                    
                    
                    value_convert = r_value
                           
                    #value_convert=['String',[format(ir_expression.variable)]]
        
           convertExpr=['-1','=',l_value,['convertTo'+format(ir_expression.type),value_convert]]
           
           if 'TMP' in l_value[0] and l_value[0] not in self.map_TMP:
           
               self.map_TMP[l_value[0]]=['convertTo'+format(ir_expression.type),value_convert]

           if 'REF' in l_value[0] and l_value[0] not in self.map_REF:
           
               self.map_REF[l_value[0]]=['convertTo'+format(ir_expression.type),value_convert]

           
           
           if 'convertTo'+format(ir_expression.type) not in Var_Facts:
               
               list_fact=[]
                    
               list_fact.append('_y'+str(len(Var_Facts)+1))

               
               if folTras.is_number(ir_expression.variable)==True:
                       
                       list_fact.append('uint256')
                     
               else:
                       list_fact.append('address')
                       #list_fact.append('string')    
                                          
               list_fact.append(format(ir_expression.type))
                                             
               Var_Facts['convertTo'+format(ir_expression.type)]=list_fact


           return convertExpr 
              
        
        else:
    
           print('IR construction Conversion Error!')
    
    
    
    


    '''
    #Handling unary Expression
    '''
    def unaryExpression(self, ir_expression,Var_Facts):
    
            
        lvalue = ir_expression.lvalue
        
        l_value = self.variableHandling(lvalue,Var_Facts)
        
        r_value = self.variableHandling(ir_expression._variable,Var_Facts)
        
        if r_value[0] in self.map_REF:
        
           r_value=self.map_REF[r_value[0]]

        if r_value[0] in self.map_TMP:
        
           r_value=self.map_TMP[r_value[0]]

             
        
        if l_value is not None :
        
           if format(ir_expression.type)=='!':
    
              unaryExpr=['-1','=',l_value,['!=',r_value,['True']]]
                           
              #print("\t\t\t{}".format(binaryExpr))
              
              
              
              if l_value[0] not in self.map_REF and 'REF' in l_value[0]:
        
                 self.map_REF[l_value[0]]=['!=',r_value,['True']]

              if l_value[0] not in self.map_TMP and 'TMP' in l_value[0]:
        
                 self.map_TMP[l_value[0]]=['!=',r_value,['True']]
       
              return unaryExpr 
              
           else:
           
              print('Wrong unary opertor')
              
              return None
        
        else:
    
           print('IR construction Unary Error!')
       
           return None





    '''
    #Handling Binary Expression
    '''
    def conditionExpression(self, ir_expression,Var_Facts):
            
        lvalue = ir_expression.value
        
        l_value = self.variableHandling(lvalue,Var_Facts)
        
        if l_value[0] in self.map_REF:
        
           l_value=self.map_REF[l_value[0]]
        
        if l_value[0] in self.map_TMP:
 
            l_value=self.map_TMP[l_value[0]]       
                
        
        if l_value is not None :
    
           condExpr=['-1','==',l_value,['True']]
                           
           #print("\t\t\t{}".format(binaryExpr))
       
           return condExpr 
        
        else:
    
           print('IR construction Condition Error!')
       
           return None

       
        
        
    '''
    #Handling Binary Expression
    '''
    def binaryExpression(self, ir_expression,Var_Facts):
    

        left_term = None
                           
        right_term = None
                           
        lvalue = ir_expression.lvalue
                           
        for var in ir_expression._variables:
                           
           if left_term==None:
                               
              left_term=var
                                  
           elif left_term is not None and right_term==None:
                               
              right_term=var
          
        l_value = self.variableHandling(lvalue,Var_Facts) 
    
        l_term = self.variableHandling(left_term,Var_Facts)
    
        r_term = self.variableHandling(right_term,Var_Facts)


        if l_value is not None and l_term is not None and r_term is not None:
        
        
                           
           if l_term[0] in self.map_TMP:
           
              l_term = self.map_TMP[l_term[0]]
              
           if l_term[0] in self.map_REF:
           
              l_term = self.map_REF[l_term[0]]
              
           if r_term[0] in self.map_REF:
           
              r_term = self.map_REF[r_term[0]] 
        
           if r_term[0] in self.map_TMP:
           
              r_term = self.map_TMP[r_term[0]] 
              
                         
           if 'REF' in l_value[0] and l_value[0] in self.map_REF: 
           
              REF_KEY = l_value[0]
           
              l_value = self.map_REF[l_value[0]] 
                         
              self.map_REF[REF_KEY]=[format(ir_expression.type),l_term,r_term]
           
            
           binaryExpr=['-1','=',l_value,[format(ir_expression.type),l_term,r_term]]
           
           if 'TMP' in l_value[0]:
           
              self.map_TMP[l_value[0]]=[format(ir_expression.type),l_term,r_term]
                           
           #print("\t\t\t{}".format(binaryExpr))
       
           return binaryExpr
       
       
        else:
    
           print('IR construction Error!')
       
           return None



    '''
    #Handling tuple Unpack Expression
    '''

    def tupleUnpackExpression(self, ir_expression,Var_Facts):

        lvalue = ir_expression.lvalue
        
        l_term = self.variableHandling(lvalue,Var_Facts)
        
        if l_term is not None:
        
           TYPE_RET=format(ir_expression.tuple.type[ir_expression._idx].type)
    
           upack_term=['-1','=',l_term,['UNPACK'+str(len(ir_expression.tuple.type))+'DTUPLET'+str(TYPE_RET),[format(ir_expression.tuple)],folTras.expres(str(ir_expression._idx))]]
    
           if 'UNPACK'+str(len(ir_expression.tuple.type))+'DTUPLET'+str(TYPE_RET) not in Var_Facts:
               
               list_fact=[]
                    
               list_fact.append('_y'+str(len(Var_Facts)+1))
           
               list_fact.append('TUPLE')
            
               list_fact.append('uint256')
            
               list_fact.append(TYPE_RET)
                                             
               Var_Facts['UNPACK'+str(len(ir_expression.tuple.type))+'DTUPLET'+str(TYPE_RET)]=list_fact
            
           return upack_term
       
        else:
        
          print('IR construction Error!')
       
          return None 
          
          
    '''
    #Handling Low LEVEL CALL 
    '''          
          
    def lowLevelCallFunction(self, ir_expression,Var_Facts):
    
        arguments = []

        lvalue = ir_expression.lvalue
    
        l_term = self.variableHandling(lvalue,Var_Facts)
                                                       
        FUNCTION_NAME = format(ir_expression.function_name)
        
        #print('@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@')
        
        #print(ir_expression.context)
        
        #print(ir_expression.__dict__)
        
        #print('----------------------------------------')
        
        #print(FUNCTION_NAME)
        
        #print('----------------------------------------')
        
        #print(ir_expression.type_call)
        
        
        #print(ir_expression.call_value)
        
        #print(type(ir_expression.call_value))
        
        #print(ir_expression.call_value)
        
        #print('@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@')
        
        FUNCTION_NAME='LOW_LEVEL_CALL_'+FUNCTION_NAME
        
        arguments.append(FUNCTION_NAME)
        
        list_fact=[]
        
        if ir_expression.destination is not None:
        
           r_dest = self.variableHandling(ir_expression.destination,Var_Facts)
           
           if r_dest[0] in self.map_TMP:
           
              r_dest = self.map_TMP[r_dest[0]]
              
           if r_dest[0] in self.map_REF:
           
              r_dest = self.map_REF[r_dest[0]]

           
           list_fact.append(Var_Facts[r_dest[0]][-1])
           

        
           arguments.append(r_dest)
        
                             
        if ir_expression.nbr_arguments>0:
       
       
           if FUNCTION_NAME=='LOW_LEVEL_CALL_call':
                 
              call_expression = self.variableHandling(ir_expression.call_value,Var_Facts)
 
              arguments.append(call_expression)                 

                                                      
           for argument in ir_expression.arguments:
           

               arg = self.variableHandling(argument,Var_Facts)
   
               if arg[0] in self.map_TMP:
           
                  arg = self.map_TMP[arg[0]]
              
               if arg[0] in self.map_REF:
           
                  arg = self.map_REF[arg[0]]
               
               if arg[0] != 'String':
               
                  if arg[0] in Var_Facts:

                     
                     list_fact.append(Var_Facts[arg[0]][-1])
                     
                  else:
                  
                     list_fact.append('uint256')
                  
                  
               else:
               
                  if FUNCTION_NAME!='LOW_LEVEL_CALL_call':
               
                     list_fact.append('string')
                     
                  else:
                  
                     list_fact.append('uint256')
          
               if arg is not None:
               
                  if FUNCTION_NAME!='LOW_LEVEL_CALL_call':
                      
                     arguments.append(arg)
                     
                     
                  
               
           if l_term is not None and len(arguments)>0:
           
              if FUNCTION_NAME not in Var_Facts:
              
                 list_fact=['_y'+str(len(Var_Facts)+1)]+list_fact
              
                 list_fact.append(Var_Facts[l_term[0]][1])
                               
                 Var_Facts[FUNCTION_NAME]=list_fact
                 

              return ['-1','=',l_term,arguments]
           
           
           elif l_term is None and len(arguments)>0:
        
               if '_'+str(self.dummy_count+1)+'dummy' not in Var_Facts:
               
                  list_fact=[]
                    
                  list_fact.append('_y'+str(len(Var_Facts)+1))
           
                  list_fact.append('void')
                  
                  self.dummy_count=self.dummy_count+1
                     
                  Var_Facts['_'+str(self.dummy_count)+'dummy']=list_fact

        
               return ['-1','=',['_'+str(self.dummy_count)+'dummy'],arguments]
           
           else:
        
               print('IR construction Error!')
       
               return None 
           
        else: 
        
           #print('------------------------/////////////////////////')
           #print(l_term)
           #print('------------------------/////////////////////////')        
    
           if l_term is not None:
        
              return ['-1','=',l_term,[FUNCTION_NAME]]
           
           elif l_term is None:
        
              if '_'+str(self.dummy_count+1)+'dummy' not in Var_Facts:
               
                 list_fact=[]
                    
                 list_fact.append('_y'+str(len(Var_Facts)+1))
           
                 list_fact.append('void')
                 
                 self.dummy_count=self.dummy_count+1
                     
                 Var_Facts['_'+str(self.dummy_count)+'dummy']=list_fact
        
              return ['-1','=',['_'+str(self.dummy_count)+'dummy'],[FUNCTION_NAME]]
           
           else:
        
              print('IR construction Error!')
       
              return None       
 
    '''
    #Handling solidityCall
    '''         
          
    def solidityCallExpression(self, ir_expression,Var_Facts):
    
        arguments = []

        lvalue = ir_expression.lvalue
    
        l_term = self.variableHandling(lvalue,Var_Facts)
                                                               
        FUNCTION_NAME = format(ir_expression.function.name)
          
        if FUNCTION_NAME=='require(bool,string)':
        
           FUNCTION_NAME='require'
           
        elif FUNCTION_NAME=='require(bool)':
        
           FUNCTION_NAME='require'
           
        elif FUNCTION_NAME=='revert(string)':
        
           FUNCTION_NAME='revert_string'
           
        elif FUNCTION_NAME=='revert(uint256,uint256)':
        
           FUNCTION_NAME='revert_uint256'
           
        elif FUNCTION_NAME=='assert(bool)':

           FUNCTION_NAME='assert' 
           
        elif FUNCTION_NAME=='abi.encodeWithSignature()':
        
           FUNCTION_NAME='abi_encodeWithSignature'
           
        elif FUNCTION_NAME=='abi.encode()':
        
           FUNCTION_NAME='abi_encode'
           
        elif FUNCTION_NAME=='abi.decode()':

           FUNCTION_NAME='abi_decode'    
              
        elif FUNCTION_NAME=='abi.encodePacked()':
        
           FUNCTION_NAME='abi_encodePacked'
           
        elif FUNCTION_NAME=='abi.encodeWithSelector()':
        
           FUNCTION_NAME='abi_encodeWithSelector'
           
        elif FUNCTION_NAME=='abi.encodeWithSignature()':

           FUNCTION_NAME='abi_encodeWithSignature'  
           
        elif FUNCTION_NAME=='keccak256(bytes)':

           FUNCTION_NAME='keccak256'  
                     
        elif FUNCTION_NAME=='mstore(uint256,uint256)':

           FUNCTION_NAME='mstore' 
           
        elif FUNCTION_NAME=='mload(uint256)':

           FUNCTION_NAME='mload'   

        elif FUNCTION_NAME=='selfdestruct(address)':

           FUNCTION_NAME='selfdestruct'   

                    
        elif FUNCTION_NAME=='type()':

           FUNCTION_NAME='type'                 
      
      
        FUNCTION_NAME='SOLIDITY_CALL_'+FUNCTION_NAME

                
        if ir_expression.nbr_arguments>0:
                           
           for argument in ir_expression.arguments:
        
               arg = self.variableHandling(argument,Var_Facts)
                          
               if arg[0] in self.map_REF:
               
                  arg = self.map_REF[arg[0]]
               
               if arg[0] in self.map_TMP:
               
                  arg = self.map_TMP[arg[0]]               
          
               if arg is not None:
        
                  arguments.append(arg)
                  
        if FUNCTION_NAME is not None:
        
            if 'REF' in l_term[0]  and l_term[0] not in self.map_REF:
               
               self.map_REF[l_term[0]] = [FUNCTION_NAME]+arguments
               
            if 'TMP' in l_term[0]  and l_term[0] not in self.map_TMP:
               
               self.map_TMP[l_term[0]] = [FUNCTION_NAME]+arguments

            if l_term[0] in Var_Facts and Var_Facts[l_term[0]][-1]=='void':
            
               if '_'+str(self.solidity_count+1)+'solidity' not in Var_Facts:
               
                 list_fact=[]
                    
                 list_fact.append('_y'+str(len(Var_Facts)+1))
           
                 list_fact.append('void')
                 
                 self.solidity_count=self.solidity_count+1
                     
                 Var_Facts['_'+str(self.solidity_count)+'solidity']=list_fact
        
               return ['-1','=',['_'+str(self.solidity_count)+'solidity'],[FUNCTION_NAME]+arguments]
               
            else:
            
            
               if 'REF' in l_term[0] and l_term[0] not in self.map_REF:
               
                  self.map_REF[l_term[0]]=[FUNCTION_NAME]+arguments
               
               if 'TMP' in l_term[0] and l_term[0] not in self.map_TMP:
               
                  self.map_TMP[l_term[0]]=[FUNCTION_NAME]+arguments 
                  
               
               
               if FUNCTION_NAME=='SOLIDITY_CALL_type':
               
                  self.stack_infec_call.append([FUNCTION_NAME]+arguments)

                  
               return ['-1','=',l_term,[FUNCTION_NAME]+arguments]
                  
        else:
        
           print('IR construction Error!')
       
           return None 
        



    '''
    #Handling new keyword 
    '''
    def highlevelfunctionExpression(self, ir_expression,Var_Facts):

        arguments = []

        lvalue = ir_expression.lvalue
    
        l_term = self.variableHandling(lvalue,Var_Facts)
                                                       
        FUNCTION_NAME = format(ir_expression.function_name)
        
        #print('@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@')
        
        #print(l_term)
        
        #print(ir_expression.destination.name)
        
        if 'TMP' in ir_expression.destination.name and ir_expression.destination.name in self.map_TMP:
               
            dest_name = self.map_TMP[ir_expression.destination.name]
            
        else:
        
            dest_name = [ir_expression.destination.name]
            
        #print(dest_name)
        
        #print(self.Map_inheritance[format(ir_expression.destination.type)])
        
        #print(ir_expression.destination.type)
        
        #print(ir_expression.__dict__)
        
        #print('----------------------------------------')
        
        #print(FUNCTION_NAME)
        
        #print(FUNCTION_NAME)
        
        #print('@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@')
                             
        if ir_expression.nbr_arguments>0:
    
           arguments.append(FUNCTION_NAME)
                                                             
           for argument in ir_expression.arguments:
        
               arg = self.variableHandling(argument,Var_Facts)
          
               if arg is not None:
        
                  arguments.append(arg)
               
           if l_term is not None and len(arguments)>0:
        
              return ['-1','=',l_term,arguments]
           
           
           elif l_term is None and len(arguments)>0:
        
               if '_'+str(self.dummy_count+1)+'dummy' not in Var_Facts:
               
                  list_fact=[]
                    
                  list_fact.append('_y'+str(len(Var_Facts)+1))
           
                  list_fact.append('void')
                  
                  self.dummy_count=self.dummy_count+1
                     
                  Var_Facts['_'+str(self.dummy_count)+'dummy']=list_fact

        
               return ['-1','=',['_'+str(self.dummy_count)+'dummy'],arguments]
           
           else:
        
               print('IR construction Error!')
       
               return None 
           
        else: 
        
           #print('------------------------/////////////////////////')
           #print(l_term)
           #print('------------------------/////////////////////////')        
    
           if l_term is not None:
        
              return ['-1','=',l_term,[FUNCTION_NAME]]
           
           elif l_term is None:
        
              if '_'+str(self.dummy_count+1)+'dummy' not in Var_Facts:
               
                 list_fact=[]
                    
                 list_fact.append('_y'+str(len(Var_Facts)+1))
           
                 list_fact.append('void')
                 
                 self.dummy_count=self.dummy_count+1
                     
                 Var_Facts['_'+str(self.dummy_count)+'dummy']=list_fact
                 
              if len(self.Map_inheritance[format(ir_expression.destination.type)])>0:
             
                        
                 retExpr = ['-1','if1',['==',['Is'+format(ir_expression.destination.type),dest_name],['True']],['-1','=',['_'+str(self.dummy_count)+'dummy'],[format(ir_expression.destination.type)+'_'+FUNCTION_NAME,dest_name]]]
                 
                 
                 if '_'+str(self.dummy_count+1)+'dummy' not in Var_Facts:
               
                    list_fact=[]
                    
                    list_fact.append('_y'+str(len(Var_Facts)+1))
                    
                    list_fact.append('CONRT_'+format(ir_expression.destination.type))
           
                    list_fact.append('bool')
                                      
                    Var_Facts['Is'+format(ir_expression.destination.type)]=list_fact
                 
                 for item in self.Map_inheritance[format(ir_expression.destination.type)]:
                 
                     if retExpr is not None:
                     
                        if '_'+str(self.dummy_count+1)+'dummy' not in Var_Facts:
               
                           list_fact=[]
                    
                           list_fact.append('_y'+str(len(Var_Facts)+1))
                    
                           list_fact.append('CONRT_'+item)
           
                           list_fact.append('bool')
                                      
                           Var_Facts['Is'+item]=list_fact
                     
                        retExpr = ['-1','if2',['==',['Is'+item,dest_name],['True']],['-1','=',['_'+str(self.dummy_count)+'dummy'],[item+'_'+FUNCTION_NAME,dest_name]],retExpr]
                 
                   
                 return retExpr                   
                 
                 #return ['-1','=',['_'+str(self.dummy_count)+'dummy'],[format(ir_expression.destination.type)+'_'+FUNCTION_NAME,[ir_expression.destination.name]]]
              
              else:
        
                 return ['-1','=',['_'+str(self.dummy_count)+'dummy'],[format(ir_expression.destination.type)+'_'+FUNCTION_NAME, dest_name]]
           
           else:
        
              print('IR construction Error!')
       
              return None       
        

    '''
    #Handling new keyword 
    '''
    def libraryfunctionExpression(self, ir_expression,Var_Facts):
    

        arguments = []

        lvalue = ir_expression.lvalue
    
        l_term = self.variableHandling(lvalue,Var_Facts)
        
             
        
        if ir_expression.contract is not None:
        
           FUNCTION_NAME = format(ir_expression.destination)+'_'+format(ir_expression.function_name)

        else:
                                                      
           FUNCTION_NAME = format(ir_expression.function_name)
        
                             
        #if ir_expression.nbr_arguments>0:
        if len(ir_expression.arguments)>0:
    
           arguments.append(FUNCTION_NAME)
                                                             
           for argument in ir_expression.arguments:
        
               arg = self.variableHandling(argument,Var_Facts)
               
               if arg[0] in self.map_REF:
               
                  arg = self.map_REF[arg[0]]
               
               if arg[0] in self.map_TMP:
               
                  arg = self.map_TMP[arg[0]]               
          
               if arg is not None:
        
                  arguments.append(arg)
               
           if l_term is not None and len(arguments)>0:
           
           
              if 'REF' in l_term[0] and l_term[0] not in self.map_REF:
               
                  self.map_REF[l_term[0]]=arguments
               
              if 'TMP' in l_term[0] and l_term[0] not in self.map_TMP:
               
                  self.map_TMP[l_term[0]]=arguments              

              return ['-1','=',l_term,arguments]
           
           
           elif l_term is None and len(arguments)>0:
        
               if '_'+str(self.dummy_count+1)+'dummy' not in Var_Facts:
               
                  list_fact=[]
                    
                  list_fact.append('_y'+str(len(Var_Facts)+1))
           
                  list_fact.append('void')
                  
                  self.dummy_count=self.dummy_count+1
                     
                  Var_Facts['_'+str(self.dummy_count)+'dummy']=list_fact

 
               return ['-1','=',['_'+str(self.dummy_count)+'dummy'],arguments]
           
           else:
        
               print('IR construction Error!')
       
               return None 
           
        else: 
        
    
           if l_term is not None:
        
              return ['-1','=',l_term,[FUNCTION_NAME]]
           
           elif l_term is not None:
        
              if '_'+str(self.dummy_count+1)+'dummy' not in Var_Facts:
               
                 list_fact=[]
                    
                 list_fact.append('_y'+str(len(Var_Facts)+1))
           
                 list_fact.append('void')
                 
                 self.dummy_count=self.dummy_count+1
                     
                 Var_Facts['_'+str(self.dummy_count)+'dummy']=list_fact
        
              return ['-1','=',['_'+str(self.dummy_count)+'dummy'],[FUNCTION_NAME]]
           
           else:
        
              print('IR construction Error!')
       
              return None       


    '''
    #Handling new keyword 
    '''
    def internalfunctionExpression(self, ir_expression,Var_Facts):

        arguments = []

        lvalue = ir_expression.lvalue
    
        l_term = self.variableHandling(lvalue,Var_Facts)
                                                       
        FUNCTION_NAME = format(ir_expression.function_name)
        
        CONTRACT_NAME = format(ir_expression.contract_name)
        
        #print('@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@')
        
        #print(ir_expression.__dict__)
        
        #print('----------------------------------------')
        
        #print(FUNCTION_NAME)
        
        #print('@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@')
                             
        if ir_expression.nbr_arguments>0:
    
           arguments.append(CONTRACT_NAME+'_'+FUNCTION_NAME)
           
           if self.current_contract.kind!='library':
           
              arguments.append(['_c1'])
                                                             
           for argument in ir_expression.arguments:
        
               arg = self.variableHandling(argument,Var_Facts)
               
                                             
               if arg[0] in self.map_REF:
               
                  arg = self.map_REF[arg[0]]
               
               elif arg[0] in self.map_TMP:
               
                  arg = self.map_TMP[arg[0]] 
                  
          
               if arg is not None:
        
                  arguments.append(arg)
                  

               
           if l_term is not None and len(arguments)>0:
           
           
              if 'REF' in l_term[0]  and l_term[0] not in self.map_REF:
               
                 self.map_REF[l_term[0]] = arguments
               
              if 'TMP' in l_term[0]  and l_term[0] not in self.map_TMP:
               
                 self.map_TMP[l_term[0]] = arguments
                 
        
              return ['-1','=',l_term,arguments]
           
           
           elif l_term is None and len(arguments)>0:
        
               if '_'+str(self.dummy_count+1)+'dummy' not in Var_Facts:
               
                  list_fact=[]
                    
                  list_fact.append('_y'+str(len(Var_Facts)+1))
           
                  list_fact.append('void')
                  
                  self.dummy_count=self.dummy_count+1
                     
                  Var_Facts['_'+str(self.dummy_count)+'dummy']=list_fact

        
               return ['-1','=',['_'+str(self.dummy_count)+'dummy'],arguments]
           
           else:
        
               print('IR construction Error!')
       
               return None 
           
        else: 
        
           #print('------------------------/////////////////////////')
           #print(l_term)
           #print(type(ir_expression.function))
           #print('------------------------/////////////////////////')        
    
           if l_term is not None:
           
              if 'REF' in l_term[0]  and l_term[0] not in self.map_REF:
               
                 self.map_REF[l_term[0]] = [CONTRACT_NAME+'_'+FUNCTION_NAME,['_c1']]
               
              if 'TMP' in l_term[0]  and l_term[0] not in self.map_TMP:
               
                 self.map_TMP[l_term[0]] = [CONTRACT_NAME+'_'+FUNCTION_NAME,['_c1']]
        
              return ['-1','=',l_term,[CONTRACT_NAME+'_'+FUNCTION_NAME,['_c1']]]
           
           elif l_term is None:
           
              if type(ir_expression.function)==slither.core.declarations.modifier.Modifier:
              
              
                 if '_'+str(self.modifier_count+1)+'modifier' not in Var_Facts:
               
                    list_fact=[]
                    
                    list_fact.append('_y'+str(len(Var_Facts)+1))
           
                    list_fact.append('void')
                 
                    self.modifier_count=self.modifier_count+1
                     
                    Var_Facts['_'+str(self.modifier_count)+'modifier']=list_fact
        
                    return ['-1','=',['_'+str(self.modifier_count)+'modifier'],[CONTRACT_NAME+'_'+FUNCTION_NAME]]


        
              elif '_'+str(self.dummy_count+1)+'dummy' not in Var_Facts:
               
                 list_fact=[]
                    
                 list_fact.append('_y'+str(len(Var_Facts)+1))
           
                 list_fact.append('void')
                 
                 self.dummy_count=self.dummy_count+1
                     
                 Var_Facts['_'+str(self.dummy_count)+'dummy']=list_fact
        
                 return ['-1','=',['_'+str(self.dummy_count)+'dummy'],[CONTRACT_NAME+'_'+FUNCTION_NAME,['this']]]
           
           else:
           
              
        
              print('IR construction Error!')
       
              return None       



    '''
    #Handling new keyword Contract
    '''
    def newExpression(self, ir_expression,Var_Facts):

        arguments = []

        lvalue = ir_expression.lvalue
    
        l_term = self.variableHandling(lvalue,Var_Facts)
    
        for argument in ir_expression.arguments:
    
            arg = self.variableHandling(argument,Var_Facts)
    
            if arg is not None:
        
               arguments.append(arg)
           
        CLASS = format(ir_expression.contract_name)
    
        if CLASS not in Var_Facts:
                
           list_fact=[]
                     
           list_fact.append('_y'+str(len(Var_Facts)+1))
           
           list_fact.append(CLASS)
           
           list_fact.append('bool')
          
           Var_Facts[CLASS]=list_fact

           
        if l_term is not None and CLASS is not None:
        
           if l_term[0] in self.map_NEW:
           
              self.map_NEW[l_term[0]]=['-1','NEW',[CLASS],l_term,arguments]
        
           return ['-1','NEW',[CLASS],l_term,arguments]
    
        else:
       
           print('IR construction Error!')
       
           return None


    '''
    #Handling new keyword Structure
    '''
    def newExpressionStruct(self, ir_expression,Var_Facts):

        arguments = {}

        lvalue = ir_expression.lvalue
    
        l_term = self.variableHandling(lvalue,Var_Facts)
        
        
        CLASS = format(ir_expression.structure)
                
      
        for i in range(0,len(ir_expression.arguments)):
        
            argument = ir_expression.arguments[i]
            
            arg = self.variableHandling(argument,Var_Facts)
            
            class_mem_name = format(list(ir_expression.structure.elems.values())[i].structure)+'_'+format(list(ir_expression.structure.elems.values())[i].name)
            
            class_mem = ['=',[class_mem_name,l_term],arg]
            
            if class_mem_name not in Var_Facts:
               
                list_fact=[]
                    
                list_fact.append('_y'+str(len(Var_Facts)+1))
           
                list_fact.append(format(list(ir_expression.structure.elems.values())[i].structure))
                
                list_fact.append(format(list(ir_expression.structure.elems.values())[i].type))
                                       
                Var_Facts[class_mem_name]=list_fact
            
    
            if class_mem is not None:
        
               arguments[class_mem_name]=class_mem
           
        
        if CLASS not in Var_Facts:
                
           list_fact=[]
                     
           list_fact.append('_y'+str(len(Var_Facts)+1))
           
           list_fact.append(CLASS)
           
           list_fact.append('bool')
          
           Var_Facts[CLASS]=list_fact

           
        if l_term is not None and CLASS is not None:
        
           if l_term[0] not in self.map_NEW:
           
              self.map_NEW[l_term[0]]=['-1','NEW',[CLASS],l_term,arguments]
                
           return ['-1','NEW',[CLASS],l_term,arguments]
    
        else:
       
           print('IR construction Error!')
       
           return None


    '''
    #Handling Assigment Expression
    '''
    def assigmentExpression(self, ir_expression,Var_Facts):

        lvalue = ir_expression.lvalue
                  
        rvalue = ir_expression.rvalue
              

        l_term = self.variableHandling(lvalue,Var_Facts)
        
        if l_term[0] in self.map_REF:
        
           l_term = self.map_REF[l_term[0]]
               
        r_term = self.variableHandling(rvalue,Var_Facts)
        
        if r_term[0] in self.map_TMP:
        
           r_term = self.map_TMP[r_term[0]]

        if r_term[0] in self.map_REF:
        
           r_term = self.map_REF[r_term[0]]
           

        if l_term is not None and r_term is not None:
        
                   
           if r_term[0] in self.map_NEW:
           

              new_l_term = self.map_NEW[r_term[0]]
                            
              if new_l_term[1]=='NEW':
              
                 new_l_term[3]=l_term
                 
                 for para_meter in new_l_term[4]:
                                                            
                     new_l_term[4][para_meter] = folTras.expr_replace(new_l_term[4][para_meter], r_term, l_term)
                 
                 assigm_term=new_l_term
              
              else: 
              
                 assigm_term=['-1','=',l_term,r_term]

              
           elif r_term[0] in self.map_NEW_ELEMENT:
           
                if l_term[0] in Var_Facts and Var_Facts[l_term[0]][1]==self.map_NEW_ELEMENT[r_term[0]][3][0]:
                
                
                   Var_Facts[l_term[0]][1]='array'
                

                   if 'd'+str(len(self.map_NEW_ELEMENT[r_term[0]][4]))+'T'+Var_Facts[l_term[0]][1] not in Var_Facts:
           
                      list_fact=[]
              
                      list_fact.append('_y'+str(len(Var_Facts)+1))
                                                  
                      list_fact.append('array')
           
                      list_fact.append('uint256')
          
                      Var_Facts['d'+str(len(self.map_NEW_ELEMENT[r_term[0]][4]))+'T'+Var_Facts[l_term[0]][1]]=list_fact
                      
                      Var_Facts[l_term[0]]=list_fact


                
                
                if 'array_length' not in Var_Facts:
           
                    list_fact=[]
              
                    list_fact.append('_y'+str(len(Var_Facts)+1))
                                                  
                    list_fact.append('array')
           
                    list_fact.append('uint256')
          
                    Var_Facts['array_length']=list_fact

         
                assigm_term= ['-1','=',['array_length',l_term],self.map_NEW_ELEMENT[r_term[0]][4][0]]
                               
           else:
           
              if len(self.stack_infec_call)>0 and format(rvalue.type)=='bytes4' and folTras.is_number(r_term[0]):
              
                 new_r_term = ['SOLIDITY_CALL_interfaceId',self.stack_infec_call.pop()]
                 
                 self.solidity_call.append(['-1','=',new_r_term,r_term])
              
                 r_term = new_r_term
                 
                 
              
              
           
              assigm_term=['-1','=',l_term,r_term]
           
              if 'TMP' in l_term[0]:
        
                 self.map_TMP[l_term[0]]=r_term
                 
              elif 'REF' in l_term[0] and l_term[0] not in self.map_REF:
        
                 self.map_REF[l_term[0]]=r_term

                                     
           #print("\t\t\t{}".format(assigm_term))

           return assigm_term
          
        else:
       
          print('IR construction Error!')
       
          return None



    '''
    #Handling Array Index Expression
    '''
    def arrayIndexExpression(self, ir_expression,Var_Facts):
                   
        left_term = None
                           
        right_term = None
                           
        lvalue = ir_expression.lvalue
                           
        for var in ir_expression._variables:
                           
            if left_term==None:
                               
               left_term=var
                                  
            elif left_term is not None and right_term==None:
                               
               right_term=var
          
                   
        l_value = self.variableHandling(lvalue,Var_Facts) 
                
        l_term = self.variableHandling(left_term,Var_Facts)
    
        r_term = self.variableHandling(right_term,Var_Facts)
        
        
           
        if self.indexType(left_term)=='Mapping':
    
           if l_value is not None and l_term is not None and r_term is not None:
           
              map_value, map_from, map_to=self.getMappingType(left_term)
                                            
              if type(left_term)==slither.slithir.variables.reference.ReferenceVariable:
              
              
                 if l_term[0] in self.map_REF:
                 
                    map_fun = self.map_REF[l_term[0]][0]
                    
                    new_l_term = self.map_REF[l_term[0]][1:]
                    
                    if r_term[0] in self.map_REF:
                    
                       r_term = self.map_REF[r_term[0]]
                    

                    if r_term[0] in self.map_TMP:
                    
                       r_term = self.map_TMP[r_term[0]]
                    
                    if map_fun is not None and map_fun.startswith('d'+str(len(new_l_term)-1))==False and map_fun.endswith('mapping')==False:
                    
                       new_l_term = self.map_REF[l_term[0]]
                       
                       mapping_index_term = ['-1','=',l_value,['d1'+'F'+str(map_from)+'T'+str(map_to)+'mapping']+[new_l_term]+[r_term]]
                       
                       if l_value is not None and 'REF' in l_value[0]:
        
                          self.map_REF[l_value[0]]=['d1'+'F'+str(map_from)+'T'+str(map_to)+'mapping']+[new_l_term]+[r_term]
                          
                       if 'd1'+'F'+str(map_from)+'T'+str(map_to)+'mapping' not in Var_Facts:
           
                          list_fact=[]
              
                          list_fact.append('_y'+str(len(Var_Facts)+1))
                                                  
                          list_fact.append(Var_Facts[new_l_term[0]][-1])
           
                          list_fact.append(map_from)

                          list_fact.append(map_to)
          
                          Var_Facts['d1'+'F'+str(map_from)+'T'+str(map_to)+'mapping']=list_fact
           
                       
                
                    else:
                                                         
                       mapping_index_term = ['-1','=',l_value,['d'+str(len(new_l_term))+'F'+str(map_from)+'T'+str(map_to)+'mapping']+new_l_term+[r_term]]
                       
                       if l_value is not None and 'REF' in l_value[0]:
        
                          self.map_REF[l_value[0]]=['d'+str(len(new_l_term))+'F'+str(map_from)+'T'+str(map_to)+'mapping']+new_l_term+[r_term]
                          
                       if 'd'+str(len(new_l_term))+'F'+str(map_from)+'T'+str(map_to)+'mapping' not in Var_Facts:
           
                          list_fact=[]
              
                          list_fact.append('_y'+str(len(Var_Facts)+1))
                          
                          #self.map_all_function
                       
                          for v_term in new_l_term:
                                                                               
                              if v_term[0] in Var_Facts:
                                                         
                                 list_fact.append(Var_Facts[v_term[0]][-1])
                                 
                              elif v_term[0] in self.map_all_function:
                              
                                 list_fact.append(self.map_all_function[v_term[0]][-1])
                              

           
                          list_fact.append(map_from)

                          list_fact.append(map_to)
                                                    
          
                          Var_Facts['d'+str(len(new_l_term))+'F'+str(map_from)+'T'+str(map_to)+'mapping']=list_fact

                    
                    
                    
                 else:
                 
                    mapping_index_term=['-1','=',l_value,['d1F'+str(map_from)+'T'+str(map_to)+'mapping',l_term,r_term]]
                    
                    if 'd1F'+str(map_from)+'T'+str(map_to)+'mapping' not in Var_Facts:
           
                       list_fact=[]
              
                       list_fact.append('_y'+str(len(Var_Facts)+1))
           
                       list_fact.append(map_value)
           
                       list_fact.append(map_from)

                       list_fact.append(map_to)
          
                       Var_Facts['d1F'+str(map_from)+'T'+str(map_to)+'mapping']=list_fact
                       
                    if l_value is not None and 'REF' in l_value[0]:
        
                       self.map_REF[l_value[0]]=['d1F'+str(map_from)+'T'+str(map_to)+'mapping',l_term,r_term]
              
              else:
              
                 if r_term[0] in self.map_REF:
                    
                     r_term = self.map_REF[r_term[0]]
                    
                 if r_term[0] in self.map_TMP:
                    
                     r_term = self.map_TMP[r_term[0]]

              
                 mapping_index_term=['-1','=',l_value,['d1F'+str(map_from)+'T'+str(map_to)+'mapping',l_term,r_term]] 
                 
                 if 'd1F'+str(map_from)+'T'+str(map_to)+'mapping' not in Var_Facts:
           
                    list_fact=[]
              
                    list_fact.append('_y'+str(len(Var_Facts)+1))
           
                    list_fact.append(map_value)
           
                    list_fact.append(map_from)

                    list_fact.append(map_to)
          
                    Var_Facts['d1F'+str(map_from)+'T'+str(map_to)+'mapping']=list_fact
              
                 if l_value is not None and 'REF' in l_value[0]:
        
                    self.map_REF[l_value[0]]=['d1F'+str(map_from)+'T'+str(map_to)+'mapping',l_term,r_term]

              return mapping_index_term

           else:
    
              print('IR construction Error!')
              return None
   
           
        elif self.indexType(left_term)=='Array':
        
          
           if l_value is not None and l_term is not None and r_term is not None:
           
              ARRAY_TYPE=self.getArrayType(left_term)
              
              if '[]' in ARRAY_TYPE:
              
                 ARRAY_TYPE='array'
                 
                 
              ARRAY_TYPE_INDEX=Var_Facts[r_term[0]][-1]
                                 
              if type(left_term)==slither.slithir.variables.reference.ReferenceVariable:
              
              
                 if l_term[0] in self.map_REF:
                 
                    array_fun = self.map_REF[l_term[0]][0]
                    
                    new_l_term = self.map_REF[l_term[0]][1:]
                    
                    if r_term[0] in self.map_REF:
                    
                       r_term = self.map_REF[r_term[0]]
                    
                    if r_term[0] in self.map_TMP:
                    
                       r_term = self.map_TMP[r_term[0]]
                    
                    if array_fun is not None and array_fun.startswith('d'+str(len(new_l_term)-1))==False and array_fun.endswith('array')==False:
                    
                       new_l_term = self.map_REF[l_term[0]]
                       
                    
                       array_index_term=['-1','=',l_value,['d1T'+ARRAY_TYPE+'array']+[new_l_term]+[r_term]]
                       
                       if 'd1T'+ARRAY_TYPE+'array' not in Var_Facts:
       
                          list_fact=[]
       
                          list_fact.append('_y'+str(len(Var_Facts)+1))
                          
                          #list_fact.append(Var_Facts[new_l_term[0]][-1])
                                                  
                          list_fact.append('array') 
                                                                                             
                          if folTras.is_number(r_term[0])==True:

                             list_fact.append('uint256') 
                                       
                          else:
                                                               
                             list_fact.append(ARRAY_TYPE_INDEX)

                          list_fact.append(ARRAY_TYPE)
          
                          Var_Facts['d1T'+ARRAY_TYPE+'array']=list_fact
                          
                       if l_value is not None and 'REF' in l_value[0]:
        
                          self.map_REF[l_value[0]]=['d1T'+ARRAY_TYPE+'array']+[new_l_term]+[r_term]
                    
                   
                    else:

                       
                       array_index_term=['-1','=',l_value,['d'+str(len(new_l_term))+'T'+ARRAY_TYPE+'array']+new_l_term+[r_term]]
                    
                       if 'd'+str(len(new_l_term))+'T'+ARRAY_TYPE+'array' not in Var_Facts:
       
                          list_fact=[]
       
                          list_fact.append('_y'+str(len(Var_Facts)+1))
                                              
                          for v_term in new_l_term:
                       
                              if v_term[0] in Var_Facts:

                                 list_fact.append('array')
                                 #list_fact.append(Var_Facts[v_term[0]][-1])
                                               
                          if folTras.is_number(r_term[0])==True:

                             list_fact.append('uint256') 
                                       
                          else:
                 
                             list_fact.append(ARRAY_TYPE_INDEX)

                          list_fact.append(ARRAY_TYPE)
          
                          Var_Facts['d'+str(len(new_l_term))+'T'+ARRAY_TYPE+'array']=list_fact                                                         
                    
                       if l_value is not None and 'REF' in l_value[0]:
        
                          self.map_REF[l_value[0]]=['d'+str(len(new_l_term))+'T'+ARRAY_TYPE+'array']+new_l_term+[r_term]
                       
                 else:
                 
                 
                    if r_term[0] in self.map_REF:
                    
                       r_term = self.map_REF[r_term[0]]
                    
                    if r_term[0] in self.map_TMP:
                    
                       r_term = self.map_TMP[r_term[0]]
                 
                    if 'd1T'+ARRAY_TYPE+'array' not in Var_Facts:
       
                       list_fact=[]
       
                       list_fact.append('_y'+str(len(Var_Facts)+1))
           
                       #list_fact.append(Var_Facts[l_term[0]][-1])

                       list_fact.append('array')
                 
                       if folTras.is_number(r_term[0])==True:

                          list_fact.append('uint256') 
                                       
                       else:
                 
                         list_fact.append(ARRAY_TYPE_INDEX)

                       list_fact.append(ARRAY_TYPE)
                       
                       Var_Facts['d1T'+ARRAY_TYPE+'array']=list_fact


                    array_index_term=['-1','=',l_value,['d1T'+ARRAY_TYPE+'array',l_term,r_term]]
                    
                    if l_value is not None and 'REF' in l_value[0]:
        
                       self.map_REF[l_value[0]]=['d1T'+ARRAY_TYPE+'array',l_term,r_term]
              
              else:
                       
                            
                 if 'd1T'+ARRAY_TYPE+'array' not in Var_Facts:
       
                    list_fact=[]
       
                    list_fact.append('_y'+str(len(Var_Facts)+1))
           
                    #list_fact.append(Var_Facts[l_term[0]][-1])
                    
                    list_fact.append('array')
                 
                    if folTras.is_number(r_term[0])==True:

                       list_fact.append('uint256') 
                                       
                    else:
                 
                       list_fact.append(Var_Facts[r_term[0]][-1])

                    list_fact.append(ARRAY_TYPE)
                    
                    Var_Facts['d1T'+ARRAY_TYPE+'array']=list_fact

                 if r_term[0] in self.map_REF:
                    
                     r_term = self.map_REF[r_term[0]]
                    
                 if r_term[0] in self.map_TMP:
                    
                     r_term = self.map_TMP[r_term[0]]
              
                 array_index_term=['-1','=',l_value,['d1T'+ARRAY_TYPE+'array',l_term,r_term]]
                 
                 if l_value is not None and 'REF' in l_value[0]:
        
                    self.map_REF[l_value[0]]=['d1T'+ARRAY_TYPE+'array',l_term,r_term]
                   
              if Var_Facts[array_index_term[-1][1][0]][-1]!='array':
                 Var_Facts[array_index_term[-1][1][0]][-1]='array'
              return array_index_term  
              
           else:
    
              print('IR construction Error!!')
       
              return None 
              
              
        elif self.indexType(left_term)==None:
        
    
           if l_value is not None and l_term is not None and r_term is not None:
           
              ARRAY_TYPE=self.getArrayType(left_term)
              
              if '[]' in ARRAY_TYPE:
              
                 ARRAY_TYPE='array'
                     
           
              if l_term[0] in self.map_REF:
                    
                 l_term = self.map_REF[l_term[0]]
                    
              if l_term[0] in self.map_TMP:
                    
                 l_term = self.map_TMP[l_term[0]]
           
              if r_term[0] in self.map_REF:
                    
                 r_term = self.map_REF[r_term[0]]
                    
              if r_term[0] in self.map_TMP:
                    
                 r_term = self.map_TMP[r_term[0]]
              
                    
              if 'd1T'+ARRAY_TYPE+'array' not in Var_Facts:
       
                 list_fact=[]
       
                 list_fact.append('_y'+str(len(Var_Facts)+1))
                            
                 #list_fact.append(Var_Facts[l_term[0]][-1])
                 
                 
                 list_fact.append('array')
           
                 if folTras.is_number(r_term[0])==True:

                    list_fact.append('uint256')
                                     
                 else:
                                     
                    if r_term[0] in folTras._infix_op:
                    
                       if r_term[1][0] in Var_Facts:
                       
                          list_fact.append(Var_Facts[r_term[1][0]][1])
                       
                       elif r_term[2][0] in Var_Facts:
                       
                          list_fact.append(Var_Facts[r_term[2][0]][1])
                          
                       else:
                       
                           list_fact.append('uint256')
                    
                    else:
                 
                       list_fact.append(Var_Facts[r_term[0]][1])

                 list_fact.append(ARRAY_TYPE)
          
                 Var_Facts['d1T'+ARRAY_TYPE+'array']=list_fact
                 

              #if l_value is not None and 'REF' in l_value[0]:
              
              #   ARRAY_TYPE=self.getArrayType(left_term)
        
              #   l_value = ['REFERENCE_'+ARRAY_TYPE+'_TYPE',l_value]
           
              #   if 'REFERENCE_'+ARRAY_TYPE+'_TYPE' not in Var_Facts:
           
              #     list_fact=[]
                            
              #     list_fact.append('_y'+str(len(Var_Facts)+1))
           
              #     list_fact.append(ARRAY_TYPE)
               
              #     list_fact.append()
          
              #     Var_Facts['REFERENCE_'+ARRAY_TYPE+'_TYPE']=list_fact
                   

              if type(lvalue)==slither.slithir.variables.reference.ReferenceVariable:
           
                 array_index_term=['-1','=',l_value,['d1T'+ARRAY_TYPE+'array',l_term,r_term]]
              
              else:
              
                 array_index_term=['-1','=',l_value,['d1T'+ARRAY_TYPE+'array',l_term,r_term]]
              #print("\t\t\t{}".format(array_index_term))
              
              if l_value is not None and 'REF' in l_value[0] and l_value[0] not in self.map_REF:
        
                 self.map_REF[l_value[0]]=['d1T'+ARRAY_TYPE+'array',l_term,r_term]
                 
              if l_value is not None and 'TMP' in l_value[0] and l_value[0] not in self.map_TMP:
        
                 self.map_TMP[l_value[0]]=['d1T'+ARRAY_TYPE+'array',l_term,r_term]
              
              return array_index_term  
              
           else:
    
              print('IR construction Error!!')
       
              return None       
      
        else:
        

    
           print('IR construction Error!')
       
           return None




    '''
    #Handling Return Value
    '''

    def returnExpression(self, ir_expression,Var_Facts):

       if len(ir_expression._values)==1:
       
          ret_obj = ir_expression._values[0]
    
          ret_value=self.variableHandling(ret_obj,Var_Facts)
          
          RET_TYPE=None
          
          if ret_value[0] in Var_Facts:
          
             RET_TYPE=Var_Facts[ret_value[0]][-1]
          
          
          if type(ret_obj)==slither.slithir.variables.reference.ReferenceVariable:
                       
             if ret_value[0] in self.map_REF:
                             
                ret_value=self.map_REF[ret_value[0]]
                
                
          if ret_value[0] in self.map_TMP:
                             
              ret_value=self.map_TMP[ret_value[0]]
              
            
          if RET_TYPE is None and ret_value[0] in Var_Facts:
          
             RET_TYPE=Var_Facts[ret_value[0]][-1]
             
             
                                     
          if ret_value is not None:
       
             ret_term = ['-1','=',['ret'],ret_value]
          
             if 'ret' not in Var_Facts:
        
                list_fact=[]
       
                list_fact.append('_y'+str(len(Var_Facts)+1))
                                
                if ret_value[0] not in Var_Facts:
                
                   if folTras.is_number(ret_value[0])==True:
                
                      list_fact.append('uint256')
                   
                   elif ret_value[0]=='True' or ret_value[0]=='False':
              
                      list_fact.append('bool')
                      
                   else:
                   
                       if RET_TYPE is not None:
                       
                          list_fact.append(RET_TYPE)
                          
                       elif ret_value[0]=='String':
                      
                          list_fact.append('string')
                      
                   
                else:
                        
                   list_fact.append(Var_Facts[ret_value[0]][-1])
           
                Var_Facts['ret']=list_fact

          
             #print("\t\t\t{}".format(ret_term))
       
             return ret_term
      
          else:
    
             print('IR construction Error!')
         
             return None
         
       else:

          parameters = []
       
          update_list = []
          
          list_fact=[]
       
          for ret_obj in ir_expression._values:
    
              ret_value=self.variableHandling(ret_obj,Var_Facts)
              
              if ret_value[0] in Var_Facts:
                            
                 list_fact.append(Var_Facts[ret_value[0]][-1])
                 
              elif folTras.is_number(ret_value[0])==True:
              
              
                 list_fact.append('uint256')
              
              
              elif ret_value[0]=='True' or ret_value[0]=='False':
              
                 list_fact.append('bool')
                           
                         
              parameters.append(ret_value)
           
                     
          if len(parameters)>0:
       
             ret_term = ['-1','=',['ret'],folTras.expres('D'+str(len(parameters))+'TUPLE',parameters)]
             
             if 'D'+str(len(parameters))+'TUPLE' not in Var_Facts:
             
                list_fact=['_y'+str(len(Var_Facts)+1)]+list_fact
                
                list_fact.append('TUPLE')
                
                
                Var_Facts['D'+str(len(parameters))+'TUPLE']=list_fact
                
          
             if 'ret' not in Var_Facts:
        
                list_fact=[]
       
                list_fact.append('_y'+str(len(Var_Facts)+1))
             
                list_fact.append('TUPLE')
                                                 
                Var_Facts['ret']=list_fact

          
          #print("\t\t\t{}".format(ret_term))
       
          return ret_term

    
    '''
    #Handling Array Length
    '''

    def arrayLengthExpression(self, ir_expression,Var_Facts):

        lvalue = ir_expression.lvalue
                                    
        l_value = self.variableHandling(lvalue,Var_Facts) 
    
        array_name = self.variableHandling(ir_expression._value,Var_Facts)
                

        if array_name is not None and l_value is not None:
        
           if type(lvalue)==slither.slithir.variables.reference.ReferenceVariable:
           
              if array_name[0] in self.map_REF:
              
                 new_l_term = self.map_REF[array_name[0]]
                 
                 array_len_term = ['-1','=',l_value,['array_length']+[new_l_term]]
                 
                 if 'array_length' not in Var_Facts:
        
                    list_fact=[]
       
                    list_fact.append('_y'+str(len(Var_Facts)+1))
                    
                    #list_fact.append(Var_Facts[new_l_term[0]][-1])
                    
                    list_fact.append('array')
                      
                    list_fact.append('uint256')

                    Var_Facts['array_length']=list_fact

                 if l_value is not None and 'REF' in l_value[0]:
        
                    self.map_REF[l_value[0]]=['array_length']+[new_l_term]

              
              
              else:
              
                 array_len_term = ['-1','=',l_value,['array_length',array_name]]
              
                 if 'array_length' not in Var_Facts:
        
                    list_fact=[]
       
                    list_fact.append('_y'+str(len(Var_Facts)+1))
           
                    #list_fact.append(Var_Facts[array_name[0]][-1])
                    
                    list_fact.append('array')
           
                    list_fact.append('uint256')

                    Var_Facts['array_length']=list_fact

                 if l_value is not None and 'REF' in l_value[0]:
        
                    self.map_REF[l_value[0]]=['array_length',array_name] 
           
           else:
           
              array_len_term = ['-1','=',l_value,['array_length',array_name]]
              
              if 'array_length' not in Var_Facts:
        
                 list_fact=[]
       
                 list_fact.append('_y'+str(len(Var_Facts)+1))
           
                 #list_fact.append(Var_Facts[array_name[0]][-1])
                 
                 list_fact.append('array')
           
                 list_fact.append('uint256')

                 Var_Facts['array_length']=list_fact

              if l_value is not None and 'REF' in l_value[0]:
        
                self.map_REF[l_value[0]]=['array_length',array_name]              
              
           
           return array_len_term
      
        else:
    
           print('IR construction Error!')



    '''
    #Handling Array Initaization
    '''

    def arrayInitaization(self, ir_expression,Var_Facts):

       lvalue = ir_expression.lvalue
                                    
       array_name = self.variableHandling(lvalue,Var_Facts) 
    
       exp_stack=[]
    
       if array_name is not None:
    
          counter=0
        
          for value in ir_expression._init_values:
        
              counter=counter+1
              
              ARRAY_TYPE=self.getArrayType(lvalue)
              
              if '[]' in ARRAY_TYPE:
              
               update_arry_expression = [array_name]+[[str(counter-1)]]
               
               update_exp_stack = self.recarrayInitaization(update_arry_expression, value, ARRAY_TYPE, Var_Facts)
               
               for exp in update_exp_stack:
               
                  exp_stack.append(exp)
             
              else:
                   
                 exp_stack.append(['-1','=',['d1T'+ARRAY_TYPE+'array',array_name,[str(counter-1)]],[str(value)]])
            

          if '[]' in ARRAY_TYPE:
        
             ARRAY_TYPE=ARRAY_TYPE.replace('[]','')

          if len(exp_stack)>0:
          
             array_defi = exp_stack[0][2][0]
             
             if array_defi not in Var_Facts:
             
                list_fact=[]
       
                list_fact.append('_y'+str(len(Var_Facts)+1))
                
                list_fact.append(Var_Facts[array_name[0]][-1])
             
                for i  in range(0,len(exp_stack[0][2][1:])-1):
                
                    list_fact.append('uint256')

                list_fact.append(ARRAY_TYPE)
          
                Var_Facts[array_defi]=list_fact             


       
          array_init_term = self.IRTranslation(exp_stack,Var_Facts)
          
        
          return array_init_term
      
       else:
    
          print('IR construction Error!')
        
          return None


    '''
    #Rec Init Array Construction
    '''
    def recarrayInitaization(self, arry_expression, values, ARRAY_TYPE, Var_Facts):
    
        counter=0
        
        exp_stack=[]
        
        if '[]' in ARRAY_TYPE:
        
           ARRAY_TYPE=ARRAY_TYPE.replace('[]','')
    
        for value in values:
        
            counter=counter+1
            
            if type(value)==list:
            
               update_arry_expression = arry_expression+[[str(counter-1)]]
               
               update_exp_stack = self.recarrayInitaization(update_arry_expression, values, Var_Facts)
               
               for exp in update_exp_stack:
               
                  exp_stack.append(exp)
            
            else:
            

               exp_stack.append(['-1','=',['d'+str(len(arry_expression)-1)+'T'+ARRAY_TYPE+'array']+arry_expression+[[str(counter-1)]],[str(value)]])
                          
            
        return exp_stack




    '''
    #Filter  TMP AND REF VARIABLES
    '''

    def IRFilter(self, exp_list, Var_Facts):
    
   
        update_exp_list=[]
                
        for exp in exp_list:
                 
           
           
            if exp[1]=='=':
            
               if 'TMP' not in exp[2][0] and 'REF' not in exp[2][0]:
                  
                  update_exp_list.append(exp)
                  
            elif exp[1]=='NEW':
            
               if 'TMP' not in exp[3][0] and 'REF' not in exp[3][0]:
                  
                  update_exp_list.append(exp)
                  
            else:
            
                if exp[1]!='new_element':
            
                   update_exp_list.append(exp)           
            
                            
        return update_exp_list                



    '''
    #Filter  TMP AND REF VARIABLES
    '''

    def vfactFilter(self, Var_Facts):
    
        update_Var_Facts={}

        for vfact in Var_Facts:
        
            if 'TMP' not in vfact and 'REF' not in vfact:
            
               update_Var_Facts[vfact]=Var_Facts[vfact]
               
             
        return update_Var_Facts                


    '''
    #Construction of IR for Translation
    '''

    def IRTranslation(self, exp_stack, Var_Facts):
    
        exps_inter=None
        
        #update_exp_stack = []
        
        #['-1', '=', ['TUPLE_0'], ['LOW_LEVEL_CALL_call', ['msg_sender'], ['String', ['']]]]
        
        #for exp in exp_stack:
        
            #update_exp_stack.append(exp)
        
            #if exp[1]=='=' and exp[3][0]=='LOW_LEVEL_CALL_call':
                                       
            #   update_exp_stack.append(exp)
               
            #   index = exp_stack.index(exp)
               
            #   update_exp_stack.append(['-1','if1',['==',['EXECPTION_LOW_LEVEL_CALL_call',exp[2]],['False']],self.IRTranslation(exp_stack[index+1:], Var_Facts)])
               
            #   if 'EXECPTION_LOW_LEVEL_CALL_call' not in Var_Facts:
             
            #       list_fact=[]
       
            #       list_fact.append('_y'+str(len(Var_Facts)+1))
                
            #       list_fact.append('TUPLE')
             
            #       list_fact.append('bool')
          
            #       Var_Facts['EXECPTION_LOW_LEVEL_CALL_call']=list_fact             

            #   break;
                           
            #else:
            
            #   update_exp_stack.append(exp)
        
        
        #exp_stack = update_exp_stack
         
        while len(exp_stack)>0:
                    
             if exps_inter is None and len(exp_stack)>1:
                    
                EXPR1 = exp_stack.pop()
                             
                EXPR2 = exp_stack.pop()
                             
                exps_inter=['-1','seq',EXPR2,EXPR1]
                                          
             elif exps_inter is None and len(exp_stack)==1:
                          
                EXPR1 = exp_stack.pop()
                          
                exps_inter = EXPR1
                
             else:
                          
                EXPR1 = exp_stack.pop()
                              
                exps_inter=['-1','seq',EXPR1,exps_inter]

        #print('==========================================')
        #print(exps_inter)                             
        #print('==========================================')
        return exps_inter                  


    '''
    #Get Array Type
    '''
    def getArrayType(self, variable):
    

       if type(variable.type)==slither.core.solidity_types.array_type.ArrayType:
   
          return format(variable.type.type)
          
       elif type(variable.type)==slither.core.solidity_types.elementary_type.ElementaryType:
   
          return format(variable.type.type)
          
       return None
   
   
   
    '''
    #Get Mapping Type
    '''
    def getMappingType(self, variable):

        if type(variable.type)==slither.core.solidity_types.mapping_type.MappingType:
   
           map_to = variable.type._to
           map_from = variable.type._from
           
           MAPPING_TYPE=format(variable.type)
           
           if 'mapping(' in MAPPING_TYPE:
           
              MAPPING_TYPE='map'
               
           MAPPING_TO_TYPE=format(map_to)
           
           if 'mapping(' in MAPPING_TO_TYPE:
           
              MAPPING_TO_TYPE='map'
           
           
           
           
              
           return MAPPING_TYPE, format(map_from), MAPPING_TO_TYPE
            
        return None,None,None


    '''
    #Is Type of Index type 
    '''
    def indexType(self, variable):

       if type(variable.type)==slither.core.solidity_types.array_type.ArrayType:
   
           return 'Array'
      
       elif type(variable.type)==slither.core.solidity_types.mapping_type.MappingType:
   
           return 'Mapping'   
      
       return None
  





    '''
    #Handling Variable
    '''

    def variableHandling(self, variable,Var_Facts): 
    
        if type(variable)==slither.core.declarations.contract.Contract:
                   
           term = [format(variable.name)]
       
           if format(variable.name) not in Var_Facts:
       
               list_fact=[]
       
               list_fact.append('_y'+str(len(Var_Facts)+1))
               
               list_fact.append('CONRT_'+format(variable.name))
               
               Var_Facts[format(variable.name)]=list_fact
               

           return term      
    
        elif type(variable)==slither.core.variables.state_variable.StateVariable and self.current_contract!='library':
        
           #term = [format(variable),['_c1']]
           
           #term = [format(self.current_contract.name)+'_'+format(variable),['_x1']]
           
           term = [format(self.current_contract.name)+'_'+format(variable),['_c1']]
           
           
           if format(variable) not in Var_Facts:
           
              list_fact=[]
              
              if type(variable.type)==slither.core.solidity_types.array_type.ArrayType:
              
                 #if 'd1T'+format(variable.type.type)+'array' not in Var_Facts:
       
                 #   list_fact_array=[]
       
                 #   list_fact_array.append('_y'+str(len(Var_Facts)+1))
           
                 #   list_fact_array.append('array')
           
                 #   list_fact_array.append('uint256')

                 #   list_fact_array.append(format(variable.type.type))
          
                 #   Var_Facts['d1T'+format(variable.type.type)+'array']=list_fact_array
    
                 list_fact.append('_y'+str(len(Var_Facts)+1))
       
                 list_fact.append('CONRT_'+format(variable.contract))
             
                 list_fact.append('array')
       
              else:
              
                 list_fact.append('_y'+str(len(Var_Facts)+1))
       
                 list_fact.append('CONRT_'+format(variable.contract))
                 
                 TYPE_NAME = format(variable.type)
                 
                 if 'mapping(' in TYPE_NAME:
                 
                    list_fact.append('map')
                 
                 else:
                    if type(variable.type)==slither.core.solidity_types.user_defined_type.UserDefinedType:
                    
                       #if type(variable.type.type)==slither.core.declarations.structure_contract.StructureContract:
                       
                       #    list_fact.append('struct')
                       
                       #else:
                       list_fact.append(TYPE_NAME)
                      
                    else:
                    
                       list_fact.append(TYPE_NAME)
                 
              Var_Facts[format(self.current_contract.name)+'_'+format(variable)]=list_fact
                                  
           return term

        elif type(variable)==slither.core.variables.state_variable.StateVariable and self.current_contract=='library':
        
           term = [format(self.current_contract.name)+'_'+format(variable)]
           
           
           if format(variable) not in Var_Facts:
           
              list_fact=[]
              
              if type(variable.type)==slither.core.solidity_types.array_type.ArrayType:
                  
                 list_fact.append('_y'+str(len(Var_Facts)+1))
       
                 #list_fact.append('CONRT_'+format(variable.contract))
             
                 list_fact.append('array')
       
              else:
              
                 list_fact.append('_y'+str(len(Var_Facts)+1))
       
                 #list_fact.append('CONRT_'+format(variable.contract))
                 
                 TYPE_NAME = format(variable.type)
                 
                 if 'mapping(' in TYPE_NAME:
                 
                    list_fact.append('map')
                 
                 else:
                    if type(variable.type)==slither.core.solidity_types.user_defined_type.UserDefinedType:
                    
                       list_fact.append(TYPE_NAME)
                      
                    else:
                    
                       list_fact.append(TYPE_NAME)
                 
              Var_Facts[format(self.current_contract.name)+'_'+format(variable)]=list_fact
                                  
           return term
           
       
        elif type(variable)==slither.core.variables.local_variable.LocalVariable:
                           
            term = [format(variable)]
       
            if format(variable) not in Var_Facts:
       
               list_fact=[]
       
               list_fact.append('_y'+str(len(Var_Facts)+1))
               
               if variable.type is not None:
               
                  TYPE_NAME = format(variable.type.type)
                 
                  if 'mapping(' in TYPE_NAME:
                 
                    list_fact.append('map')
                 
                  else:
             
                    list_fact.append(TYPE_NAME)
                           
               else:

                  list_fact.append("void")               
       
               Var_Facts[format(variable)]=list_fact


            return term
       
        elif type(variable)==slither.slithir.variables.temporary.TemporaryVariable:
                           
            term = [format(variable)]
       
            if format(variable) not in Var_Facts:
       
               list_fact=[]
       
               list_fact.append('_y'+str(len(Var_Facts)+1))
               
               if variable.type is None:
               
                  list_fact.append('void')
               
               else:
               
                  TYPE_NAME = format(variable.type.type)
                 
                  if 'mapping(' in TYPE_NAME:
                 
                    list_fact.append('map')
                 
                  else:
             
                    list_fact.append(TYPE_NAME)
 
                                         
                  #list_fact.append(format(variable.type.type))
       
               Var_Facts[format(variable)]=list_fact

            return term
       
        elif type(variable)==slither.slithir.variables.constant.Constant:
    
            if format(variable.type)=='string':
       
               term = ['String',[format(variable)]]

            else:
       
              term = [format(variable)]
       

            return term
       
        elif type(variable)==slither.slithir.variables.reference.ReferenceVariable:
    
    
            term = [format(variable)]
       
            list_fact=[]
       
            if format(variable) not in Var_Facts:
       
               list_fact=[]
       
               list_fact.append('_y'+str(len(Var_Facts)+1))
          
          
               if type(variable.type)==slither.core.solidity_types.mapping_type.MappingType:
               
                  TYPE_NAME = format(variable.type)
                  
                  if 'mapping(' in TYPE_NAME:
                  
                        TYPE_NAME='map'
                        
                        list_fact.append(TYPE_NAME)
                        
                  else:
                      
                        list_fact.append(TYPE_NAME)
                  
                  #list_fact.append(format(variable.type))
                  #list_fact.append('REF'+'_'+format(variable.type))
          
               else:
               
                  if variable.type is None:
               
                     list_fact.append('void')
                     
                  else:
                  
                     TYPE_NAME = format(variable.type.type)
                     
                 
                     if 'mapping(' in TYPE_NAME:
                 
                        TYPE_NAME='map'
                 
                     list_fact.append(TYPE_NAME)
       
               Var_Facts[format(variable)]=list_fact


            return term
       
        elif type(variable)==slither.core.declarations.solidity_variables.SolidityVariableComposed:
    
    
            if variable.name=='block.timestamp':
            
               if 'block_timestamp' not in Var_Facts:
       
                  list_fact=[]
       
                  list_fact.append('_y'+str(len(Var_Facts)+1))
                    
                  list_fact.append('uint256')
          
                  Var_Facts['block_timestamp']=list_fact
         
               term = ['block_timestamp']
       
               return term
           
            elif variable.name=='msg.sender':
            
               if 'msg_sender' not in Var_Facts:
       
                  list_fact=[]
       
                  list_fact.append('_y'+str(len(Var_Facts)+1))
                    
                  list_fact.append('address')
          
                  Var_Facts['msg_sender']=list_fact
         
               term = ['msg_sender']
       
               return term
               
            elif variable.name=='msg.data':
            
               if 'msg_data' not in Var_Facts:
       
                  list_fact=[]
       
                  list_fact.append('_y'+str(len(Var_Facts)+1))
                    
                  list_fact.append('bytes')
          
                  Var_Facts['msg_data']=list_fact
         
               term = ['msg_data']
       
               return term
               
            elif variable.name=='msg.value':
            
               if 'msg_value' not in Var_Facts:
       
                  list_fact=[]
       
                  list_fact.append('_y'+str(len(Var_Facts)+1))
                    
                  list_fact.append('uint256')
          
                  Var_Facts['msg_value']=list_fact
         
               term = ['msg_value']
       
               return term


            elif variable.name=='msg.sig':
            
               if 'msg_sig' not in Var_Facts:
       
                  list_fact=[]
       
                  list_fact.append('_y'+str(len(Var_Facts)+1))
                    
                  list_fact.append('bytes4')
          
                  Var_Facts['msg_sig']=list_fact
         
               term = ['msg_sig']
       
               return term


            elif variable.name=='blockhash':
            
               if 'blockhash' not in Var_Facts:
       
                  list_fact=[]
       
                  list_fact.append('_y'+str(len(Var_Facts)+1))
                    
                  list_fact.append('bytes32')
          
                  Var_Facts['blockhash']=list_fact
         
               term = ['blockhash']
       
               return term
 
            elif variable.name=='block.basefee':
            
               if 'block_basefee' not in Var_Facts:
       
                  list_fact=[]
       
                  list_fact.append('_y'+str(len(Var_Facts)+1))
                    
                  list_fact.append('uint256')
          
                  Var_Facts['block_basefee']=list_fact
         
               term = ['block_basefee']
       
               return term

            elif variable.name=='block.chainid':
            
               if 'block_chainid' not in Var_Facts:
       
                  list_fact=[]
       
                  list_fact.append('_y'+str(len(Var_Facts)+1))
                    
                  list_fact.append('uint256')
          
                  Var_Facts['block_chainid']=list_fact
         
               term = ['block_chainid']
       
               return term
   
            elif variable.name=='block.coinbase':
            
               if 'block_coinbase' not in Var_Facts:
       
                  list_fact=[]
       
                  list_fact.append('_y'+str(len(Var_Facts)+1))
                    
                  list_fact.append('address')
          
                  Var_Facts['block_coinbase']=list_fact
         
               term = ['block_coinbase']
       
               return term
       
            elif variable.name=='block.difficulty':
            
               if 'block_difficulty' not in Var_Facts:
       
                  list_fact=[]
       
                  list_fact.append('_y'+str(len(Var_Facts)+1))
                    
                  list_fact.append('uint256')
          
                  Var_Facts['block_difficulty']=list_fact
         
               term = ['block_difficulty']
       
               return term

            elif variable.name=='block.difficulty':
            
               if 'block_difficulty' not in Var_Facts:
       
                  list_fact=[]
       
                  list_fact.append('_y'+str(len(Var_Facts)+1))
                    
                  list_fact.append('uint256')
          
                  Var_Facts['block_difficulty']=list_fact
         
               term = ['block_difficulty']
       
               return term
       
            elif variable.name=='block.gaslimit':
            
               if 'block_gaslimit' not in Var_Facts:
       
                  list_fact=[]
       
                  list_fact.append('_y'+str(len(Var_Facts)+1))
                    
                  list_fact.append('uint256')
          
                  Var_Facts['block_gaslimit']=list_fact
         
               term = ['block_gaslimit']
       
               return term
       
            elif variable.name=='block.number':
            
               if 'block_number' not in Var_Facts:
       
                  list_fact=[]
       
                  list_fact.append('_y'+str(len(Var_Facts)+1))
                    
                  list_fact.append('uint256')
          
                  Var_Facts['block_number']=list_fact
         
               term = ['block_number']
       
               return term
               
            elif variable.name=='block.number':
            
               if 'block_number' not in Var_Facts:
       
                  list_fact=[]
       
                  list_fact.append('_y'+str(len(Var_Facts)+1))
                    
                  list_fact.append('uint256')
          
                  Var_Facts[format(variable)]=list_fact
         
               term = ['block_number']
       
               return term
       
            elif variable.name=='tx.gasprice':
            
               if 'tx_gasprice' not in Var_Facts:
       
                  list_fact=[]
       
                  list_fact.append('_y'+str(len(Var_Facts)+1))
                    
                  list_fact.append('uint256')
          
                  Var_Facts['tx_gasprice']=list_fact
         
               term = ['tx_gasprice']
       
               return term

            elif variable.name=='tx.origin':
            
               if 'tx_origin' not in Var_Facts:
       
                  list_fact=[]
       
                  list_fact.append('_y'+str(len(Var_Facts)+1))
                    
                  list_fact.append('address')
          
                  Var_Facts['tx_origin']=list_fact
         
               term = ['tx_origin']
       
               return term
              
        elif type(variable)==slither.core.expressions.literal.Literal:
       
       
            term = [format(variable)]
      
            return term
       
       
        elif type(variable)==slither.slithir.variables.tuple.TupleVariable:
           
            term = [format(variable)]
        
            list_fact=[]
       
            if format(variable) not in Var_Facts:
       
               list_fact=[]
       
               list_fact.append('_y'+str(len(Var_Facts)+1))
                    
               list_fact.append('TUPLE')
          
               Var_Facts[format(variable)]=list_fact
       
            return term
       
        elif type(variable)==slither.core.variables.local_variable_init_from_tuple.LocalVariableInitFromTuple:
    
            term = [format(variable)]
         
            if format(variable) not in Var_Facts:
       
               list_fact=[]
       
               list_fact.append('_y'+str(len(Var_Facts)+1))
                      
               list_fact.append(format(variable.type))
          
               Var_Facts[format(variable)]=list_fact
             
            return term
        
               
        elif type(variable)==str:
    
            term = [format(variable)]
       
            return term
            
            #conditionExpression(self, ir_expression,Var_Facts)
                  
        else:


            print()
            print('*****************************')    
            print(type(variable))
            print('*****************************')

            return None

    '''
    #Handling Delete Array Element
    '''
    def arrayDelete(self, ir_expression,Var_Facts):

        lvalue = ir_expression.lvalue
                                    
        array_name = self.variableHandling(lvalue,Var_Facts)
    
        element_name = self.variableHandling(ir_expression.variable,Var_Facts) 
        
        if array_name[0] in self.map_REF:
        
           array_name = self.map_REF[array_name[0]]
           
           
        if element_name[0] in self.map_REF:
        
           element_name = self.map_REF[element_name[0]]
        
        #print('^^^^^^^^^^^^^^^^^^^^^^^^^^^^')
    
        #print(array_name)
    
        #print(element_name)
           
        #print('^^^^^^^^^^^^^^^^^^^^^^^^^^^^')
                
        
        if type(ir_expression.variable.type)==slither.core.solidity_types.user_defined_type.UserDefinedType:
    
           print('^^^^^^^^^^^^^^^^^^^^^^^^^^^^')
    
           print(array_name)
    
           print(element_name)
           
           print('^^^^^^^^^^^^^^^^^^^^^^^^^^^^')

        elif type(ir_expression.variable.type)==slither.core.solidity_types.elementary_type.ElementaryType:
    
             if array_name is not None and element_name is not None:
    
           
                if type(lvalue.type)==slither.core.solidity_types.mapping_type.MappingType:
           
                   map_value, map_from, map_to=self.getMappingType(lvalue)
                   
                   array_delete_term = ['-1','=',['F'+str(map_from)+'T'+str(map_to)+'Mapping_domain',array_name,element_name],['False']]
               
                   if 'F'+str(map_from)+'T'+str(map_to)+'Mapping_domain' not in Var_Facts:
       
                      list_fact=[]
       
                      list_fact.append('_y'+str(len(Var_Facts)+1))
                  
                      list_fact.append(map_value)
            
                      list_fact.append(map_to)
                  
                      #list_fact.append(map_to)
           
                      list_fact.append('bool')
            
                      Var_Facts['F'+str(map_from)+'T'+str(map_to)+'Mapping_domain']=list_fact


                   return array_delete_term
           
           
           
                else:
                
                   ARRAY_TYPE = format(ir_expression.variable.type)
           
                   array_delete_term = ['-1','=',['T'+ARRAY_TYPE+'Array_domain',array_name,element_name],['False']]
               
                   if 'T'+ARRAY_TYPE+'Array_domain' not in Var_Facts:
       
                      list_fact=[]
       
                      list_fact.append('_y'+str(len(Var_Facts)+1))
           
                      list_fact.append(Var_Facts[array_name[0]][-1])
            
                      list_fact.append(ARRAY_TYPE)
           
                      list_fact.append('bool')
            
                      Var_Facts['T'+ARRAY_TYPE+'Array_domain']=list_fact
                  
                   return array_delete_term

       
        
        elif type(ir_expression.variable.type)==slither.core.solidity_types.array_type.ArrayType:
    
             if array_name is not None and element_name is not None:
             
                if type(lvalue.type)==slither.core.solidity_types.mapping_type.MappingType:
           
                   map_value, map_from, map_to=self.getMappingType(lvalue)
                   
                   array_delete_term = ['-1','=',['F'+str(map_from)+'T'+str(map_to)+'Mapping_domain',array_name,element_name],['False']]
               
                   if 'F'+str(map_from)+'T'+str(map_to)+'Mapping_domain' not in Var_Facts:
       
                      list_fact=[]
       
                      list_fact.append('_y'+str(len(Var_Facts)+1))
                  
                      list_fact.append(map_value)
            
                      list_fact.append(map_to)
                  
                      #list_fact.append(map_to)
           
                      list_fact.append('bool')
            
                      Var_Facts['F'+str(map_from)+'T'+str(map_to)+'Mapping_domain']=list_fact


                   return array_delete_term
           
           
           
                else:
                
                   ARRAY_TYPE = format(ir_expression.variable.type)
                   
                   if '[]' in ARRAY_TYPE:
                   
                       ARRAY_TYPE = 'array'
                             
                   array_delete_term = ['-1','=',['T'+ARRAY_TYPE+'Array_domain',array_name,element_name],['False']]
               
                   if 'T'+ARRAY_TYPE+'Array_domain' not in Var_Facts:
       
                      list_fact=[]
       
                      list_fact.append('_y'+str(len(Var_Facts)+1))
           
                      list_fact.append(Var_Facts[array_name[0]][-1])
            
                      list_fact.append(ARRAY_TYPE)
           
                      list_fact.append('bool')
            
                      Var_Facts['T'+ARRAY_TYPE+'Array_domain']=list_fact
                  
                   return array_delete_term
                
             
                

        else:
    
           print('IR construction Error!')
        
           return None


    '''
    #Handling Member Operation 
    '''
    def memberOperationHandling(self, ir_expression,Var_Facts):

        lvalue=ir_expression.lvalue
    
        left_term=ir_expression.variable_left
    
        right_term=ir_expression.variable_right
    
        l_value = self.variableHandling(lvalue,Var_Facts)
        
        #if l_value is not None and 'REF' in l_value[0]:
        
        #    l_value = ['REFERENCE_'+format(lvalue.type)+'_TYPE',l_value]
           
        #    if 'REFERENCE_'+format(lvalue.type)+'_TYPE' not in Var_Facts:
           
        #        list_fact=[]
                            
        #        list_fact.append('_y'+str(len(Var_Facts)+1))
           
        #        list_fact.append(format(lvalue.type))
               
        #        list_fact.append(format(lvalue.type))
          
        #        Var_Facts['REFERENCE_'+format(lvalue.type)+'_TYPE']=list_fact

        #print('@@@@@@@@@@@@@@@@@@@@@@@@@@')
        #print(l_value)
        #print(type(left_term))
        #print(type(right_term))
        #print('@@@@@@@@@@@@@@@@@@@@@@@@@@')



        if type(left_term)==slither.core.declarations.enum_contract.EnumContract and type(right_term)==slither.slithir.variables.constant.Constant:
       
            #term = [format(left_term),['_c1'],[format(right_term)]]
            term = [format(left_term),['_x1'],[format(right_term)]]
       
            if l_value is not None and term is not None:
       
               enum_term = ['-1','=',l_value,term]
        
               if format(left_term) not in Var_Facts:
       
                  list_fact=[]
       
                  list_fact.append('_y'+str(len(Var_Facts)+1))
           
                  list_fact.append(format(ir_expression.contract))
                       
                  list_fact.append(format(lvalue.type))
             
                  list_fact.append('uint256')
            
                  Var_Facts[format(left_term)]=list_fact

               #print('####################')
               #print(enum_term)
               #print('####################')
               
               if l_value is not None and 'REF' in l_value[0]:
        
                  self.map_REF[l_value[0]]=term
               
               return enum_term
               
        elif type(left_term)==slither.core.variables.local_variable.LocalVariable and type(right_term)==slither.slithir.variables.constant.Constant:
        
             term = [format(left_term.type.type)+'_'+format(right_term),[format(left_term)]]
             
             
             if l_value is not None and term is not None:
       
               struct_term = ['-1','=',l_value,term]
               
               if format(left_term.type.type)+'_'+format(right_term) not in Var_Facts:
       
                  list_fact=[]
       
                  list_fact.append('_y'+str(len(Var_Facts)+1))
           
                  list_fact.append(format(left_term.type.type))
                  
                  if 'mapping' in format(lvalue.type):
                  
                     list_fact.append('map')
                  
                  else:
                       
                     STRUCT_TYPE=format(lvalue.type)
                     
                     if '[]' in STRUCT_TYPE:
                     
                        STRUCT_TYPE='array'
                                                                   
                     list_fact.append(STRUCT_TYPE)
                         
                  Var_Facts[format(left_term.type.type)+'_'+format(right_term)]=list_fact
                  
               if l_value is not None and 'REF' in l_value[0]:
        
                  self.map_REF[l_value[0]]=term
                  
                  
               
               return struct_term

          
        else:
        
           term = [format(left_term.type.type)+'_'+format(right_term),[format(left_term)]]
           
           if l_value is not None and term is not None:
       
               struct_term = ['-1','=',l_value,term]
               
               if format(left_term.type.type)+'_'+format(right_term) not in Var_Facts:
       
                  list_fact=[]
       
                  list_fact.append('_y'+str(len(Var_Facts)+1))
           
                  list_fact.append(format(left_term.type.type))
                       
                  list_fact.append(format(lvalue.type))
                         
                  Var_Facts[format(left_term.type.type)+'_'+format(right_term)]=list_fact
                  
                  
               if l_value is not None and 'REF' in l_value[0]:
        
                  self.map_REF[l_value[0]]=term
                  
                  
               print('~~~~~~~~~~~~~~~~~~~~~`')
               print(term)
               print(term)
               print(map_REF)
               print('~~~~~~~~~~~~~~~~~~~~~`')
               return struct_term
           
           else:
        
              print('IR construction Error!')
        
              return None


