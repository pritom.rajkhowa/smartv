import abc

from slither.utils import output


class IncorrectTranslatorInitialization(Exception):
    pass


class AbstractTranslator(metaclass=abc.ABCMeta):
    ARGUMENT = ""  # run the printer with smartV.py --ARGUMENT
    HELP = ""  # help information

    WIKI = ""

    def __init__(self, slither, logger):
        self.slither = slither
        self.contracts = slither.contracts
        self.filename = slither.filename

        self.logger = logger

        if not self.HELP:
            raise IncorrectTranslatorInitialization(
                "HELP is not initialized {}".format(self.__class__.__name__)
            )

        if not self.ARGUMENT:
            raise IncorrectTranslatorInitialization(
                "ARGUMENT is not initialized {}".format(self.__class__.__name__)
            )

        if not self.WIKI:
            raise IncorrectTranslatorInitialization(
                "WIKI is not initialized {}".format(self.__class__.__name__)
            )

    def info(self, info):
        if self.logger:
            self.logger.info(info)

    def generate_output(self, info, additional_fields=None):
        if additional_fields is None:
            additional_fields = {}
        translator_output = output.Output(info, additional_fields)
        translator_output.data["translator"] = self.ARGUMENT

        return translator_output

    @abc.abstractmethod
    def output(self, filename):
        """TODO Documentation"""
        return
