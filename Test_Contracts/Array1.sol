// SPDX-License-Identifier: MIT
pragma solidity >=0.5.0 <0.9.0;

contract Array {
    // Several ways to initialize an array
    uint[] public arr;
    uint[] public arr2 = [1,2,3];
    // Fixed sized array, all elements initialize to 0
    uint[10] public myFixedSizeArr;


    function remove(uint index) public {
        // Delete does not change the array length.
        // It resets the value at index to it's default value,
        // in this case 0
        delete arr[index];
    }
}

